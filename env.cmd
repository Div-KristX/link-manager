@echo=off
setx POSTGRES_PASSWORD "changeme"
setx JWT_SECRET "c2ltcGxlLy8tLSo="
setx DEFAULT_ADMIN_PASSWORD "$2a$10$8OMmVRz/.1T/ZZClbRz.ze9KMr2EycsYjVAINzvnRbCHRZm2/0bi."