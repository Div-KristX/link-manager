create table user_statuses
(
    id          int  not null primary key,
    description text not null
);
-- Default statuses
insert into user_statuses (id, description)
values (0, 'ACTIVE');
insert into user_statuses (id, description)
values (1, 'SUSPENDED');

create table account_types
(
    id        bigserial primary key,
    name      text not null unique,
    max_links int check ( max_links > 0 ) default 1 not null
);


insert into account_types(name, max_links)
values ('Default Account', 10);

create index account_types_idx on account_types (name);

create table users
(
    id           bigserial primary key,
    nickname     text not null unique,
    password     text not null,
    account_type bigint references account_types (id) on delete cascade,
    status       int references user_statuses (id) on delete cascade,
    used_links   int default 0
);

create table authorities
(
    id    int primary key,
    value text not null
);

create unique index authorities_value_idx on authorities (value);


create table user_authorities
(
    user_id      bigint not null,
    authority_id int    not null,
    primary key (user_id, authority_id),
    constraint user_authorities_users_fk foreign key (user_id)
        references users (id) on delete cascade,
    constraint user_authorities_authorities_fk foreign key (authority_id)
        references authorities (id) on delete cascade
);

insert into authorities (id, value)
values (0, 'ROLE_USER');
insert into authorities (id, value)
values (1, 'ROLE_ADMIN');


create table tags
(
    id  bigserial primary key,
    tag text not null unique
);

create table links
(
    id           bigserial primary key,
    hyperlink    text not null,
    description  text not null,
    tag_id       bigint references tags (id) on delete cascade,
    is_important bool not null,
    user_id      bigint references users (id) on delete cascade
);

create index users_account_idx on users (account_type);

create index links_tag_idx on links (tag_id);

create table links_share
(
    id                  bigserial primary key,
    origin_user_id      bigint references users (id) on delete cascade,
    destination_user_id bigint references users (id) on delete cascade,
    shared_link_id      bigint references links (id) on delete cascade
);

create table refresh_tokens
(
    value     uuid        not null primary key,
    user_id   bigint      not null,
    issued_at timestamptz not null,
    expire_at timestamptz not null,
    next      uuid,
    constraint refresh_tokens_user_fk foreign key (user_id)
        references users (id) on delete cascade,
    constraint refresh_tokens_next_fk foreign key (next)
        references refresh_tokens (value) on delete cascade
);

create procedure prune_refresh_tokens()
    language SQL
as
$$
delete
from refresh_tokens rt
where rt.expire_at < current_timestamp
   or rt.user_id in (select u.id from users u where u.id = rt.user_id and u.status = 1)
$$;
