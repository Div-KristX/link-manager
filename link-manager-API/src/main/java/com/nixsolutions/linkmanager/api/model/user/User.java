package com.nixsolutions.linkmanager.api.model.user;

import com.nixsolutions.linkmanager.api.model.account.AccountType;
import com.nixsolutions.linkmanager.api.model.link.Link;
import com.nixsolutions.linkmanager.api.model.sharing.LinkShare;
import java.io.Serializable;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.MapKeyEnumerated;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true, updatable = false)
    private String nickname;

    @Column(nullable = false)
    private String password;

    @JoinColumn(name = "account_type")
    @ManyToOne(fetch = FetchType.LAZY)
    private AccountType accountType;

    @Column(nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private UserStatus status = UserStatus.ACTIVE;


    @ManyToMany
    @JoinTable(name = "user_authorities",
        joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "authority_id", referencedColumnName = "id")
    )
    @MapKeyEnumerated(EnumType.ORDINAL)
    @MapKey(name = "id")
    private Map<KnownAuthority, UserAuthority> authorities = new EnumMap<>(KnownAuthority.class);


    @Column(name = "used_links", nullable = false)
    private int usedLinks = 0;


    @OrderBy("isImportant")
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Link> links;

    @OneToMany(mappedBy = "originUser", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<LinkShare> linksToShare;

    @OneToMany(mappedBy = "destinationUser", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<LinkShare> linksToReceive;

    public List<Link> getLinks() {
        return links;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

    public List<LinkShare> getLinksToShare() {
        return linksToShare;
    }

    public void setLinksToShare(
        List<LinkShare> linksToShare) {
        this.linksToShare = linksToShare;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public List<LinkShare> getLinksToReceive() {
        return linksToReceive;
    }

    public void setLinksToReceive(
        List<LinkShare> linksToReceive) {
        this.linksToReceive = linksToReceive;
    }

    public void addLink(Link link) {
        links.add(link);
        createLink();
        link.setUser(this);
    }

    public void removeLink(Link link) {
        links.remove(link);
        deleteLink();
        link.setUser(null);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public int getUsedLinks() {
        return usedLinks;
    }

    public void setUsedLinks(int usedLinks) {
        this.usedLinks = usedLinks;
    }

    private void createLink() {
        this.usedLinks++;
    }

    private void deleteLink() {
        this.usedLinks--;
    }

    public Map<KnownAuthority, UserAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(
        Map<KnownAuthority, UserAuthority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User that = (User) o;
        return Objects.equals(nickname, that.nickname);
    }

    @Override
    public int hashCode() {
        return nickname.hashCode();
    }
}
