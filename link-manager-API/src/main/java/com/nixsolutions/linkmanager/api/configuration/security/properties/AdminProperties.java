package com.nixsolutions.linkmanager.api.configuration.security.properties;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AdminProperties {

    @NotNull(message = "email must not be null")
    private String nickname;

    @NotEmpty(message = "password must not be empty")
    @Size(min = 8, message = "password's length must be at least 8")
    private char[] password;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public char[] getPassword() {
        return password;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

}
