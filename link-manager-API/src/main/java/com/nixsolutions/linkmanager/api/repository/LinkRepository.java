package com.nixsolutions.linkmanager.api.repository;

import com.nixsolutions.linkmanager.api.model.link.Link;
import com.nixsolutions.linkmanager.api.model.tag.Tag;
import com.nixsolutions.linkmanager.api.model.user.User;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface LinkRepository extends JpaRepository<Link, Long> {


    Page<Link> findAllByUserOrderByIsImportantDesc(@Param("user") User user, Pageable pageable);

    @Query("""
        select l from Link l
        where l.user = :user
        and (
        lower(l.description) like lower(concat('%', :text, '%'))
        or
        lower( l.hyperlink) like lower(concat('%', :text, '%'))
        )
        """)
    Page<Link> findAllByUserAndHyperlinkOrDescription(@Param("user") User user,
        @Param("text") String text,
        Pageable pageable);

    @Query("""
        select l from Link l
        where l.user = :user
        and l.tag = :tag and (
        lower(l.description) like lower(concat('%', :text, '%'))
        or
        lower( l.hyperlink) like lower(concat('%', :text, '%'))
        )
        """)
    Page<Link> findAllByUserHyperlinkOrDescriptionAndTag(@Param("user") User user,
        @Param("tag") Tag tag, @Param("text") String text,
        Pageable pageable);

    Page<Link> findAllByUserAndTag(User user, Tag tag,
        Pageable pageable);

    Optional<Link> findByUserAndId(User user, Long id);
}
