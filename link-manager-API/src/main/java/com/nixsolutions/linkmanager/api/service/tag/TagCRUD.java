package com.nixsolutions.linkmanager.api.service.tag;

import com.nixsolutions.linkmanager.api.model.tag.request.TagRequest;
import com.nixsolutions.linkmanager.api.model.tag.response.TagResponse;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

public interface TagCRUD {

    Page<TagResponse> findAll(Pageable pageable);

    TagResponse create(TagRequest request, Authentication authentication);

    Optional<TagResponse> getById(Long id);

    Page<TagResponse> getByName(String name, Pageable pageable);

    void update(Long id, TagRequest request, Authentication authentication);

    void deleteById(Long id, Authentication authentication);
}
