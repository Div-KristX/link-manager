package com.nixsolutions.linkmanager.api.service.sharing;

import com.nixsolutions.linkmanager.api.model.user.User;
import com.nixsolutions.linkmanager.api.exception.auth.AuthenticationExceptions;
import com.nixsolutions.linkmanager.api.exception.LinkManagerExceptions;
import com.nixsolutions.linkmanager.api.model.link.Link;
import com.nixsolutions.linkmanager.api.model.link.request.LinkSaveRequest;
import com.nixsolutions.linkmanager.api.model.link.response.LinkResponse;
import com.nixsolutions.linkmanager.api.model.sharing.LinkShare;
import com.nixsolutions.linkmanager.api.model.sharing.request.LinkShareRequest;
import com.nixsolutions.linkmanager.api.model.sharing.response.LinkShareResponse;
import com.nixsolutions.linkmanager.api.model.user.KnownAuthority;
import com.nixsolutions.linkmanager.api.repository.LinkRepository;
import com.nixsolutions.linkmanager.api.repository.LinkShareRepository;
import com.nixsolutions.linkmanager.api.repository.UserRepository;
import com.nixsolutions.linkmanager.api.service.link.LinkCRUD;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.REPEATABLE_READ)
public class LinkShareService implements LinkShareCRUD {

    private final LinkRepository linkRepository;

    private final LinkCRUD linkCRUD;
    private final UserRepository userRepository;
    private final LinkShareRepository shareRepository;

    public LinkShareService(LinkRepository linkRepository, LinkCRUD linkCRUD,
        UserRepository userRepository,
        LinkShareRepository shareRepository) {
        this.linkRepository = linkRepository;
        this.linkCRUD = linkCRUD;
        this.userRepository = userRepository;
        this.shareRepository = shareRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LinkShareResponse> findSharesForOriginUser(Authentication authentication,
        Pageable pageable) {
        User user = findUser(authentication.getPrincipal().toString());

        return shareRepository.findAllByDestinationUser(user, pageable)
            .map(LinkShareResponse::fromLinkShare);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LinkShareResponse> findSharesForOtherUsers(Authentication authentication,
        Pageable pageable) {
        User user = findUser(authentication.getPrincipal().toString());
        return shareRepository.findAllByOriginUser(user, pageable)
            .map(LinkShareResponse::fromLinkShare);
    }

    @Override
    public LinkShareResponse createInvite(Authentication authentication, LinkShareRequest request) {

        Link linkToShare = findLink(request.linkId());

        String nickname = authentication.getPrincipal().toString();

        if (!isLinkOwner(nickname, linkToShare)) {
            throw AuthenticationExceptions.noAccessRightsToUseLink(nickname, linkToShare.getId());
        }

        User originUser = findUser(nickname);

        User destinationUser = findUser(request.destinationUser());

        LinkShare share = new LinkShare();

        share.setOriginUser(originUser);

        share.setDestinationUser(destinationUser);

        share.setSharedLink(linkToShare);

        return LinkShareResponse.fromLinkShare(shareRepository.save(share));
    }

    @Override
    public void deleteInviteById(Long shareInviteId,
        Authentication authentication) {
        LinkShare share = findShare(shareInviteId);
        checkAccessRights(authentication, share);
        shareRepository.deleteById(shareInviteId);
    }

    @Override
    public LinkResponse acceptOfferedLink(Long shareInviteId, Authentication authentication) {

        String nickname = authentication.getPrincipal().toString();

        LinkShare shareInfo = findShare(shareInviteId);

        if (!isOfferHolder(nickname, shareInfo)) {
            throw AuthenticationExceptions.noAccessRightsToUseLinkShare(nickname,
                shareInfo.getId());
        }

        Link linkToShare = shareInfo.getSharedLink();

        LinkSaveRequest linkSaveRequest = new LinkSaveRequest(
            linkToShare.getHyperlink(),
            linkToShare.getDescription(),
            linkToShare.getTag().getId(),
            linkToShare.getImportant()
        );

        LinkResponse response = linkCRUD.saveLink(linkSaveRequest, authentication);

        deleteInviteById(shareInviteId, authentication);

        return response;
    }

    @Override
    public void declineOfferedLink(Long shareInviteId,
        Authentication authentication) {
        deleteInviteById(shareInviteId, authentication);
    }

    private Link findLink(Long id) {
        return linkRepository.findById(id)
            .orElseThrow(
                () -> LinkManagerExceptions.linkNotFound(id)
            );
    }

    private LinkShare findShare(Long id) {
        return shareRepository.findById(id)
            .orElseThrow(
                () -> LinkManagerExceptions.linkShareNotFound(id)
            );
    }

    private User findUser(String nickname) {
        return userRepository.findByNickname(nickname)
            .orElseThrow(
                () -> LinkManagerExceptions.userNotFound(nickname));
    }

    private boolean isOwner(String nickname, LinkShare share) {
        return nickname.equals(share.getOriginUser().getNickname());
    }

    private boolean isLinkOwner(String nickname, Link link) {
        return nickname.equals(link.getUser().getNickname());
    }

    private boolean isOfferHolder(String nickname, LinkShare share) {
        return nickname.equals(share.getDestinationUser().getNickname());
    }

    private boolean isAdmin(Authentication authentication) {
        return authentication.getAuthorities().contains(KnownAuthority.ROLE_ADMIN);
    }

    private void checkAccessRights(Authentication authentication, LinkShare share) {
        String nickname = authentication.getPrincipal().toString();
        boolean hasRights =
            isAdmin(authentication) || isOwner(nickname, share) || isOfferHolder(nickname,
                share);
        if (hasRights) {
            return;
        }
        throw AuthenticationExceptions.noAccessRightsToLink(nickname, share.getId());
    }
}
