package com.nixsolutions.linkmanager.api.controller;

import com.nixsolutions.linkmanager.api.Endpoints;
import com.nixsolutions.linkmanager.api.exception.auth.AuthenticationExceptions;
import com.nixsolutions.linkmanager.api.exception.auth.InvalidRefreshTokenException;
import com.nixsolutions.linkmanager.api.model.auth.LinkManagerUserDetails;
import com.nixsolutions.linkmanager.api.model.auth.request.RefreshTokenRequest;
import com.nixsolutions.linkmanager.api.model.auth.request.SignInRequest;
import com.nixsolutions.linkmanager.api.model.auth.response.AccessTokenResponse;
import com.nixsolutions.linkmanager.api.service.auth.AuthCRUD;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Endpoints.TOKEN)
public class AuthController {

    private final AuthCRUD authCRUD;

    public AuthController(AuthCRUD authCRUD) {
        this.authCRUD = authCRUD;
    }

    // no-authenticated
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @io.swagger.v3.oas.annotations.parameters.RequestBody(
        content = @Content(schema = @Schema(implementation = SignInRequest.class)))
    public AccessTokenResponse login(@AuthenticationPrincipal LinkManagerUserDetails userDetails) {
        return authCRUD.getToken(userDetails);
    }

    @PostMapping(value = "/refresh", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public AccessTokenResponse refresh(@RequestBody @Valid RefreshTokenRequest request) {
        try {
            return authCRUD.refreshToken(request.refreshToken());
        } catch (InvalidRefreshTokenException e) {
            throw AuthenticationExceptions.invalidRefreshToken(e);
        }
    }

    @PostMapping(value = "/invalidate", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void invalidate(@RequestBody @Valid RefreshTokenRequest request,
        @AuthenticationPrincipal String nickname) {
        try {
            authCRUD.invalidateToken(request.refreshToken(), nickname);
        } catch (InvalidRefreshTokenException e) {
            throw AuthenticationExceptions.invalidRefreshToken(e);
        }
    }

    // admin
    @DeleteMapping("/old-tokens")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void pruneRefreshTokens() {
        authCRUD.pruneRefreshTokens();
    }
}
