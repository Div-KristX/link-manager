package com.nixsolutions.linkmanager.api.service.account;


import com.nixsolutions.linkmanager.api.exception.LinkManagerExceptions;
import com.nixsolutions.linkmanager.api.exception.auth.AuthenticationExceptions;
import com.nixsolutions.linkmanager.api.model.account.AccountType;
import com.nixsolutions.linkmanager.api.model.account.request.AccountTypeRequest;
import com.nixsolutions.linkmanager.api.model.account.response.AccountTypeResponse;
import com.nixsolutions.linkmanager.api.model.user.KnownAuthority;
import com.nixsolutions.linkmanager.api.repository.AccountTypeRepository;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.REPEATABLE_READ)
public class AccountTypeService implements AccountTypeCRUD {

    private final AccountTypeRepository accountTypeRepository;

    public AccountTypeService(AccountTypeRepository accountTypeRepository) {
        this.accountTypeRepository = accountTypeRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AccountTypeResponse> findAll(Pageable pageable) {
        return accountTypeRepository.findAll(pageable).map(AccountTypeResponse::fromAccountType);
    }

    @Override
    public AccountTypeResponse create(AccountTypeRequest request, Authentication authentication) {
        if (!isAdmin(authentication)) {
            throw AuthenticationExceptions.noAccessRightsToCreateAccountType(
                authentication.getPrincipal().toString()
            );
        }

        validateUniqueFields(request);

        AccountType accountType = new AccountType();
        accountType.setName(request.name());
        accountType.setMaxLinks(request.maxLinks());
        return AccountTypeResponse.fromAccountType(accountTypeRepository.save(accountType));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AccountTypeResponse> getById(Long id) {
        return accountTypeRepository.findById(id).map(AccountTypeResponse::fromAccountType);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AccountTypeResponse> getByName(String name, Pageable pageable) {
        return accountTypeRepository.findAllByNameContainsIgnoreCase(name, pageable)
            .map(AccountTypeResponse::fromAccountType);
    }

    @Override
    public void update(Long id, AccountTypeRequest request, Authentication authentication) {
        AccountType accountType = findAccountType(id);
        checkAccessRights(authentication, accountType);
        validateUniqueFields(request);
        if (request.name() != null) accountType.setName(request.name());
        if (request.maxLinks() != null) accountType.setMaxLinks(request.maxLinks());
    }

    @Override
    public void deleteById(Long id, Authentication authentication) {
        AccountType accountType = findAccountType(id);
        checkAccessRights(authentication, accountType);
        accountTypeRepository.deleteById(id);
    }

    private AccountType findAccountType(long id) {
        return accountTypeRepository.findById(id).orElseThrow(
            () -> LinkManagerExceptions.accountTypeNotFound(id)
        );
    }

    private boolean isAdmin(Authentication authentication) {
        return authentication.getAuthorities().contains(KnownAuthority.ROLE_ADMIN);
    }

    private void checkAccessRights(Authentication authentication, AccountType accountType) {
        String nickname = authentication.getPrincipal().toString();
        boolean hasRights = isAdmin(authentication);
        if (hasRights) {
            return;
        }
        throw AuthenticationExceptions.noAccessRightsToAccountType(nickname, accountType.getId());
    }

    private void validateUniqueFields(AccountTypeRequest request) {
        String name = request.name();
        if (accountTypeRepository.existsByNameIgnoreCase(name)) {
            throw LinkManagerExceptions.duplicateAccountType(name);
        }
    }
}
