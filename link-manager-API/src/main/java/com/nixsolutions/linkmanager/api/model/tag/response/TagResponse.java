package com.nixsolutions.linkmanager.api.model.tag.response;

import com.nixsolutions.linkmanager.api.model.tag.Tag;

public record TagResponse(
    Long id,
    String tag
) {

    public static TagResponse fromTag(Tag tag) {
        return new TagResponse(
            tag.getId(),
            tag.getTag()
        );
    }
}
