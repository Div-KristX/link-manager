package com.nixsolutions.linkmanager.api.configuration.security;

public class SecurityConstants {

    private SecurityConstants() {
    }

    public static final String AUTH_TOKEN_PREFIX = "Bearer ";

    public static final String AUTHORITIES_CLAIM = "authorities";

}
