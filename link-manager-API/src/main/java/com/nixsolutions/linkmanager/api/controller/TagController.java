package com.nixsolutions.linkmanager.api.controller;


import com.nixsolutions.linkmanager.api.Endpoints;
import com.nixsolutions.linkmanager.api.exception.LinkManagerExceptions;
import com.nixsolutions.linkmanager.api.model.tag.request.TagRequest;
import com.nixsolutions.linkmanager.api.model.tag.response.TagResponse;
import com.nixsolutions.linkmanager.api.service.tag.TagCRUD;
import io.swagger.v3.oas.annotations.Parameter;
import javax.validation.Valid;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping(Endpoints.LINK_TAG)
public class TagController {

    private final TagCRUD tagCRUD;

    public TagController(TagCRUD tagCRUD) {
        this.tagCRUD = tagCRUD;
    }

    // authenticated
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @PageableAsQueryParam
    public Page<TagResponse> findAll(
        @RequestParam(value = "keyword", required = false) String keyword,
        @Parameter(hidden = true) Pageable pageable) {
        if (keyword == null) {
            return tagCRUD.findAll(pageable);
        } else {
            return tagCRUD.getByName(keyword, pageable);
        }
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TagResponse get(@PathVariable Long id) {
        return tagCRUD.getById(id)
            .orElseThrow(() -> LinkManagerExceptions.tagNotFound(id));
    }

    // admin operations
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TagResponse> create(
        @Valid @RequestBody TagRequest request,
        UriComponentsBuilder ucb,
        Authentication authentication) {
        TagResponse response = tagCRUD.create(request, authentication);
        return ResponseEntity
            .created(ucb.path("/{id}").build(response.id()))
            .body(response);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void update(
        @PathVariable Long id,
        @Valid @RequestBody TagRequest request,
        Authentication authentication) {
        tagCRUD.update(id, request, authentication);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long id, Authentication authentication) {
        tagCRUD.deleteById(id, authentication);
    }
}
