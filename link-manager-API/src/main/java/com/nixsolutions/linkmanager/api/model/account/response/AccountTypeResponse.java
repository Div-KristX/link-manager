package com.nixsolutions.linkmanager.api.model.account.response;

import com.nixsolutions.linkmanager.api.model.account.AccountType;

public record AccountTypeResponse(
    Long id,
    String name,
    Integer maxLinks
) {

    public static AccountTypeResponse fromAccountType(AccountType accountType) {
        return new AccountTypeResponse(
            accountType.getId(),
            accountType.getName(),
            accountType.getMaxLinks()
        );
    }
}
