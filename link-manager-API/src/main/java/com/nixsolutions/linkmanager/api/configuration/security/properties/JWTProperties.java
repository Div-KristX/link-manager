package com.nixsolutions.linkmanager.api.configuration.security.properties;

import java.time.Duration;
import javax.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.time.DurationMax;
import org.hibernate.validator.constraints.time.DurationMin;

public class JWTProperties {

    @NotEmpty
    private char[] secret;

    @DurationMin(minutes = 1)
    @DurationMax(minutes = 10)
    private Duration accessTime;

    @DurationMin(hours = 1)
    @DurationMax(days = 1)
    private Duration refreshTime;

    public char[] getSecret() {
        return secret;
    }

    public void setSecret(char[] secret) {
        this.secret = secret;
    }

    public Duration getAccessTime() {
        return accessTime;
    }

    public void setAccessTime(Duration accessTime) {
        this.accessTime = accessTime;
    }

    public Duration getRefreshTime() {
        return refreshTime;
    }

    public void setRefreshTime(Duration refreshTime) {
        this.refreshTime = refreshTime;
    }
}
