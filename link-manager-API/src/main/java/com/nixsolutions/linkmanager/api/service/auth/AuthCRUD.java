package com.nixsolutions.linkmanager.api.service.auth;

import com.nixsolutions.linkmanager.api.exception.auth.InvalidRefreshTokenException;
import com.nixsolutions.linkmanager.api.model.auth.LinkManagerUserDetails;
import com.nixsolutions.linkmanager.api.model.auth.response.AccessTokenResponse;

public interface AuthCRUD {

    AccessTokenResponse getToken(LinkManagerUserDetails userDetails);

    AccessTokenResponse refreshToken(String refreshToken) throws InvalidRefreshTokenException;

    void invalidateToken(String refreshToken, String ownerEmail) throws InvalidRefreshTokenException;

    void pruneRefreshTokens();
}
