package com.nixsolutions.linkmanager.api.model.user.response;


import com.nixsolutions.linkmanager.api.model.account.response.AccountTypeResponse;
import com.nixsolutions.linkmanager.api.model.user.User;
import com.nixsolutions.linkmanager.api.model.user.KnownAuthority;
import com.nixsolutions.linkmanager.api.model.user.UserStatus;
import java.util.EnumSet;
import java.util.Set;

public record UserResponse(
    Long id,
    String nickname,
    AccountTypeResponse accountType,
    UserStatus status,
    Set<KnownAuthority> authorities,
    Integer usedLinks
) {

    public static UserResponse fromUser(User userManager) {
        return new UserResponse(
            userManager.getId(),
            userManager.getNickname(),
            AccountTypeResponse.fromAccountType(userManager.getAccountType()),
            userManager.getStatus(),
            EnumSet.copyOf(userManager.getAuthorities().keySet()),
            userManager.getUsedLinks()
        );
    }

    public static UserResponse fromUserWithBasicAttributes(User userManager){
        return new UserResponse(
            userManager.getId(),
            userManager.getNickname(),
            AccountTypeResponse.fromAccountType(userManager.getAccountType()),
            userManager.getStatus(),
            null,
            userManager.getUsedLinks()
        );
    }
}
