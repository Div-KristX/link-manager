package com.nixsolutions.linkmanager.api.repository;

import com.nixsolutions.linkmanager.api.model.user.KnownAuthority;
import com.nixsolutions.linkmanager.api.model.user.UserAuthority;
import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Stream;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<UserAuthority, KnownAuthority> {

    Set<KnownAuthority> ADMIN_AUTHORITIES = EnumSet.of(
        KnownAuthority.ROLE_USER,
        KnownAuthority.ROLE_ADMIN
    );

    Stream<UserAuthority> findAllByIdIn(Set<KnownAuthority> ids);
}
