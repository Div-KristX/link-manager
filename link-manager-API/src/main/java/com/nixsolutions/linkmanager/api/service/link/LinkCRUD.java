package com.nixsolutions.linkmanager.api.service.link;

import com.nixsolutions.linkmanager.api.model.link.request.LinkSaveRequest;
import com.nixsolutions.linkmanager.api.model.link.request.LinkUpdateRequest;
import com.nixsolutions.linkmanager.api.model.link.response.LinkResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

public interface LinkCRUD {

    Page<LinkResponse> getOwnLinks(long userId, Pageable pageable);

    Page<LinkResponse> getOwnLinks(Authentication authentication, Pageable pageable);

    LinkResponse getLinkById(Authentication authentication, Long id);

    Page<LinkResponse> getOwnLinksByDescriptionOrHyperlink(String description, Authentication authentication,
        Pageable pageable);

    Page<LinkResponse> getOwnLinksByLinkTypeId(long linkTypeId, Authentication authentication,
        Pageable pageable);

    LinkResponse saveLink(LinkSaveRequest request, Authentication authentication);

    LinkResponse updateLinkById(long id, LinkUpdateRequest request, Authentication authentication);

    void deleteById(long id, Authentication authentication);

    Page<LinkResponse> getOwnLinksByDescriptionAndLinkType(Long typeId, String keywords, Authentication authentication, Pageable pageable);
}
