package com.nixsolutions.linkmanager.api.controller;

import com.nixsolutions.linkmanager.api.Endpoints;
import com.nixsolutions.linkmanager.api.model.link.response.LinkResponse;
import com.nixsolutions.linkmanager.api.model.sharing.request.LinkShareRequest;
import com.nixsolutions.linkmanager.api.model.sharing.response.LinkShareResponse;
import com.nixsolutions.linkmanager.api.service.sharing.LinkShareCRUD;
import io.swagger.v3.oas.annotations.Parameter;
import javax.validation.Valid;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping(Endpoints.SHARING)
public class LinkShareController {

    private final LinkShareCRUD shareCRUD;

    public LinkShareController(LinkShareCRUD shareCRUD) {
        this.shareCRUD = shareCRUD;
    }

    // authenticated

    @GetMapping(value = "/offers", produces = MediaType.APPLICATION_JSON_VALUE)
    @PageableAsQueryParam
    public Page<LinkShareResponse> findSharesForOriginUser(
        Authentication authentication,
        @Parameter(hidden = true) Pageable pageable
    ) {
        return shareCRUD.findSharesForOriginUser(authentication, pageable);
    }

    @GetMapping(value = "/invites", produces = MediaType.APPLICATION_JSON_VALUE)
    @PageableAsQueryParam
    public Page<LinkShareResponse> findSharesForOtherUsers(
        Authentication authentication,
        @Parameter(hidden = true) Pageable pageable
    ) {
        return shareCRUD.findSharesForOtherUsers(authentication, pageable);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/invites", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LinkShareResponse> createInvite(
        @Valid @RequestBody LinkShareRequest request,
        Authentication authentication,
        UriComponentsBuilder ucb) {
        LinkShareResponse response = shareCRUD.createInvite(authentication, request);
        return ResponseEntity
            .created(ucb.path("/invites").build(response.id()))
            .body(response);
    }

    @DeleteMapping(value = "/invites/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteInvite(
        @PathVariable Long id,
        Authentication authentication) {
        shareCRUD.deleteInviteById(id, authentication);
    }

    @DeleteMapping(value = "/offers/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void declineOfferedLink(
        @PathVariable Long id,
        Authentication authentication) {
        shareCRUD.declineOfferedLink(id, authentication);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/offers/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public LinkResponse acceptOfferedLink(
        @PathVariable Long id,
        Authentication authentication) {
        return shareCRUD.acceptOfferedLink(id, authentication);
    }

}
