package com.nixsolutions.linkmanager.api.controller;

import com.nixsolutions.linkmanager.api.Endpoints;
import com.nixsolutions.linkmanager.api.model.account.request.AccountTypeRequest;
import com.nixsolutions.linkmanager.api.model.account.response.AccountTypeResponse;
import com.nixsolutions.linkmanager.api.exception.LinkManagerExceptions;
import com.nixsolutions.linkmanager.api.service.account.AccountTypeCRUD;
import io.swagger.v3.oas.annotations.Parameter;
import javax.validation.Valid;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping(value = Endpoints.ACCOUNT_TYPES)
public class AccountTypeController {

    private final AccountTypeCRUD accountTypeCRUD;

    public AccountTypeController(AccountTypeCRUD accountTypeCRUD) {
        this.accountTypeCRUD = accountTypeCRUD;
    }

    // authenticated operation
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @PageableAsQueryParam
    public Page<AccountTypeResponse> listAccounts(
        @RequestParam(value = "keyword", required = false) String name,
        @Parameter(hidden = true) Pageable pageable) {

        if (name == null) {
            return accountTypeCRUD.findAll(pageable);
        } else {
            return accountTypeCRUD.getByName(name, pageable);
        }
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public AccountTypeResponse get(@PathVariable Long id) {
        return accountTypeCRUD.getById(id)
            .orElseThrow(() -> LinkManagerExceptions.tagNotFound(id));
    }

    // admin operations
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AccountTypeResponse> create(
        @Valid @RequestBody AccountTypeRequest request,
        UriComponentsBuilder ucb,
        Authentication authentication) {
        AccountTypeResponse response = accountTypeCRUD.create(request, authentication);
        return ResponseEntity
            .created(ucb.path("/id/{id}").build(response.id()))
            .body(response);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void update(
        @PathVariable Long id,
        @Valid @RequestBody AccountTypeRequest request,
        Authentication authentication) {
        accountTypeCRUD.update(id, request, authentication);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long id, Authentication authentication) {
        accountTypeCRUD.deleteById(id, authentication);
    }
}
