package com.nixsolutions.linkmanager.api.model.sharing.response;

import com.nixsolutions.linkmanager.api.model.sharing.LinkShare;
import com.nixsolutions.linkmanager.api.model.link.response.LinkResponse;

public record LinkShareResponse(
    Long id,
    String originUser,
    String destinationUser,
    LinkResponse linkToShare
) {

    public static LinkShareResponse fromLinkShare(LinkShare share) {
        return new LinkShareResponse(
            share.getId(),
            share.getOriginUser().getNickname(),
            share.getDestinationUser().getNickname(),
            LinkResponse.fromLink(share.getSharedLink())
        );
    }
}
