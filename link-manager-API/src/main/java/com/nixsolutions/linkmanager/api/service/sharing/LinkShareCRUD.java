package com.nixsolutions.linkmanager.api.service.sharing;

import com.nixsolutions.linkmanager.api.model.sharing.request.LinkShareRequest;
import com.nixsolutions.linkmanager.api.model.sharing.response.LinkShareResponse;
import com.nixsolutions.linkmanager.api.model.link.response.LinkResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

public interface LinkShareCRUD {

    Page<LinkShareResponse> findSharesForOriginUser(Authentication authentication,
        Pageable pageable);

    Page<LinkShareResponse> findSharesForOtherUsers(Authentication authentication,
        Pageable pageable);

    LinkShareResponse createInvite(Authentication authentication, LinkShareRequest request);

    void deleteInviteById(Long shareInviteId, Authentication authentication);

    LinkResponse acceptOfferedLink(Long shareInviteId, Authentication authentication);

    void declineOfferedLink(Long shareInviteId,
        Authentication authentication);

}
