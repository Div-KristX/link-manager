package com.nixsolutions.linkmanager.api.model.user.request;

import javax.validation.constraints.NotBlank;

public record ChangeUserAccountRequest(
    @NotBlank
    String accountType
) {

}
