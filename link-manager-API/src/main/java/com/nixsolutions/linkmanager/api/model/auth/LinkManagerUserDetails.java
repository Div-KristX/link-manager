package com.nixsolutions.linkmanager.api.model.auth;

import com.nixsolutions.linkmanager.api.model.user.User;
import com.nixsolutions.linkmanager.api.model.user.UserStatus;
import java.util.EnumSet;

public class LinkManagerUserDetails extends org.springframework.security.core.userdetails.User {

    private final User source;

    public LinkManagerUserDetails(User source) {
        super(
            source.getNickname(),
            source.getPassword(),
            source.getStatus() == UserStatus.ACTIVE,
            true,
            true,
            true,
            EnumSet.copyOf(source.getAuthorities().keySet()));
        this.source = source;
    }

    public User getSource() {
        return source;
    }
}
