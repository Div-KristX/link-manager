package com.nixsolutions.linkmanager.api.configuration.security.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nixsolutions.linkmanager.api.model.auth.request.SignInRequest;
import java.io.IOException;
import java.io.UncheckedIOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final ObjectMapper objectMapper;

    public JWTAuthenticationFilter(
        AuthenticationManager authenticationManager,
        ObjectMapper objectMapper
    ) {
        setAuthenticationManager(authenticationManager);
        setUsernameParameter("login");
        this.objectMapper = objectMapper;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
        HttpServletResponse res) {
        SignInRequest credentials;
        try {
            credentials = objectMapper.readValue(req.getInputStream(), SignInRequest.class);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        var authToken = new UsernamePasswordAuthenticationToken(
            credentials.login(),
            credentials.password()
        );
        return getAuthenticationManager().authenticate(authToken);
    }

    @Override
    protected void successfulAuthentication(
        HttpServletRequest req,
        HttpServletResponse res,
        FilterChain chain,
        Authentication auth) throws IOException, ServletException {

        SecurityContextHolder.getContext().setAuthentication(auth);

        chain.doFilter(req, res);
    }

}
