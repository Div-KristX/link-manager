package com.nixsolutions.linkmanager.api.exception.auth;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class AuthenticationExceptions {

    private AuthenticationExceptions() {

    }

    public static ResponseStatusException authorityNotFound(String value) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND,
            "User authority " + value + " not defined");
    }

    public static ResponseStatusException noAccessRightsToCreateLinkType(String nickname) {
        return new ResponseStatusException(HttpStatus.FORBIDDEN,
            "User " + nickname + " has no access rights to create link-types ");
    }

    public static ResponseStatusException noAccessRightsToCreateAccountType(String nickname) {
        return new ResponseStatusException(HttpStatus.FORBIDDEN,
            "User " + nickname + " has no access rights to create account-types ");
    }

    public static ResponseStatusException noAccessRightsToUseLink(String nickname, long linkId) {
        return new ResponseStatusException(HttpStatus.FORBIDDEN,
            "User " + nickname + " has no access rights to use link " + linkId);
    }

    public static ResponseStatusException noAccessRightsToTag(String nickname, long tagId) {
        return new ResponseStatusException(HttpStatus.FORBIDDEN,
            "User " + nickname + " has no access rights to tag " + tagId);
    }

    public static ResponseStatusException noAccessRightsToAccountType(String nickname, long accountTypeId) {
        return new ResponseStatusException(HttpStatus.FORBIDDEN,
            "User " + nickname + " has no access rights to account-type " + accountTypeId);
    }

    public static ResponseStatusException noAccessRightsToLink(String nickname, long linkId) {
        return new ResponseStatusException(HttpStatus.FORBIDDEN,
            "User " + nickname + " has no access rights to link " + linkId);
    }

    public static ResponseStatusException noAccessRightsToUseLinkShare(String nickname,
        long linkId) {
        return new ResponseStatusException(HttpStatus.FORBIDDEN,
            "User " + nickname + " has no access rights to use link-sharing offer " + linkId);
    }

    public static ResponseStatusException invalidRefreshToken(InvalidRefreshTokenException cause) {
        return new ResponseStatusException(HttpStatus.UNAUTHORIZED,
            "Refresh token is invalid! It may have been rotated, invalidated or expired naturally",
            cause);
    }

    public static ResponseStatusException wrongPassword() {
        return new ResponseStatusException(HttpStatus.BAD_REQUEST, "Password is incorrect");
    }
}
