package com.nixsolutions.linkmanager.api.controller;

import com.nixsolutions.linkmanager.api.Endpoints;
import com.nixsolutions.linkmanager.api.exception.LinkManagerExceptions;
import com.nixsolutions.linkmanager.api.model.link.response.LinkResponse;
import com.nixsolutions.linkmanager.api.model.user.request.ChangeUserAccountRequest;
import com.nixsolutions.linkmanager.api.model.user.request.ChangeUserPasswordRequest;
import com.nixsolutions.linkmanager.api.model.user.request.ChangeUserStatusRequest;
import com.nixsolutions.linkmanager.api.model.user.request.OverridePasswordRequest;
import com.nixsolutions.linkmanager.api.model.user.request.UserSaveRequest;
import com.nixsolutions.linkmanager.api.model.user.response.UserResponse;
import com.nixsolutions.linkmanager.api.service.link.LinkCRUD;
import com.nixsolutions.linkmanager.api.service.user.UserCRUD;
import io.swagger.v3.oas.annotations.Parameter;
import javax.validation.Valid;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Endpoints.USERS)
public class UserController {

    private final UserCRUD userCRUD;

    private final LinkCRUD linkCRUD;

    public UserController(UserCRUD userCRUD, LinkCRUD linkCRUD) {
        this.userCRUD = userCRUD;
        this.linkCRUD = linkCRUD;
    }

    // authenticated
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public UserResponse register(@RequestBody @Valid UserSaveRequest request) {
        return userCRUD.create(request);
    }

    @GetMapping(value = "/me", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserResponse getCurrentUser(@AuthenticationPrincipal String nickname) {
        return userCRUD.findByNickname(nickname).orElseThrow(
            () -> LinkManagerExceptions.userNotFound(nickname)
        );
    }

    @DeleteMapping(value = "/me")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCurrentUser(@AuthenticationPrincipal String nickname) {
        userCRUD.deleteByNickname(nickname);
    }

    @PatchMapping(value = "/me/password", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserResponse changeCurrentUserPassword(@AuthenticationPrincipal String nickname,
        @RequestBody @Valid
        ChangeUserPasswordRequest request) {
        return userCRUD.changePasswordByNickname(nickname, request);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @PageableAsQueryParam
    public Page<UserResponse> listUsers(
        @RequestParam(value = "keyword", required = false) String nickname,
        @Parameter(hidden = true) Pageable pageable) {
        if (nickname != null) {
            return userCRUD.listUsersWithNickname(nickname, pageable);
        } else {
            return userCRUD.list(pageable);
        }
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserResponse getUserById(@PathVariable long id) {
        return userCRUD.findById(id).orElseThrow(() -> LinkManagerExceptions.userNotFound(id));
    }

    // admin operations

    @GetMapping(value = "/{id}/links", produces = MediaType.APPLICATION_JSON_VALUE)
    @PageableAsQueryParam
    public Page<LinkResponse> getLinksById(
        @PathVariable long id,
        @Parameter(hidden = true) Pageable pageable
    ) {
        return linkCRUD.getOwnLinks(id, pageable);
    }
    @PatchMapping(value = "/{id}/password", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserResponse overrideUserPassword(
        @PathVariable long id,
        @RequestBody @Valid OverridePasswordRequest request) {
        return userCRUD.changePasswordById(id, request);
    }

    @PostMapping(value = "/admins", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public UserResponse registerAdmin(@RequestBody @Valid UserSaveRequest request) {
        return userCRUD.createAdmin(request);
    }


    @PatchMapping(value = "/{id}/status", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserResponse changeUserStatusById(
        @PathVariable long id,
        @RequestBody @Valid ChangeUserStatusRequest status) {
        return userCRUD.changeStatusById(id, status);
    }

    @PatchMapping(value = "/{id}/account", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserResponse changeAccountTypeById(
        @PathVariable long id,
        @RequestBody @Valid ChangeUserAccountRequest request) {
        return userCRUD.changeAccountTypeById(id, request);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void delete(@PathVariable Long id) {
        userCRUD.deleteById(id);
    }
}
