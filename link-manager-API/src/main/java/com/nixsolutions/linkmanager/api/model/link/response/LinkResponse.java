package com.nixsolutions.linkmanager.api.model.link.response;

import com.nixsolutions.linkmanager.api.model.link.Link;
import com.nixsolutions.linkmanager.api.model.tag.response.TagResponse;

public record LinkResponse(
    Long id,
    String hyperlink,
    String description,
    TagResponse tag,
    Boolean important
) {

    public static LinkResponse fromLink(Link link) {
        return new LinkResponse(
            link.getId(),
            link.getHyperlink(),
            link.getDescription(),
            TagResponse.fromTag(link.getTag()),
            link.getImportant()
        );
    }
}
