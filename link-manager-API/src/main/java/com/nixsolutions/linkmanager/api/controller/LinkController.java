package com.nixsolutions.linkmanager.api.controller;


import com.nixsolutions.linkmanager.api.Endpoints;
import com.nixsolutions.linkmanager.api.model.link.request.LinkSaveRequest;
import com.nixsolutions.linkmanager.api.model.link.request.LinkUpdateRequest;
import com.nixsolutions.linkmanager.api.model.link.response.LinkResponse;
import com.nixsolutions.linkmanager.api.service.link.LinkCRUD;
import io.swagger.v3.oas.annotations.Parameter;
import javax.validation.Valid;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Endpoints.LINKS)
public class LinkController {

    private final LinkCRUD linkCRUD;

    public LinkController(LinkCRUD linkCRUD) {
        this.linkCRUD = linkCRUD;
    }

    //authenticated
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @PageableAsQueryParam
    public Page<LinkResponse> getLinks(
        Authentication authentication,
        @RequestParam(value = "keywords", required = false) String keywords,
        @RequestParam(value = "typeId", required = false) Long typeId,
        @Parameter(hidden = true) Pageable pageable
    ) {

        if (keywords == null && typeId == null) {
            return linkCRUD.getOwnLinks(authentication, pageable);
        } else if (keywords == null) {
            return linkCRUD.getOwnLinksByLinkTypeId(typeId, authentication, pageable);
        } else if (typeId == null){
            return linkCRUD.getOwnLinksByDescriptionOrHyperlink(keywords, authentication, pageable);
        } else {
            return linkCRUD.getOwnLinksByDescriptionAndLinkType(typeId, keywords, authentication, pageable);
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public LinkResponse createLink(
        @RequestBody LinkSaveRequest request,
        Authentication authentication) {
        return linkCRUD.saveLink(request, authentication);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public LinkResponse editLink(
        @PathVariable long id,
        @RequestBody @Valid LinkUpdateRequest request,
        Authentication authentication) {
        return linkCRUD.updateLinkById(id, request, authentication);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public LinkResponse getLinkByID(
        @PathVariable long id,
        Authentication authentication) {
        return linkCRUD.getLinkById(authentication, id);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable long id, Authentication authentication) {
        linkCRUD.deleteById(id, authentication);
    }
}
