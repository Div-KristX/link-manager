package com.nixsolutions.linkmanager.api.model.sharing;

import com.nixsolutions.linkmanager.api.model.link.Link;
import com.nixsolutions.linkmanager.api.model.user.User;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "links_share")
public class LinkShare {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "origin_user_id", updatable = false)
    @ManyToOne
    private User originUser;

    @JoinColumn(name = "destination_user_id", updatable = false)
    @ManyToOne
    private User destinationUser;

    @JoinColumn(name = "shared_link_id", updatable = false)
    @ManyToOne
    private Link sharedLink;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getOriginUser() {
        return originUser;
    }

    public void setOriginUser(User originUserManager) {
        this.originUser = originUserManager;
    }

    public User getDestinationUser() {
        return destinationUser;
    }

    public void setDestinationUser(User destinationUserManager) {
        this.destinationUser = destinationUserManager;
    }

    public Link getSharedLink() {
        return sharedLink;
    }

    public void setSharedLink(Link sharedLink) {
        this.sharedLink = sharedLink;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LinkShare linkShare = (LinkShare) o;
        return Objects.equals(originUser, linkShare.originUser) && Objects.equals(
            destinationUser, linkShare.destinationUser) && Objects.equals(sharedLink,
            linkShare.sharedLink);
    }

    @Override
    public int hashCode() {
        return Objects.hash(originUser, destinationUser, sharedLink);
    }
}
