package com.nixsolutions.linkmanager.api.exception.auth;

public class InvalidRefreshTokenException extends Exception{
    public InvalidRefreshTokenException() {
        super();
    }

    public InvalidRefreshTokenException(String message) {
        super(message);
    }

    public InvalidRefreshTokenException(Throwable cause) {
        super(cause);
    }

}
