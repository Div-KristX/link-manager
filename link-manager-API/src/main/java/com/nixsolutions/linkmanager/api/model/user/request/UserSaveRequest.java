package com.nixsolutions.linkmanager.api.model.user.request;

import com.fasterxml.jackson.annotation.JsonAlias;
import javax.validation.constraints.NotBlank;

public record UserSaveRequest(
    @JsonAlias({"nickname", "login"})
    @NotBlank(message = "Nickname content is mandatory") String nickname,
    @NotBlank(message = "Password content is mandatory") String password
) {

}
