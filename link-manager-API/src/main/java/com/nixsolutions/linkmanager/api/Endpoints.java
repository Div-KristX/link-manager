package com.nixsolutions.linkmanager.api;

public class Endpoints {

    public static final String API_ROOT = "/api/v1";
    public static final String USERS = API_ROOT + "/users";
    public static final String TOKEN = API_ROOT + "/token";

    public static final String LINK_TAG = API_ROOT + "/tags";

    public static final String LINKS = API_ROOT + "/links";

    public static final String SHARING = API_ROOT + "/sharing";

    public static final String ACCOUNT_TYPES = API_ROOT + "/accounts";

    private Endpoints() {
        throw new AssertionError("non-instantiable class");
    }


}
