package com.nixsolutions.linkmanager.api.model.user;

public enum UserStatus {
    ACTIVE, SUSPENDED
}
