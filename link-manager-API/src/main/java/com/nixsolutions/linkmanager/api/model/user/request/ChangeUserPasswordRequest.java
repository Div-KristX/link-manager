package com.nixsolutions.linkmanager.api.model.user.request;

import javax.validation.constraints.NotBlank;

public record ChangeUserPasswordRequest(
    @NotBlank(message = "Old password must not be blank")
    String oldPassword,

    @NotBlank(message = "New password must not be blank")
    String newPassword
) {

}
