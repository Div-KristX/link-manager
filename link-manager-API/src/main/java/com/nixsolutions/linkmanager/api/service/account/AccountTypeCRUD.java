package com.nixsolutions.linkmanager.api.service.account;

import com.nixsolutions.linkmanager.api.model.account.request.AccountTypeRequest;
import com.nixsolutions.linkmanager.api.model.account.response.AccountTypeResponse;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

public interface AccountTypeCRUD {

    Page<AccountTypeResponse> findAll(Pageable pageable);

    AccountTypeResponse create(AccountTypeRequest request, Authentication authentication);

    Optional<AccountTypeResponse> getById(Long id);

    Page<AccountTypeResponse> getByName(String name, Pageable pageable);

    void update(Long id, AccountTypeRequest request, Authentication authentication);

    void deleteById(Long id, Authentication authentication);
}
