package com.nixsolutions.linkmanager.api.service.user;


import com.nixsolutions.linkmanager.api.exception.LinkManagerExceptions;
import com.nixsolutions.linkmanager.api.exception.auth.AuthenticationExceptions;
import com.nixsolutions.linkmanager.api.model.account.AccountType;
import com.nixsolutions.linkmanager.api.model.auth.LinkManagerUserDetails;
import com.nixsolutions.linkmanager.api.model.user.KnownAuthority;
import com.nixsolutions.linkmanager.api.model.user.User;
import com.nixsolutions.linkmanager.api.model.user.UserAuthority;
import com.nixsolutions.linkmanager.api.model.user.request.ChangeUserAccountRequest;
import com.nixsolutions.linkmanager.api.model.user.request.ChangeUserPasswordRequest;
import com.nixsolutions.linkmanager.api.model.user.request.ChangeUserStatusRequest;
import com.nixsolutions.linkmanager.api.model.user.request.OverridePasswordRequest;
import com.nixsolutions.linkmanager.api.model.user.request.UserSaveRequest;
import com.nixsolutions.linkmanager.api.model.user.response.UserResponse;
import com.nixsolutions.linkmanager.api.repository.AccountTypeRepository;
import com.nixsolutions.linkmanager.api.repository.AuthorityRepository;
import com.nixsolutions.linkmanager.api.repository.UserRepository;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(isolation = Isolation.REPEATABLE_READ)
public class UserService implements UserCRUD, UserDetailsService {

    private final UserRepository userRepository;

    private final AccountTypeRepository accountTypeRepository;

    private final AuthorityRepository authorityRepository;

    private final PasswordEncoder passwordEncoder;


    public UserService(UserRepository userRepository, AccountTypeRepository accountTypeRepository,
        AuthorityRepository authorityRepository,
        PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.accountTypeRepository = accountTypeRepository;
        this.authorityRepository = authorityRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserResponse> list(Pageable pageable) {
        return userRepository.findAll(pageable).map(UserResponse::fromUserWithBasicAttributes);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserResponse> listUsersWithNickname(String nickname, Pageable pageable) {
        return userRepository.findAllByNicknameContainsIgnoreCase(nickname, pageable)
            .map(UserResponse::fromUserWithBasicAttributes);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findUser(username);
        return new LinkManagerUserDetails(user);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserResponse> findById(long id) {
        return userRepository.findById(id).map(UserResponse::fromUser);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserResponse> findByNickname(String nickname) {

        return userRepository.findByNickname(nickname).map(UserResponse::fromUser);
    }

    @Override
    public UserResponse create(UserSaveRequest request) {
        validateUniqueFields(request);
        return UserResponse.fromUser(save(request, getRegularUserAuthorities()));
    }

    @Override
    public UserResponse createAdmin(UserSaveRequest request) {
        validateUniqueFields(request);
        return UserResponse.fromUser(save(request, getAdminAuthorities()));
    }

    @Override
    public UserResponse changeStatusById(long id, ChangeUserStatusRequest status) {
        User user = findUser(id);
        if (user.getStatus() != status.status()) {
            user.setStatus(status.status());
        }
        return UserResponse.fromUser(user);
    }

    @Override
    public UserResponse changeAccountTypeById(long id, ChangeUserAccountRequest request) {
        User userManager = findUser(id);

        AccountType accountType = findAccountType(request.accountType());

        if (accountChangeIsPossible(userManager, accountType)) {
            userManager.setAccountType(accountType);
        } else {
            throw LinkManagerExceptions.userAccountChangeConflict(userManager.getNickname(),
                userManager.getUsedLinks(),
                accountType.getMaxLinks());
        }
        return UserResponse.fromUser(userManager);
    }

    @Override
    public UserResponse changePasswordByNickname(String nickname,
        ChangeUserPasswordRequest request) {
        User user = findUser(nickname);
        changePassword(user, request.oldPassword(), request.newPassword());
        return UserResponse.fromUser(user);
    }

    @Override
    public UserResponse changePasswordById(long id, OverridePasswordRequest request) {
        User user = findUser(id);
        user.setPassword(passwordEncoder.encode(request.newPassword()));
        return UserResponse.fromUser(user);
    }


    @Override
    public void mergeAdmins(List<UserSaveRequest> requests) {
        if (requests.isEmpty()) {
            return;
        }
        Map<KnownAuthority, UserAuthority> authorities = getAdminAuthorities();
        for (UserSaveRequest request : requests) {
            String nickname = request.nickname();
            if (userRepository.findByNickname(nickname).isEmpty()) {
                User user = new User();
                user.setNickname(nickname);
                user.setAccountType(accountTypeRepository.findById(1L)
                    .orElseThrow(() -> LinkManagerExceptions.accountTypeNotFound(1)));
                user.setPassword(passwordEncoder.encode(request.password()));
                user.getAuthorities().putAll(authorities);
                userRepository.save(user);
            }
        }
    }


    @Override
    public void deleteById(long id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
        } else {
            throw LinkManagerExceptions.userNotFound(id);
        }
    }

    @Override
    public void deleteByNickname(String nickname) {
        if (userRepository.existsByNicknameIgnoreCase(nickname)) {
            userRepository.deleteByNickname(nickname);
        } else {
            throw LinkManagerExceptions.userNotFound(nickname);
        }
    }

    private void validateUniqueFields(UserSaveRequest request) {
        String nickname = request.nickname();
        if (userRepository.existsByNicknameIgnoreCase(nickname)) {
            throw LinkManagerExceptions.duplicateNickname(nickname);
        }
    }

    private boolean accountChangeIsPossible(User user, AccountType accountType) {
        return user.getUsedLinks() <= accountType.getMaxLinks();
    }

    private void changePassword(User user, String oldPassword, String newPassword) {
        if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
            throw AuthenticationExceptions.wrongPassword();
        }
        user.setPassword(passwordEncoder.encode(newPassword));
    }


    private Map<KnownAuthority, UserAuthority> getAdminAuthorities() {
        return authorityRepository.findAllByIdIn(AuthorityRepository.ADMIN_AUTHORITIES)
            .collect(Collectors.toMap(
                UserAuthority::getId,
                Function.identity(),
                (e1, e2) -> e2,
                () -> new EnumMap<>(KnownAuthority.class)));
    }

    private Map<KnownAuthority, UserAuthority> getRegularUserAuthorities() {
        UserAuthority authority = authorityRepository
            .findById(KnownAuthority.ROLE_USER)
            .orElseThrow(
                () -> AuthenticationExceptions.authorityNotFound(KnownAuthority.ROLE_USER.name()));
        Map<KnownAuthority, UserAuthority> authorities = new EnumMap<>(KnownAuthority.class);
        authorities.put(KnownAuthority.ROLE_USER, authority);
        return authorities;
    }


    private User save(UserSaveRequest request,
        Map<KnownAuthority, UserAuthority> authorities) {
        User user = new User();
        user.setNickname(request.nickname());
        user.setPassword(passwordEncoder.encode(request.password()));
        user.setAccountType(accountTypeRepository.findById(1L).orElseThrow(
            () -> LinkManagerExceptions.accountTypeNotFound(1)
        ));
        user.getAuthorities().putAll(authorities);
        userRepository.save(user);
        return user;
    }

    private User findUser(long id) {
        return userRepository.findById(id)
            .orElseThrow(
                () -> LinkManagerExceptions.userNotFound(id)
            );
    }

    private User findUser(String nickname) {
        return userRepository.findByNickname(nickname)
            .orElseThrow(
                () -> LinkManagerExceptions.userNotFound(nickname)
            );
    }

    private AccountType findAccountType(String name) {
        return accountTypeRepository.findByName(name)
            .orElseThrow(() -> LinkManagerExceptions.accountTypeNotFound(name));
    }
}
