package com.nixsolutions.linkmanager.api.repository;

import com.nixsolutions.linkmanager.api.model.user.User;
import com.nixsolutions.linkmanager.api.model.user.UserStatus;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByNickname(String nickname);

    Page<User> findAllByNicknameContainsIgnoreCase(String nickname, Pageable pageable);

    boolean existsByNicknameIgnoreCase(String nickname);

    void deleteByNickname(String nickname);

    @Query("update User u set u.status = :status where u.nickname = :nickname")
    @Modifying
    void changeStatusByNickname(@Param("nickname") String nickname,
        @Param("status") UserStatus status);

}
