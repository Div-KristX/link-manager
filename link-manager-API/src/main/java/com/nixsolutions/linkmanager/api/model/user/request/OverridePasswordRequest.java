package com.nixsolutions.linkmanager.api.model.user.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public record OverridePasswordRequest(
    @NotBlank(message = "password must not be blank")
    @Size(min = 8, message = "password's length must be at least 8")
    String newPassword
) {

}
