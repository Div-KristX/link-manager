package com.nixsolutions.linkmanager.api.model.link.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public record LinkUpdateRequest(
    @NotBlank(message = "Hyperlink content is mandatory") String hyperlink,

    @NotBlank(message = "Description content is mandatory") String description,

    @NotNull(message = "Tag content is mandatory") Long tag,

    @NotNull(message = "Important flag can`t be null") Boolean important
) {

}
