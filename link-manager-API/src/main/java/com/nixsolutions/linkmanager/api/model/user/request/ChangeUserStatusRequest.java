package com.nixsolutions.linkmanager.api.model.user.request;

import com.nixsolutions.linkmanager.api.model.user.UserStatus;
import javax.validation.constraints.NotNull;

public record ChangeUserStatusRequest(
    @NotNull UserStatus status
) {

}
