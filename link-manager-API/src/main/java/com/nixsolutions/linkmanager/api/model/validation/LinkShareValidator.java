package com.nixsolutions.linkmanager.api.model.validation;

import com.nixsolutions.linkmanager.api.model.sharing.request.LinkShareRequest;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LinkShareValidator implements
    ConstraintValidator<UniqueNamesToCreateInvite, LinkShareRequest> {

    @Override
    public void initialize(UniqueNamesToCreateInvite constraintAnnotation) {
    }

    @Override
    public boolean isValid(LinkShareRequest value, ConstraintValidatorContext context) {
        return !value.originUser().equals(value.destinationUser());
    }
}
