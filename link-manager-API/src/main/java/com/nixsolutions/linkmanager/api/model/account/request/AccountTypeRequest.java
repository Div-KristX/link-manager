package com.nixsolutions.linkmanager.api.model.account.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

public record AccountTypeRequest(
    @NotBlank String name,
    @Positive Integer maxLinks
) {

}
