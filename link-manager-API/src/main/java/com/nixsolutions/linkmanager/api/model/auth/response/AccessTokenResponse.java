package com.nixsolutions.linkmanager.api.model.auth.response;

public record AccessTokenResponse(
    String accessToken,
    String refreshToken,
    long expireIn
) {

}
