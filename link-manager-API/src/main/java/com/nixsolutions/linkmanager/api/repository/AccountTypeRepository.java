package com.nixsolutions.linkmanager.api.repository;

import com.nixsolutions.linkmanager.api.model.account.AccountType;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountTypeRepository extends JpaRepository<AccountType, Long> {

    Page<AccountType> findAllByNameContainsIgnoreCase(String name, Pageable pageable);

    Optional<AccountType> findByName(String name);

    boolean existsByNameIgnoreCase(String name);
}
