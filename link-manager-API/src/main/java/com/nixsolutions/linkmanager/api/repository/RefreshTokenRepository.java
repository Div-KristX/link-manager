package com.nixsolutions.linkmanager.api.repository;

import com.nixsolutions.linkmanager.api.model.auth.RefreshToken;
import com.nixsolutions.linkmanager.api.model.user.UserStatus;
import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken, UUID> {

    @Query("""
        select rt from RefreshToken rt
        inner join fetch rt.user u
        where rt.value = :value and rt.expireAt > :when and u.status = :status
        """)
    Optional<RefreshToken> findIfValid(@Param("value") UUID value, @Param("when") OffsetDateTime when, @Param("status") UserStatus status);

    @Query("delete from RefreshToken rt where rt = :head or rt.next = :head")
    @Modifying
    void deleteChain(@Param("head") RefreshToken head);

    @Query("update RefreshToken rt set rt.next = :newHead where rt = :oldHead or rt.next = :oldHead")
    @Modifying
    void updateChain(@Param("oldHead") RefreshToken oldHead, @Param("newHead") RefreshToken newHead);

    @Query(value = "CALL prune_refresh_tokens();", nativeQuery = true)
    @Modifying
    void pruneRefreshTokens();
}
