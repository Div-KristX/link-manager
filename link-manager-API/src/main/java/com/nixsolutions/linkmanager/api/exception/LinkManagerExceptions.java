package com.nixsolutions.linkmanager.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class LinkManagerExceptions {

    private LinkManagerExceptions() {
    }

    public static ResponseStatusException userNotFound(String nickname) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND,
            "User with nickname " + nickname + " not found");
    }

    public static ResponseStatusException userNotFound(long id) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND,
            "User with id " + id + " not found");
    }

    public static ResponseStatusException userLinkCreationLimit(int maxLinks) {
        return new ResponseStatusException(HttpStatus.CONFLICT,
            "User link usage limit of " + maxLinks + " reached");
    }

    public static ResponseStatusException userAccountChangeConflict(String nickname,
        int currentLinksCount,
        int accountMaxLinks) {
        return new ResponseStatusException(HttpStatus.CONFLICT,
            "User [" + nickname + "] current links [" + currentLinksCount
                + "] count by Account-type greater than offered Account-type [" + accountMaxLinks
                + "] may use");
    }

    public static ResponseStatusException tagNotFound(long id) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND,
            "Tag with id " + id + " not found");
    }

    public static ResponseStatusException linkNotFound(long id) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND,
            "Link with id " + id + " not found");
    }

    public static ResponseStatusException accountTypeNotFound(long id) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND,
            "Account-type with id " + id + " not found");
    }

    public static ResponseStatusException accountTypeNotFound(String name) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND,
            "Account-type with name " + name + " not found");
    }

    public static ResponseStatusException linkShareNotFound(long id) {
        return new ResponseStatusException(HttpStatus.NOT_FOUND,
            "Link-share with id " + id + " not found");
    }


    public static ResponseStatusException duplicateNickname(String nickname) {
        return new ResponseStatusException(HttpStatus.BAD_REQUEST,
            "The nickname " + nickname + "  has already been taken");
    }

    public static ResponseStatusException duplicateTag(String linkType) {
        return new ResponseStatusException(HttpStatus.BAD_REQUEST,
            "Tag " + linkType + " already exists");
    }

    public static ResponseStatusException duplicateAccountType(String account) {
        return new ResponseStatusException(HttpStatus.BAD_REQUEST,
            "Account-type " + account + " already exists");
    }

}
