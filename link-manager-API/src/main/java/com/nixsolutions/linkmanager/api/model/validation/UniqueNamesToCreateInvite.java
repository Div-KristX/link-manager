package com.nixsolutions.linkmanager.api.model.validation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = LinkShareValidator.class)
@Target({TYPE, METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface UniqueNamesToCreateInvite {

    String message() default "{Origin user and destination user are the same}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
