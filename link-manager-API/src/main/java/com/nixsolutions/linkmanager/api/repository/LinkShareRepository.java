package com.nixsolutions.linkmanager.api.repository;

import com.nixsolutions.linkmanager.api.model.user.User;
import com.nixsolutions.linkmanager.api.model.sharing.LinkShare;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LinkShareRepository extends JpaRepository<LinkShare, Long> {

    Page<LinkShare> findAllByDestinationUser(User user, Pageable pageable);

    Page<LinkShare> findAllByOriginUser(User user, Pageable pageable);
}
