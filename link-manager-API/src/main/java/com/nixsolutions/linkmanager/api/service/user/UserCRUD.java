package com.nixsolutions.linkmanager.api.service.user;

import com.nixsolutions.linkmanager.api.model.user.request.ChangeUserAccountRequest;
import com.nixsolutions.linkmanager.api.model.user.request.ChangeUserPasswordRequest;
import com.nixsolutions.linkmanager.api.model.user.request.ChangeUserStatusRequest;
import com.nixsolutions.linkmanager.api.model.user.request.OverridePasswordRequest;
import com.nixsolutions.linkmanager.api.model.user.request.UserSaveRequest;
import com.nixsolutions.linkmanager.api.model.user.response.UserResponse;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserCRUD extends UserDetailsService {

    Page<UserResponse> list(Pageable pageable);

    Page<UserResponse> listUsersWithNickname(String nickname, Pageable pageable);

    Optional<UserResponse> findById(long id);

    Optional<UserResponse> findByNickname(String nickname);

    UserResponse create(UserSaveRequest request);

    UserResponse createAdmin(UserSaveRequest request);

    UserResponse changeStatusById(long id, ChangeUserStatusRequest status);

    UserResponse changeAccountTypeById(long id, ChangeUserAccountRequest request);

    UserResponse changePasswordByNickname(String nickname, ChangeUserPasswordRequest request);

    UserResponse changePasswordById(long id, OverridePasswordRequest request);

    void mergeAdmins(List<UserSaveRequest> requests);

    void deleteById(long id);

    void deleteByNickname(String nickname);

}
