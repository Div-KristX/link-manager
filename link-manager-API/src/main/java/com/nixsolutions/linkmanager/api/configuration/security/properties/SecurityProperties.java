package com.nixsolutions.linkmanager.api.configuration.security.properties;

import java.util.Map;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;

@Validated
@ConfigurationProperties(prefix = "link-manager.security")
public class SecurityProperties {
    @Valid
    @NestedConfigurationProperty
    private JWTProperties jwt;

    private Map<@NotBlank String, @Valid AdminProperties> admins;

    public JWTProperties getJwt() {
        return jwt;
    }

    public void setJwt(JWTProperties jwt) {
        this.jwt = jwt;
    }

    public Map<String, AdminProperties> getAdmins() {
        return admins;
    }

    public void setAdmins(Map<String, AdminProperties> admins) {
        this.admins = admins;
    }

}
