package com.nixsolutions.linkmanager.api.service.auth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.nixsolutions.linkmanager.api.configuration.security.properties.JWTProperties;
import com.nixsolutions.linkmanager.api.configuration.security.properties.SecurityProperties;
import com.nixsolutions.linkmanager.api.exception.auth.InvalidRefreshTokenException;
import com.nixsolutions.linkmanager.api.model.auth.LinkManagerUserDetails;
import com.nixsolutions.linkmanager.api.model.auth.response.AccessTokenResponse;
import com.nixsolutions.linkmanager.api.model.user.User;
import com.nixsolutions.linkmanager.api.model.user.UserStatus;
import com.nixsolutions.linkmanager.api.configuration.security.SecurityConstants;
import com.nixsolutions.linkmanager.api.model.auth.RefreshToken;
import com.nixsolutions.linkmanager.api.repository.RefreshTokenRepository;
import com.nixsolutions.linkmanager.api.repository.UserRepository;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.REPEATABLE_READ)
public class JWTService implements AuthCRUD {

    private static final Logger log = LoggerFactory.getLogger(JWTService.class);
    private final RefreshTokenRepository refreshTokenRepository;

    private final UserRepository userRepository;

    private final Duration jwtExpiration;

    private final Duration refreshExpiration;

    private final Algorithm algorithm;

    public JWTService(SecurityProperties securityProperties,
        RefreshTokenRepository refreshTokenRepository,
        UserRepository userRepository) {
        this.refreshTokenRepository = refreshTokenRepository;
        this.userRepository = userRepository;
        JWTProperties jwtProperties = securityProperties.getJwt();
        this.jwtExpiration = jwtProperties.getAccessTime();
        this.refreshExpiration = jwtProperties.getRefreshTime();
        this.algorithm = Algorithm.HMAC512(new String(jwtProperties.getSecret()).getBytes());
    }

    @Override
    public AccessTokenResponse getToken(LinkManagerUserDetails userDetails) {
        RefreshToken newToken = issueRefreshToken(userDetails.getSource());
        return response(userDetails.getUsername(), userDetails.getAuthorities(), newToken);
    }

    @Override
    public AccessTokenResponse refreshToken(String refreshToken)
        throws InvalidRefreshTokenException {

        RefreshToken storedToken = refreshTokenRepository.findIfValid(
            verifyRefreshToken(refreshToken),
            OffsetDateTime.now(),
            UserStatus.ACTIVE
        ).orElseThrow(InvalidRefreshTokenException::new);

        checkIfRotated(storedToken);

        User user = storedToken.getUser();

        RefreshToken nextToken = issueRefreshToken(user);

        refreshTokenRepository.updateChain(storedToken, nextToken);

        return response(user.getNickname(), user.getAuthorities().keySet(), nextToken);
    }

    @Override
    public void invalidateToken(String refreshToken, String ownerNickname)
        throws InvalidRefreshTokenException {
        RefreshToken storedToken = refreshTokenRepository.findById(verifyRefreshToken(refreshToken))
            .orElseThrow(InvalidRefreshTokenException::new);
        checkOwner(storedToken, ownerNickname);
        checkIfRotated(storedToken);
        refreshTokenRepository.deleteChain(storedToken);
    }

    @Override
    public void pruneRefreshTokens(){
        refreshTokenRepository.pruneRefreshTokens();
    }

    private void checkOwner(RefreshToken storedToken, String nickname)
        throws InvalidRefreshTokenException {
        User user = storedToken.getUser();
        if (!user.getNickname().equals(nickname)) {
            // suspend the nasty-ass token pilferer
            String message = """
                User {} engaged in a suspicious activity, trying to use a refresh token issued to another user.
                Blocking the suspicious actor's account pending investigation!
                    """;
            log.error(message, nickname);
            userRepository.changeStatusByNickname(nickname, UserStatus.SUSPENDED);
            // invalidate token
            refreshTokenRepository.deleteChain(storedToken);
            throw new InvalidRefreshTokenException();
        }
    }

    private void checkIfRotated(RefreshToken storedToken) throws InvalidRefreshTokenException {
        // if an old token is used - we still want to invalidate whole chain in case the new one was stolen
        if (storedToken.getNext() != null) {
            String message = """
                An old refresh token used for user {}, signifying possible token theft! Invalidating the entire token chain.
                """;
            log.error(message, storedToken.getUser().getNickname());
            refreshTokenRepository.deleteChain(storedToken.getNext());
            throw new InvalidRefreshTokenException();
        }
    }

    private RefreshToken issueRefreshToken(User user) {
        RefreshToken refreshToken = new RefreshToken();
        OffsetDateTime now = OffsetDateTime.now();
        refreshToken.setIssuedAt(now);
        refreshToken.setExpireAt(now.plus(refreshExpiration));
        refreshToken.setUser(user);
        return refreshTokenRepository.save(refreshToken);
    }

    private AccessTokenResponse response(String subject,
        Collection<? extends GrantedAuthority> authorities,
        RefreshToken refreshToken) {
        String accessToken = issueJWT(subject, authorities);
        return new AccessTokenResponse(
            accessToken,
            signRefreshToken(refreshToken),
            jwtExpiration.toSeconds()
        );
    }

    private UUID verifyRefreshToken(String refreshJWT) throws InvalidRefreshTokenException {
        try {
            String id = JWT.require(algorithm)
                .build()
                .verify(refreshJWT)
                .getId();
            Objects.requireNonNull(id, "jti must be present in refresh token");
            return UUID.fromString(id);
        } catch (Exception e) {
            throw new InvalidRefreshTokenException(e);
        }
    }

    private String signRefreshToken(RefreshToken token) {
        return JWT.create()
            .withSubject(token.getUser().getNickname())
            .withJWTId(token.getValue().toString())
            .withIssuedAt(Date.from(token.getIssuedAt().toInstant()))
            .withExpiresAt(Date.from(token.getExpireAt().toInstant()))
            .sign(algorithm);
    }

    private String issueJWT(String subject, Collection<? extends GrantedAuthority> authorities) {
        long issuedAt = System.currentTimeMillis();
        return JWT.create()
            .withSubject(subject)
            .withIssuedAt(new Date(issuedAt))
            .withExpiresAt(new Date(issuedAt + jwtExpiration.toMillis()))
            .withArrayClaim(SecurityConstants.AUTHORITIES_CLAIM, authorities.stream()
                .map(GrantedAuthority::getAuthority)
                .toArray(String[]::new))
            .sign(algorithm);
    }

}
