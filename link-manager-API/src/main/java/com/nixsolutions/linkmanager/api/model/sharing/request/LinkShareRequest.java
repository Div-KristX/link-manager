package com.nixsolutions.linkmanager.api.model.sharing.request;

import com.nixsolutions.linkmanager.api.model.validation.UniqueNamesToCreateInvite;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@UniqueNamesToCreateInvite
public record LinkShareRequest(

    @NotBlank
    String originUser,
    @NotBlank
    String destinationUser,

    @Positive
    Long linkId

) {

}
