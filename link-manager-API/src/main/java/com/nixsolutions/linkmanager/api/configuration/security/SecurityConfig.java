package com.nixsolutions.linkmanager.api.configuration.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nixsolutions.linkmanager.api.Endpoints;
import com.nixsolutions.linkmanager.api.configuration.security.filters.JWTAuthenticationFilter;
import com.nixsolutions.linkmanager.api.configuration.security.filters.JWTAuthorizationFilter;
import com.nixsolutions.linkmanager.api.configuration.security.properties.SecurityProperties;
import com.nixsolutions.linkmanager.api.model.user.request.UserSaveRequest;
import com.nixsolutions.linkmanager.api.service.user.UserCRUD;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;


@Configuration
@EnableWebSecurity  
@EnableConfigurationProperties(SecurityProperties.class)
public class SecurityConfig {

    private static final Logger log = LoggerFactory.getLogger(SecurityConfig.class);

    private final SecurityProperties securityProperties;

    private final UserCRUD userService;

    private final AuthenticationConfiguration authenticationConfiguration;

    private final ObjectMapper objectMapper;

    public SecurityConfig(SecurityProperties securityProperties, UserCRUD userService,
        AuthenticationConfiguration authenticationConfiguration, ObjectMapper objectMapper) {
        this.securityProperties = securityProperties;
        this.userService = userService;
        this.authenticationConfiguration = authenticationConfiguration;
        this.objectMapper = objectMapper;
    }

    @PostConstruct
    public void init() {
        setupDefaultAdmins();
    }

    private void setupDefaultAdmins() {
        List<UserSaveRequest> requests = securityProperties.getAdmins().values().stream()
            .map(adminProperties -> new UserSaveRequest(
                adminProperties.getNickname(), new String(adminProperties.getPassword()
            )))
            .peek(admin -> log.info("Default admin found: {} ", admin.nickname()))
            .collect(Collectors.toList());
        userService.mergeAdmins(requests);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        return http.authorizeRequests()
            // open static resources
            .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
            // open swagger-ui
            .antMatchers("/v3/api-docs/**", "/swagger-ui/**", "/swagger-ui.html").permitAll()
            // allow user to see response from {account-types} and {link-types} without authorisation
            .antMatchers(HttpMethod.GET, Endpoints.ACCOUNT_TYPES + "/**").permitAll()

            .antMatchers(HttpMethod.GET, Endpoints.LINK_TAG + "/**").permitAll()

            // allow user registration and refresh, ignore authorization filters on login
            .antMatchers(HttpMethod.POST, Endpoints.USERS, Endpoints.TOKEN + "/refresh").permitAll()
            // admin can register new admins
            .antMatchers(HttpMethod.POST, Endpoints.USERS + "/admins").hasRole("ADMIN")

            // admin can delete old tokens
            .antMatchers(HttpMethod.DELETE, Endpoints.TOKEN + "/old-tokens").hasRole("ADMIN")
            // admin can change user`s password, delete a user, get the users by account-type
            .antMatchers(HttpMethod.PATCH, Endpoints.USERS + "/{id:\\d+}/**").hasRole("ADMIN")
            .antMatchers(HttpMethod.GET, Endpoints.USERS + "/{id:\\d+}/links").hasRole("ADMIN")
            .antMatchers(HttpMethod.DELETE, Endpoints.USERS + "/{id:\\d+}").hasRole("ADMIN")

            // admin can edit/delete/register an account-type
            .antMatchers(HttpMethod.PUT, Endpoints.ACCOUNT_TYPES + "/{id:\\d+}").hasRole("ADMIN")
            .antMatchers(Endpoints.ACCOUNT_TYPES + "/{id:\\d+}").hasRole("ADMIN")
            .antMatchers(HttpMethod.POST, Endpoints.ACCOUNT_TYPES).hasRole("ADMIN")

            // admin can edit/delete/register a link-type
            .antMatchers(HttpMethod.PUT, Endpoints.LINK_TAG + "/{id:\\d+}").hasRole("ADMIN")
            .antMatchers(HttpMethod.DELETE, Endpoints.LINK_TAG + "/{id:\\d+}").hasRole("ADMIN")
            .antMatchers(HttpMethod.POST, Endpoints.LINK_TAG).hasRole("ADMIN")

            // admin can use Actuator endpoints
            .requestMatchers(EndpointRequest.toAnyEndpoint()).hasRole("ADMIN")
            // by default, require authentication
            .anyRequest().authenticated()
            .and()
            // auth filter
            .addFilter(jwtAuthenticationFilter())
            // jwt-verification filter
            .addFilterAfter(jwtAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
            // for unauthorized requests return 401
            .exceptionHandling()
            .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))
            .and()
            // allow cross-origin requests for all endpoints
            .cors().configurationSource(corsConfigurationSource())
            .and()
            .csrf().disable()
            // this disables session creation on Spring Security
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and().build();
    }


    @Bean
    public AuthenticationManager authenticationManager(
        AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    private JWTAuthenticationFilter jwtAuthenticationFilter() throws Exception {
        var filter = new JWTAuthenticationFilter(authenticationManager(authenticationConfiguration),
            objectMapper);
        filter.setFilterProcessesUrl(Endpoints.TOKEN);
        return filter;
    }

    private JWTAuthorizationFilter jwtAuthorizationFilter() {
        return new JWTAuthorizationFilter(securityProperties.getJwt());
    }

    private CorsConfigurationSource corsConfigurationSource() {
        var source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }
}
