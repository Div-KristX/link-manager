package com.nixsolutions.linkmanager.api.service.tag;


import com.nixsolutions.linkmanager.api.exception.LinkManagerExceptions;
import com.nixsolutions.linkmanager.api.exception.auth.AuthenticationExceptions;
import com.nixsolutions.linkmanager.api.model.tag.Tag;
import com.nixsolutions.linkmanager.api.model.tag.request.TagRequest;
import com.nixsolutions.linkmanager.api.model.tag.response.TagResponse;
import com.nixsolutions.linkmanager.api.model.user.KnownAuthority;
import com.nixsolutions.linkmanager.api.repository.TagRepository;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.REPEATABLE_READ)
public class TagService implements TagCRUD {

    private final TagRepository tagRepository;

    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public Page<TagResponse> findAll(Pageable pageable) {
        return tagRepository.findAll(pageable).map(TagResponse::fromTag);
    }

    @Override
    public TagResponse create(TagRequest request, Authentication authentication) {
        if (!isAdmin(authentication)) {
            throw AuthenticationExceptions.noAccessRightsToCreateLinkType(
                authentication.getPrincipal().toString());
        }

        validateUniqueFields(request);

        Tag tag = new Tag();
        tag.setTag(request.tag());
        return TagResponse.fromTag(tagRepository.save(tag));
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<TagResponse> getById(Long id) {
        return tagRepository.findById(id).map(TagResponse::fromTag);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<TagResponse> getByName(String name, Pageable pageable) {
        return tagRepository.findByTagLikeIgnoreCase(name, pageable)
            .map(TagResponse::fromTag);
    }

    @Override
    public void update(Long id, TagRequest request, Authentication authentication) {
        Tag tag = findTag(id);
        checkAccessRights(authentication, tag);
        validateUniqueFields(request);
        tag.setTag(request.tag());
    }

    @Override
    public void deleteById(Long id, Authentication authentication) {
        Tag tag = findTag(id);
        checkAccessRights(authentication, tag);
        tagRepository.deleteById(id);
    }

    private Tag findTag(long id) {
        return tagRepository.findById(id)
            .orElseThrow(
                () -> LinkManagerExceptions.tagNotFound(id)
            );
    }

    private boolean isAdmin(Authentication authentication) {
        return authentication.getAuthorities().contains(KnownAuthority.ROLE_ADMIN);
    }

    private void validateUniqueFields(TagRequest request) {
        String linkType = request.tag();
        if (tagRepository.existsByTagIgnoreCase(linkType)) {
            throw LinkManagerExceptions.duplicateTag(linkType);
        }
    }

    private void checkAccessRights(Authentication authentication, Tag tag) {
        String nickname = authentication.getPrincipal().toString();
        boolean hasRights = isAdmin(authentication);
        if (hasRights) {
            return;
        }
        throw AuthenticationExceptions.noAccessRightsToTag(nickname, tag.getId());
    }
}
