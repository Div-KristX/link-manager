package com.nixsolutions.linkmanager.api.model.auth.request;

import com.fasterxml.jackson.annotation.JsonAlias;

public record SignInRequest(
    @JsonAlias({"nickname", "login"})
    String login,
    String password
) {

}
