package com.nixsolutions.linkmanager.api.model.link;

import com.nixsolutions.linkmanager.api.model.tag.Tag;
import com.nixsolutions.linkmanager.api.model.user.User;
import com.nixsolutions.linkmanager.api.model.sharing.LinkShare;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "links")
public class Link implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(nullable = false)
    private String hyperlink;

    @Column(nullable = false)
    private String description;


    @JoinColumn(name = "tag_id")
    @ManyToOne(optional = false)
    private Tag tag;

    @Column(name = "is_important", nullable = false)
    private Boolean isImportant;


    @JoinColumn(name = "user_id")
    @ManyToOne(optional = false)
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "sharedLink", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<LinkShare> sharesIn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHyperlink() {
        return hyperlink;
    }

    public void setHyperlink(String hyperlink) {
        this.hyperlink = hyperlink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getImportant() {
        return isImportant;
    }

    public void setImportant(Boolean important) {
        isImportant = important;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User userManager) {
        this.user = userManager;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public List<LinkShare> getSharesIn() {
        return sharesIn;
    }

    public void setSharesIn(List<LinkShare> sharesIn) {
        this.sharesIn = sharesIn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Link link = (Link) o;
        return Objects.equals(hyperlink, link.hyperlink) && Objects.equals(
            description, link.description) && Objects.equals(tag, link.tag)
            && Objects.equals(isImportant, link.isImportant) && Objects.equals(user,
            link.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hyperlink, description, tag, isImportant, user);
    }
}
