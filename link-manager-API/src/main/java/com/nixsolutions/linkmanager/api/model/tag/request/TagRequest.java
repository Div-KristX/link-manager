package com.nixsolutions.linkmanager.api.model.tag.request;

import javax.validation.constraints.NotBlank;

public record TagRequest(
    @NotBlank(message = "Tag can`t be null") String tag
) {

}
