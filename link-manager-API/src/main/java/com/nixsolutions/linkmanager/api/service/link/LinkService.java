package com.nixsolutions.linkmanager.api.service.link;

import com.nixsolutions.linkmanager.api.exception.LinkManagerExceptions;
import com.nixsolutions.linkmanager.api.exception.auth.AuthenticationExceptions;
import com.nixsolutions.linkmanager.api.model.link.Link;
import com.nixsolutions.linkmanager.api.model.link.request.LinkSaveRequest;
import com.nixsolutions.linkmanager.api.model.link.request.LinkUpdateRequest;
import com.nixsolutions.linkmanager.api.model.link.response.LinkResponse;
import com.nixsolutions.linkmanager.api.model.tag.Tag;
import com.nixsolutions.linkmanager.api.model.user.KnownAuthority;
import com.nixsolutions.linkmanager.api.model.user.User;
import com.nixsolutions.linkmanager.api.repository.LinkRepository;
import com.nixsolutions.linkmanager.api.repository.TagRepository;
import com.nixsolutions.linkmanager.api.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(isolation = Isolation.REPEATABLE_READ)
public class LinkService implements LinkCRUD {

    private final LinkRepository linkRepository;
    private final UserRepository userRepository;
    private final TagRepository tagRepository;

    public LinkService(LinkRepository linkRepository, UserRepository userRepository,
        TagRepository tagRepository) {
        this.linkRepository = linkRepository;
        this.userRepository = userRepository;
        this.tagRepository = tagRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LinkResponse> getOwnLinks(long userId, Pageable pageable) {
        User user = findUser(userId);
        return linkRepository.findAllByUserOrderByIsImportantDesc(user, pageable).map(LinkResponse::fromLink);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LinkResponse> getOwnLinks(Authentication authentication, Pageable pageable) {
        User user = findUser(authentication);
        return linkRepository.findAllByUserOrderByIsImportantDesc(user, pageable).map(LinkResponse::fromLink);
    }

    @Override
    @Transactional(readOnly = true)
    public LinkResponse getLinkById(Authentication authentication, Long id) {
        User user = findUser(authentication);
        return LinkResponse.fromLink(linkRepository.findByUserAndId(user, id).orElseThrow(
            () -> LinkManagerExceptions.linkNotFound(id)
        ));
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LinkResponse> getOwnLinksByDescriptionOrHyperlink(String description,
        Authentication authentication, Pageable pageable) {
        User user = findUser(authentication);
        return linkRepository
            .findAllByUserAndHyperlinkOrDescription(user, description, pageable)
            .map(LinkResponse::fromLink);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LinkResponse> getOwnLinksByDescriptionAndLinkType(Long typeId, String keywords,
        Authentication authentication, Pageable pageable) {
        User user = findUser(authentication);
        Tag tag = findTagById(typeId);
        return linkRepository
            .findAllByUserHyperlinkOrDescriptionAndTag(user, tag, keywords, pageable)
            .map(LinkResponse::fromLink);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<LinkResponse> getOwnLinksByLinkTypeId(long linkTypeId,
        Authentication authentication, Pageable pageable) {
        User user = findUser(authentication);
        Tag tag = findTagById(linkTypeId);
        return linkRepository.findAllByUserAndTag(user, tag, pageable)
            .map(LinkResponse::fromLink);
    }

    @Override
    public LinkResponse saveLink(LinkSaveRequest request, Authentication authentication) {
        User user = findUser(authentication);
        Integer maxLinks = user.getAccountType().getMaxLinks();
        if (user.getUsedLinks() >= maxLinks) {
            throw LinkManagerExceptions.userLinkCreationLimit(maxLinks);
        }
        Tag tag = findTagById(request.tag());
        Link userLink = new Link();
        userLink.setUser(user);
        userLink.setTag(tag);
        userLink.setHyperlink(request.hyperlink());
        userLink.setImportant(request.important());
        userLink.setDescription(request.description());
        LinkResponse response = LinkResponse.fromLink(linkRepository.save(userLink));
        user.addLink(userLink);
        return response;
    }

    @Override
    public LinkResponse updateLinkById(long id, LinkUpdateRequest request,
        Authentication authentication) {

        Link userLink = findLink(id);

        checkAccessRights(authentication, userLink);

        Tag tag = findTagById(request.tag());

        userLink.setTag(tag);
        userLink.setHyperlink(request.hyperlink());
        userLink.setImportant(request.important());
        userLink.setDescription(request.description());

        return LinkResponse.fromLink(linkRepository.save(userLink));
    }

    @Override
    public void deleteById(long id, Authentication authentication) {
        User user = findUser(authentication);
        Link link = findLink(id);
        checkAccessRights(authentication, link);
        user.removeLink(link);
        linkRepository.deleteById(id);
    }


    private boolean isOwner(String nickname, Link link) {
        return nickname.equals(link.getUser().getNickname());
    }

    private boolean isAdmin(Authentication authentication) {
        return authentication.getAuthorities().contains(KnownAuthority.ROLE_ADMIN);
    }

    private void checkAccessRights(Authentication authentication, Link link) {
        String nickname = authentication.getPrincipal().toString();
        boolean hasRights = isAdmin(authentication) || isOwner(nickname, link);
        if (hasRights) {
            return;
        }
        throw AuthenticationExceptions.noAccessRightsToLink(nickname, link.getId());
    }

    private User findUser(long id) {
        return userRepository.findById(id)
            .orElseThrow(() -> LinkManagerExceptions.userNotFound(id));
    }

    private User findUser(Authentication authentication) {
        String nickname = authentication.getPrincipal().toString();
        return userRepository.findByNickname(nickname)
            .orElseThrow(() -> LinkManagerExceptions.userNotFound(nickname));
    }

    private Tag findTagById(long id) {
        return tagRepository.findById(id)
            .orElseThrow(() -> LinkManagerExceptions.tagNotFound(id));
    }

    private Link findLink(long id) {
        return linkRepository.findById(id)
            .orElseThrow(() -> LinkManagerExceptions.linkNotFound(id));
    }
}
