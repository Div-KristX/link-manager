package com.nixsolutions.linkmanager.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.nixsolutions.linkmanager.api.model.auth.request.RefreshTokenRequest;
import com.nixsolutions.linkmanager.api.model.auth.request.SignInRequest;
import com.nixsolutions.linkmanager.api.model.auth.response.AccessTokenResponse;
import com.nixsolutions.linkmanager.api.model.tag.request.TagRequest;
import com.nixsolutions.linkmanager.api.model.tag.response.TagResponse;
import com.nixsolutions.linkmanager.api.model.user.request.UserSaveRequest;
import com.nixsolutions.linkmanager.api.model.user.response.UserResponse;
import java.util.Collections;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ActiveProfiles({"postgresDb", "debug"})
@Testcontainers
class LinkManagerApplicationTest {

    @Container
    private static final PostgreSQLContainer container = new PostgreSQLContainer("postgres:latest")
        .withPassword("sa")
        .withUsername("sa")
        .withDatabaseName("linkAppTest");
    @Autowired
    private TestRestTemplate restTemplate;


    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", container::getJdbcUrl);
        registry.add("spring.datasource.password", container::getPassword);
        registry.add("spring.datasource.username", container::getUsername);
    }

    @Test
    void CreatedUserISDummy_NotNull_HasId() {
        String nickname = "dummy";
        String password = "dummy123";
        ResponseEntity<UserResponse> response = createUser(nickname, password);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType());

        UserResponse createdUser = response.getBody();

        assertNotNull(createdUser);
        assertEquals(nickname, createdUser.nickname());
        assertNotNull(createdUser.id());
    }

    @Test
    void CreatingAdminWithoutAuthentication_Is_UNAUTHORIZED_Status() {
        String nickname = "IoPon231";
        String password = "ddamm230za//";
        ResponseEntity<UserResponse> response = createAdmin(nickname, password);
        assertEquals(HttpStatus.UNAUTHORIZED, response.getStatusCode());
    }

    @Test
    void tryToCreateIncorrectUser() {
        String nickname = "root";
        String password = "djasdklx";

        ResponseEntity<?> responseForDuplicate = createInvalidUser(nickname, password);
        assertEquals(HttpStatus.BAD_REQUEST, responseForDuplicate.getStatusCode());

        nickname = " ";
        password = " ";

        ResponseEntity<?> responseForIncorrect = createInvalidUser(nickname, password);
        assertEquals(HttpStatus.BAD_REQUEST, responseForIncorrect.getStatusCode());
    }

    @Test
    void onlyAdminCanCreateTag() {

        String nickname = "IoPon231";
        String password = "ddamm230za//";

        TagRequest saveTag = new TagRequest("Useful");

        // user
        ResponseEntity<UserResponse> responseUser = createUser(nickname, password);

        assertEquals(HttpStatus.CREATED, responseUser.getStatusCode());

        assertEquals(MediaType.APPLICATION_JSON, responseUser.getHeaders().getContentType());

        SignInRequest signInRequest = new SignInRequest(nickname, password);

        ResponseEntity<AccessTokenResponse> loginUser = restTemplate.postForEntity(Endpoints.TOKEN,
            signInRequest, AccessTokenResponse.class);


        assertEquals(HttpStatus.OK, loginUser.getStatusCode());

        assertEquals(MediaType.APPLICATION_JSON, loginUser.getHeaders().getContentType());

        HttpHeaders forbiddenUserHead = headers(loginUser.getBody().accessToken());

        HttpEntity<TagRequest> userContent = tagContentCreate(saveTag, forbiddenUserHead);

        ResponseEntity<TagResponse> responseLinkTypeFailure = creationResponse(userContent);

        assertEquals(HttpStatus.FORBIDDEN, responseLinkTypeFailure.getStatusCode());

        // admin

        String adminLogin = "root";
        String adminPassword = "$2a$10$8OMmVRz/.1T/ZZClbRz.ze9KMr2EycsYjVAINzvnRbCHRZm2/0bi.";

        SignInRequest adminSignInRequest = new SignInRequest(adminLogin, adminPassword);

        ResponseEntity<AccessTokenResponse> loginAdmin = restTemplate.postForEntity(Endpoints.TOKEN,
            adminSignInRequest, AccessTokenResponse.class);

        assertEquals(HttpStatus.OK, loginAdmin.getStatusCode());

        assertEquals(MediaType.APPLICATION_JSON, loginAdmin.getHeaders().getContentType());

        HttpHeaders adminHead = headers(loginAdmin.getBody().accessToken());

        adminHead.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<TagRequest> adminContent = tagContentCreate(saveTag, adminHead);

        ResponseEntity<TagResponse> responseTagSuccess = creationResponse(adminContent);

        assertEquals(HttpStatus.CREATED, responseTagSuccess.getStatusCode());

        // created tag

        assertEquals(saveTag.tag(), responseTagSuccess.getBody().tag());

        HttpHeaders userHead = headers(loginUser.getBody().accessToken());

        HttpEntity<TagResponse> createdLinkType = new HttpEntity<>(userHead);

        ResponseEntity<TagResponse> postedTag = restTemplate.exchange(
            Endpoints.LINK_TAG + "/" + responseTagSuccess.getBody().id(), HttpMethod.GET,
            createdLinkType,
            TagResponse.class);

        assertEquals(postedTag.getStatusCode(), HttpStatus.OK);
        assertEquals(saveTag.tag(), postedTag.getBody().tag());

        // refreshing token
        RefreshTokenRequest entity = new RefreshTokenRequest(loginAdmin.getBody().refreshToken());
        HttpHeaders adminTokenRefresh = new HttpHeaders();

        adminTokenRefresh.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        adminTokenRefresh.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<RefreshTokenRequest> request = new HttpEntity<>(entity, adminTokenRefresh);

        ResponseEntity<AccessTokenResponse> newTokens = restTemplate.exchange(
            Endpoints.TOKEN + "/refresh",
            HttpMethod.POST,
            request,
            AccessTokenResponse.class
        );

        // after refreshing

        assertEquals(newTokens.getStatusCode(), HttpStatus.OK);

        assertNotEquals(newTokens.getBody(), loginAdmin.getBody());

        HttpHeaders newAdminHead = headers(newTokens.getBody().accessToken());

        HttpEntity<TagResponse> responseTag = new HttpEntity<>(newAdminHead);

        ResponseEntity<TagResponse> getLinkType = restTemplate.exchange(
            Endpoints.LINK_TAG + "/" + responseTagSuccess.getBody().id(), HttpMethod.GET,
            responseTag,
            TagResponse.class);

        assertEquals(getLinkType.getStatusCode(), HttpStatus.OK);


    }

    private HttpHeaders headers(String token) {
        HttpHeaders authenticationHead = new HttpHeaders();
        authenticationHead.add("Authorization", "Bearer " + token);
        authenticationHead.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return authenticationHead;
    }

    private HttpEntity<TagRequest> tagContentCreate(TagRequest request,
        HttpHeaders headers) {
        return new HttpEntity<>(request, headers);
    }


    private ResponseEntity<TagResponse> creationResponse(HttpEntity<TagRequest> entity) {
        return restTemplate.exchange(
            Endpoints.LINK_TAG, HttpMethod.POST, entity, TagResponse.class);
    }

    private ResponseEntity<UserResponse> createUser(String nickname, String password) {
        UserSaveRequest requestBody = new UserSaveRequest(nickname, password);
        return restTemplate.postForEntity(Endpoints.USERS, requestBody, UserResponse.class);
    }

    private ResponseEntity<String> createInvalidUser(String nickname, String password) {
        UserSaveRequest requestBody = new UserSaveRequest(nickname, password);
        return restTemplate.postForEntity(Endpoints.USERS, requestBody, String.class);
    }

    private ResponseEntity<UserResponse> createAdmin(String nickname, String password) {
        UserSaveRequest requestBody = new UserSaveRequest(nickname, password);
        return restTemplate.postForEntity(Endpoints.USERS + "/admins", requestBody,
            UserResponse.class);
    }
}