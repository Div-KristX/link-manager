package com.nixsolutions.linkmanager.api.service.account;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.nixsolutions.linkmanager.api.exception.LinkManagerExceptions;
import com.nixsolutions.linkmanager.api.model.account.AccountType;
import com.nixsolutions.linkmanager.api.model.account.request.AccountTypeRequest;
import com.nixsolutions.linkmanager.api.model.account.response.AccountTypeResponse;
import com.nixsolutions.linkmanager.api.model.user.KnownAuthority;
import com.nixsolutions.linkmanager.api.repository.AccountTypeRepository;
import com.nixsolutions.linkmanager.api.service.AuthPrincipal;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.server.ResponseStatusException;

class AccountTypeServiceTest {

    private AccountTypeRepository accountTypeRepository;

    private AccountTypeCRUD accountTypeCRUD;

    @BeforeEach
    void setUp() {
        accountTypeRepository = mock(AccountTypeRepository.class);
        accountTypeCRUD = new AccountTypeService(accountTypeRepository);
    }

    @Test
    public void createAccounts() {
        String username = "root";

        Authentication userAuth = new UsernamePasswordAuthenticationToken(
            AuthPrincipal.principal(username), null,
            List.of(KnownAuthority.ROLE_USER, KnownAuthority.ROLE_ADMIN));

        AccountType defaultAccount = new AccountType();

        defaultAccount.setId(1L);
        defaultAccount.setName("Default Account");
        defaultAccount.setMaxLinks(10);

        // Create account-type

        AccountTypeRequest request = new AccountTypeRequest("Account", 15);

        when(accountTypeRepository.existsByNameIgnoreCase(notNull())).thenAnswer(
            answer -> {
                String name = answer.getArgument(0);
                return name.equalsIgnoreCase(defaultAccount.getName());
            }
        );

        when(accountTypeRepository.save(notNull())).thenAnswer(
            answer -> {
                AccountType requestedAccount = answer.getArgument(0);
                assertNull(requestedAccount.getId());
                assertEquals(requestedAccount.getName(), request.name());
                assertEquals(requestedAccount.getMaxLinks(), request.maxLinks());
                requestedAccount.setId(2L);
                return requestedAccount;
            }
        );

        AccountTypeResponse response = accountTypeCRUD.create(request, userAuth);

        assertEquals(request.name(), response.name());

        assertNotNull(response.id());

        AccountTypeRequest duplicateRequest = new AccountTypeRequest("default account", 12);

        assertThrows(ResponseStatusException.class,
            () -> accountTypeCRUD.create(duplicateRequest, userAuth),
            LinkManagerExceptions.duplicateAccountType(duplicateRequest.name()).getMessage());
    }

    @Test
    public void updateAccountType() {

        String previousName = "Default Account";
        int previousMaxLinks = 10;

        Authentication userAuth = new UsernamePasswordAuthenticationToken(
            AuthPrincipal.principal("root"), null,
            List.of(KnownAuthority.ROLE_USER, KnownAuthority.ROLE_ADMIN));

        AccountType defaultAccount = new AccountType();

        defaultAccount.setId(1L);
        defaultAccount.setName(previousName);
        defaultAccount.setMaxLinks(previousMaxLinks);

        AccountTypeRequest request = new AccountTypeRequest("Account", 15);

        when(accountTypeRepository.existsByNameIgnoreCase(notNull())).thenAnswer(
            answer -> {
                String name = answer.getArgument(0);
                return name.equalsIgnoreCase(defaultAccount.getName());
            }
        );

        when(accountTypeRepository.findById(notNull())).thenAnswer(
            answer -> {
                Long id = answer.getArgument(0);
                if (id.equals(defaultAccount.getId())) {
                    return Optional.of(defaultAccount);
                } else {
                    return Optional.empty();
                }
            }
        );

        accountTypeCRUD.update(defaultAccount.getId(), request, userAuth);

        assertNotEquals(previousName, defaultAccount.getName());
        assertNotEquals(previousMaxLinks, defaultAccount.getMaxLinks());

    }
}