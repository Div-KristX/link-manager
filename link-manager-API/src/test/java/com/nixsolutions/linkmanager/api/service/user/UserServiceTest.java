package com.nixsolutions.linkmanager.api.service.user;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.nixsolutions.linkmanager.api.exception.LinkManagerExceptions;
import com.nixsolutions.linkmanager.api.exception.auth.AuthenticationExceptions;
import com.nixsolutions.linkmanager.api.model.account.AccountType;
import com.nixsolutions.linkmanager.api.model.user.KnownAuthority;
import com.nixsolutions.linkmanager.api.model.user.User;
import com.nixsolutions.linkmanager.api.model.user.UserAuthority;
import com.nixsolutions.linkmanager.api.model.user.UserStatus;
import com.nixsolutions.linkmanager.api.model.user.request.ChangeUserAccountRequest;
import com.nixsolutions.linkmanager.api.model.user.request.ChangeUserPasswordRequest;
import com.nixsolutions.linkmanager.api.model.user.request.ChangeUserStatusRequest;
import com.nixsolutions.linkmanager.api.model.user.request.UserSaveRequest;
import com.nixsolutions.linkmanager.api.model.user.response.UserResponse;
import com.nixsolutions.linkmanager.api.repository.AccountTypeRepository;
import com.nixsolutions.linkmanager.api.repository.AuthorityRepository;
import com.nixsolutions.linkmanager.api.repository.UserRepository;
import java.security.SecureRandom;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.server.ResponseStatusException;

public class UserServiceTest {

    private UserCRUD userCRUD;

    private AuthorityRepository authorityRepository;

    private UserRepository userRepository;

    private AccountTypeRepository accountTypeRepository;

    private PasswordEncoder passwordEncoder;

    private UserAuthority defaultAuthority;

    private UserAuthority adminAuthority;

    private AccountType defaultAccountType;

    private AccountType accountTypeWithOneLink;


    @BeforeEach
    void setUp() {
        userRepository = mock(UserRepository.class);
        authorityRepository = mock(AuthorityRepository.class);
        accountTypeRepository = mock(AccountTypeRepository.class);
        userRepository = mock(UserRepository.class);
        passwordEncoder = new BCryptPasswordEncoder(10, new SecureRandom());

        userCRUD = new UserService(
            userRepository,
            accountTypeRepository,
            authorityRepository,
            passwordEncoder
        );

        defaultAuthority = new UserAuthority();

        defaultAuthority.setId(KnownAuthority.ROLE_USER);

        adminAuthority = new UserAuthority();

        adminAuthority.setId(KnownAuthority.ROLE_ADMIN);

        defaultAccountType = new AccountType();

        defaultAccountType.setId(1L);
        defaultAccountType.setName("Default Account");
        defaultAccountType.setMaxLinks(10);

        accountTypeWithOneLink = new AccountType();
        accountTypeWithOneLink.setId(2L);
        accountTypeWithOneLink.setName("Account wit One link");
        accountTypeWithOneLink.setMaxLinks(1);
    }

    @Test
    void saveNewUser() {
        UserSaveRequest correctRequest = new UserSaveRequest("nickname", "ew231zx");
        long userId = 1;
        UserAuthority defaultUser = new UserAuthority();
        defaultUser.setId(KnownAuthority.ROLE_USER);
        when(authorityRepository.findById(KnownAuthority.ROLE_USER)).thenAnswer(
            answer -> {
                UserAuthority authority = new UserAuthority();
                authority.setId(KnownAuthority.ROLE_USER);
                return Optional.of(authority);
            }
        );

        when(accountTypeRepository.findById(1L)).thenAnswer(
            answer -> {
                AccountType account = new AccountType();
                account.setId(1L);
                account.setName("Default Account");
                return Optional.of(account);
            }
        );
        when(userRepository.save(notNull())).thenAnswer(
            answer -> {
                User user = answer.getArgument(0);
                assertThat(user.getId()).isNull();
                assertThat(user.getNickname()).isEqualTo(correctRequest.nickname());
                assertThat(user.getUsedLinks()).isEqualTo(0);
                user.setId(userId);
                return user;
            }
        );
        UserResponse response = userCRUD.create(correctRequest);

        assertThat(response.id()).isEqualTo(userId);

        assertThat(response.nickname()).isEqualTo(correctRequest.nickname());

        assertThat(response.status()).isEqualTo(UserStatus.ACTIVE);

        verify(userRepository, times(1)).save(notNull());
    }

    @Test
    void updateUser() {
        User dummyUser = getDummyUser();

        when(userRepository.findById(notNull())).thenAnswer(
            answer -> {
                Long id = answer.getArgument(0);
                if (id.equals(1L)) {
                    return Optional.of(dummyUser);
                } else {
                    return Optional.empty();
                }
            }
        );

        when(userRepository.findByNickname(notNull())).thenAnswer(
            answer -> {
                String nickname = answer.getArgument(0);
                if (nickname.equals(dummyUser.getNickname())) {
                    return Optional.of(dummyUser);
                } else {
                    return Optional.empty();
                }
            }
        );

        when(accountTypeRepository.findByName(notNull())).thenAnswer(
            answer -> {
                String name = answer.getArgument(0);
                if (name.equals(accountTypeWithOneLink.getName())) {
                    return Optional.of(accountTypeWithOneLink);
                } else {
                    return Optional.empty();
                }
            }
        );

        // change status

        assertThat(dummyUser.getStatus()).isEqualTo(UserStatus.ACTIVE);

        userCRUD.changeStatusById(dummyUser.getId(),
            new ChangeUserStatusRequest(UserStatus.SUSPENDED));

        assertThat(dummyUser.getStatus()).isEqualTo(UserStatus.SUSPENDED);

        userCRUD.changeStatusById(dummyUser.getId(),
            new ChangeUserStatusRequest(UserStatus.ACTIVE));

        assertThat(dummyUser.getStatus()).isEqualTo(UserStatus.ACTIVE);

        // change password

        String previousPassword = "12345678";

        String newPassword = "87654321";

        userCRUD.changePasswordByNickname(dummyUser.getNickname(),
            new ChangeUserPasswordRequest(previousPassword, newPassword));

        assertThat(passwordEncoder.matches(newPassword, dummyUser.getPassword())).isTrue();

        assertThat(previousPassword).isNotEqualTo(dummyUser.getPassword());

        assertThrows(ResponseStatusException.class, () ->
                userCRUD.changePasswordByNickname(dummyUser.getNickname(),
                    new ChangeUserPasswordRequest(previousPassword, newPassword)),
            AuthenticationExceptions.wrongPassword().getMessage());

        // change account-type

        assertThat(dummyUser.getUsedLinks()).isLessThanOrEqualTo(
            dummyUser.getAccountType().getMaxLinks());

        // setting used links greater than a requested account-type links capacity

        dummyUser.setUsedLinks(9);

        assertThrows(
            ResponseStatusException.class,
            () -> userCRUD.changeAccountTypeById(dummyUser.getId(),
                new ChangeUserAccountRequest(accountTypeWithOneLink.getName())),
            LinkManagerExceptions.userAccountChangeConflict(dummyUser.getNickname(),
                dummyUser.getUsedLinks(), dummyUser.getAccountType().getMaxLinks()).getMessage()
        );

        dummyUser.setUsedLinks(1);

        userCRUD.changeAccountTypeById(dummyUser.getId(),
            new ChangeUserAccountRequest(accountTypeWithOneLink.getName()));

        assertThat(dummyUser.getAccountType()).isEqualTo(accountTypeWithOneLink);

    }

    private User getDummyUser() {
        User dummyUser = new User();

        String password = "12345678";
        dummyUser.setId(1L);
        dummyUser.setNickname("dummy");
        dummyUser.setPassword(passwordEncoder.encode(password));
        dummyUser.setStatus(UserStatus.ACTIVE);
        dummyUser.setAccountType(defaultAccountType);
        dummyUser.setUsedLinks(0);
        dummyUser.setLinks(new LinkedList<>());
        dummyUser.setAuthorities(Map.of(KnownAuthority.ROLE_USER, defaultAuthority));

        return dummyUser;
    }

}
