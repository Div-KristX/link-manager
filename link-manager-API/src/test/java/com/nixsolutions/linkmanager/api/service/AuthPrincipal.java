package com.nixsolutions.linkmanager.api.service;

public class AuthPrincipal {

    private final String principal;

    public AuthPrincipal(String principal) {
        this.principal = principal;
    }

    @Override
    public String toString() {
        return principal;
    }

    public static Object principal(String principal) {
        return new AuthPrincipal(principal);
    }
}
