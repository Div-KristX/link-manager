package com.nixsolutions.linkmanager.api.service.link;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.nixsolutions.linkmanager.api.model.account.AccountType;
import com.nixsolutions.linkmanager.api.model.link.Link;
import com.nixsolutions.linkmanager.api.model.link.request.LinkSaveRequest;
import com.nixsolutions.linkmanager.api.model.link.request.LinkUpdateRequest;
import com.nixsolutions.linkmanager.api.model.link.response.LinkResponse;
import com.nixsolutions.linkmanager.api.model.tag.Tag;
import com.nixsolutions.linkmanager.api.model.user.KnownAuthority;
import com.nixsolutions.linkmanager.api.model.user.User;
import com.nixsolutions.linkmanager.api.model.user.UserAuthority;
import com.nixsolutions.linkmanager.api.model.user.UserStatus;
import com.nixsolutions.linkmanager.api.repository.LinkRepository;
import com.nixsolutions.linkmanager.api.repository.TagRepository;
import com.nixsolutions.linkmanager.api.repository.UserRepository;
import com.nixsolutions.linkmanager.api.service.AuthPrincipal;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

class LinkServiceTest {

    private LinkRepository linkRepository;

    private UserRepository userRepository;

    private TagRepository tagRepository;

    private LinkCRUD linkCRUD;

    private AccountType defaultAccountType;

    private UserAuthority defaultAuthority;

    @BeforeEach
    void setUp() {
        linkRepository = mock(LinkRepository.class);
        userRepository = mock(UserRepository.class);
        tagRepository = mock(TagRepository.class);

        linkCRUD = new LinkService(linkRepository, userRepository, tagRepository);

        defaultAccountType = new AccountType();

        defaultAccountType.setId(1L);
        defaultAccountType.setName("Default Account");
        defaultAccountType.setMaxLinks(10);

        defaultAuthority = new UserAuthority();

        defaultAuthority.setId(KnownAuthority.ROLE_USER);
    }


    @Test
    public void createLink() {

        Authentication userAuth = new UsernamePasswordAuthenticationToken(
            AuthPrincipal.principal("root"), null, List.of(KnownAuthority.ROLE_USER));

        User root = getDummyUser();

        when(userRepository.findByNickname(notNull())).thenAnswer(
            answer -> {
                String nickname = answer.getArgument(0);
                if (nickname.equalsIgnoreCase("root")) {
                    return Optional.of(root);
                } else {
                    return Optional.empty();
                }
            }
        );

        when(tagRepository.findById(notNull())).thenAnswer(
            answer -> {
                Long id = answer.getArgument(0);
                if (id.equals(1L)) {
                    Tag tag = new Tag();
                    tag.setId(1L);
                    tag.setTag("Start-page");
                    return Optional.of(tag);
                } else {
                    return Optional.empty();
                }
            }
        );

        when(linkRepository.save((notNull()))).thenAnswer(
            answer -> {
                Link link = answer.getArgument(0);
                assertNull(link.getId());
                link.setId(1L);
                return link;
            }
        );

        LinkSaveRequest request = new LinkSaveRequest("Nure.ua", "interesting", 1L, false);

        LinkResponse response = linkCRUD.saveLink(request, userAuth);

        assertEquals(request.hyperlink(), response.hyperlink());
        assertEquals(request.description(), response.description());
        assertEquals(request.tag(), response.tag().id());
        assertEquals(request.important(), response.important());

        assertEquals(1, root.getUsedLinks());
    }

    @Test
    public void updateLink() {
        Authentication userAuth = new UsernamePasswordAuthenticationToken(
            AuthPrincipal.principal("root"), null, List.of(KnownAuthority.ROLE_USER));

        User root = getDummyUser();

        String prevHyper = "Nure.ua";
        String prevDesc = "interesting";
        Boolean prevImp = Boolean.FALSE;

        Tag tag = new Tag();
        tag.setId(1L);
        tag.setTag("Start-page");

        Link link = new Link();
        link.setId(1L);
        link.setHyperlink(prevHyper);
        link.setDescription(prevDesc);
        link.setTag(tag);
        link.setImportant(prevImp);
        link.setUser(root);

        LinkUpdateRequest request = new LinkUpdateRequest("ukr.net", "interesting", 1L, true);

        when(userRepository.findByNickname(notNull())).thenAnswer(
            answer -> {
                String nickname = answer.getArgument(0);
                if (nickname.equalsIgnoreCase("root")) {
                    return Optional.of(root);
                } else {
                    return Optional.empty();
                }
            }
        );
        when(tagRepository.findById(notNull())).thenAnswer(
            answer -> {
                Long id = answer.getArgument(0);
                if (id.equals(1L)) {
                    return Optional.of(tag);
                } else {
                    return Optional.empty();
                }
            }
        );

        when(linkRepository.findById(notNull())).thenAnswer(
            answer -> {
                Long id = answer.getArgument(0);
                if (id.equals(link.getId())) {
                    return Optional.of(link);
                } else {
                    return Optional.empty();
                }
            }
        );
        when(linkRepository.save((notNull()))).thenAnswer(
            answer -> answer.<Link>getArgument(0)
        );

        LinkResponse response = linkCRUD.updateLinkById(link.getId(), request, userAuth);

        assertNotEquals(prevHyper, response.hyperlink());
        assertEquals(prevDesc, response.description());
        assertNotEquals(prevImp, response.important());
    }

    private User getDummyUser() {
        User dummyUser = new User();

        String password = "12345678";
        dummyUser.setId(1L);
        dummyUser.setNickname("root");
        dummyUser.setPassword(password); // in these tests the password is useless
        dummyUser.setStatus(UserStatus.ACTIVE);
        dummyUser.setAccountType(defaultAccountType);
        dummyUser.setUsedLinks(0);
        dummyUser.setLinks(new LinkedList<>());
        dummyUser.setAuthorities(Map.of(KnownAuthority.ROLE_USER, defaultAuthority));

        return dummyUser;
    }
}