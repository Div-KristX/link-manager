package com.nixsolutions.linkmanager.api.service.sharing;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.nixsolutions.linkmanager.api.model.account.AccountType;
import com.nixsolutions.linkmanager.api.model.link.Link;
import com.nixsolutions.linkmanager.api.model.link.response.LinkResponse;
import com.nixsolutions.linkmanager.api.model.sharing.LinkShare;
import com.nixsolutions.linkmanager.api.model.sharing.request.LinkShareRequest;
import com.nixsolutions.linkmanager.api.model.sharing.response.LinkShareResponse;
import com.nixsolutions.linkmanager.api.model.tag.Tag;
import com.nixsolutions.linkmanager.api.model.user.User;
import com.nixsolutions.linkmanager.api.repository.LinkRepository;
import com.nixsolutions.linkmanager.api.repository.LinkShareRepository;
import com.nixsolutions.linkmanager.api.repository.TagRepository;
import com.nixsolutions.linkmanager.api.repository.UserRepository;
import com.nixsolutions.linkmanager.api.service.AuthPrincipal;
import com.nixsolutions.linkmanager.api.service.link.LinkService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

@ExtendWith(MockitoExtension.class)
public class LinkShareServiceTest {

    private final Map<Long, AccountType> accounts = new HashMap<>();
    private final Map<String, User> users = new HashMap<>();
    private final Map<Long, Link> links = new HashMap<>();
    private final Map<Long, Tag> tags = new HashMap<>();
    private final Map<Long, LinkShare> shares = new HashMap<>();
    private long usersId = 0;
    private long accountTypeId = 0;
    private long linksId = 0;
    private long linkTypeId = 0;
    private long sharingId = 0;
    private LinkRepository linkRepository;
    private TagRepository tagRepository;
    private LinkShareCRUD shareCRUD;
    private UserRepository userRepository;
    private LinkShareRepository shareRepository;

    private LinkService linkCRUD;

    @BeforeEach
    public void setUp() {

        accounts.put(++accountTypeId, createAccountType(accountTypeId, "Default Account", 10));

        tags.put(++linkTypeId, createLinkType(linkTypeId, "Useful"));

        linkRepository = mock(LinkRepository.class);

        userRepository = mock(UserRepository.class);

        shareRepository = mock(LinkShareRepository.class);

        tagRepository = mock(TagRepository.class);

        linkCRUD = new LinkService(linkRepository, userRepository, tagRepository);

        shareCRUD = new LinkShareService(linkRepository, linkCRUD, userRepository, shareRepository);
    }


    @Test
    public void creatingInvite() {
        User linkHolder = createUser("root");

        users.put(linkHolder.getNickname(), linkHolder);

        User destinationUser = createUser("dummy");

        users.put(destinationUser.getNickname(), destinationUser);

        links.put(++linksId,
            createLink(tags.get(1L), linkHolder, "Nure.ua", "start-page", true));

        linkHolder.addLink(links.get(linksId));

        LinkShareRequest request = new LinkShareRequest(
            linkHolder.getNickname(),
            destinationUser.getNickname(),
            1L);

        Authentication originUserAuthentication = new UsernamePasswordAuthenticationToken(
            AuthPrincipal.principal(linkHolder.getNickname()), null);

        when(linkRepository.findById(anyLong())).thenAnswer(
            answer -> {
                long id = answer.getArgument(0);
                return Optional.of(links.get(id));
            }
        );

        when(userRepository.findByNickname(notNull())).thenAnswer(
            answer -> {
                String nickname = answer.getArgument(0);
                return Optional.of(users.get(nickname));
            }
        );

        when(shareRepository.save(notNull())).thenAnswer(
            answer -> {
                LinkShare share = answer.getArgument(0);

                assertThat(linkHolder.getNickname()).isEqualTo(share.getOriginUser().getNickname());

                assertThat(destinationUser.getNickname()).isEqualTo(
                    share.getDestinationUser().getNickname());

                assertThat(links.get(request.linkId())).isEqualTo(share.getSharedLink());

                share.setId(++sharingId);

                shares.put(sharingId, share);
                return share;
            }
        );
        LinkShareResponse response = shareCRUD.createInvite(originUserAuthentication, request);

        // creation is successful
        assertThat(response.originUser()).isEqualTo(linkHolder.getNickname());

        assertThat(response.destinationUser()).isEqualTo(destinationUser.getNickname());

        assertThat(response.linkToShare()).isEqualTo(
            LinkResponse.fromLink(links.get(request.linkId())));
    }

    // accepting offer
    @Test
    public void acceptOfferedLink() {
        User linkHolder = createUser("root");

        users.put(linkHolder.getNickname(), linkHolder);

        User destinationUser = createUser("dummy");

        users.put(destinationUser.getNickname(), destinationUser);

        links.put(++linksId,
            createLink(tags.get(1L), linkHolder, "Nure.ua", "start-page", true));

        linkHolder.addLink(links.get(linksId));

        LinkShare share = new LinkShare();

        share.setId(++sharingId);

        share.setSharedLink(links.get(linksId));

        share.setOriginUser(linkHolder);

        share.setDestinationUser(destinationUser);

        shares.put(sharingId, share);

        Authentication destinationUserAuthentication = new UsernamePasswordAuthenticationToken(
            AuthPrincipal.principal(
                destinationUser.getNickname()), null);

        when(userRepository.findByNickname(notNull())).thenAnswer(
            answer -> {
                String nickname = answer.getArgument(0);
                return Optional.of(users.get(nickname));
            }
        );

        when(linkRepository.save(notNull())).thenAnswer(
            answer -> {
                Link link = answer.getArgument(0);
                link.setId(++linksId);
                links.put(linksId, link);
                return link;
            }
        );

        when(tagRepository.findById(notNull())).thenAnswer(
            answer -> {
                Long id = answer.getArgument(0);
                return Optional.of(tags.get(id));
            }
        );

        when(shareRepository.findById(anyLong())).thenAnswer(
            answer -> {
                Long id = answer.getArgument(0);
                return Optional.of(shares.get(id));
            }
        );

        when(shareRepository.findAllByDestinationUser(notNull(), notNull())).thenAnswer(
            answer -> {
                User user = answer.getArgument(0);
                return new PageImpl<>(shares.values()
                    .stream()
                    .filter(r -> r.getDestinationUser().equals(user))
                    .toList());
            }
        );

        doAnswer(answer -> {
            Long id = answer.getArgument(0);
            shares.remove(id);
            return null;
        }).when(shareRepository).deleteById(notNull());

        Page<LinkShareResponse> offersForDestinationUser = shareCRUD.findSharesForOriginUser(
            destinationUserAuthentication, Pageable.unpaged());

        Long offerId = offersForDestinationUser
            .getContent()
            .stream()
            .findAny()
            .get().id();

        LinkResponse addedLink = shareCRUD.acceptOfferedLink(offerId,
            destinationUserAuthentication);

        assertThat(addedLink.id()).isEqualTo(linksId);

        assertThat(shares.isEmpty()).isTrue();

        verify(linkRepository, only()).save(notNull());

        verify(shareRepository, times(1)).deleteById(notNull());
    }

    private User createUser(String nickName) {
        User user = new User();

        user.setId(++usersId);

        user.setNickname(nickName);

        user.setAccountType(accounts.get(1L));

        user.setLinks(new ArrayList<>());
        return user;
    }

    private AccountType createAccountType(long id, String name, int maxLinks) {
        AccountType account = new AccountType();

        account.setId(id);

        account.setName(name);

        account.setMaxLinks(maxLinks);

        return account;
    }

    private Tag createLinkType(long id, String name) {
        Tag type = new Tag();

        type.setId(id);

        type.setTag(name);

        return type;
    }

    private Link createLink(Tag type, User userManager, String hyperlink,
        String description, boolean isImportant) {
        Link link = new Link();

        link.setId(linksId);

        link.setTag(type);

        link.setUser(userManager);

        link.setDescription(description);

        link.setHyperlink(hyperlink);

        link.setImportant(isImportant);

        return link;
    }
}