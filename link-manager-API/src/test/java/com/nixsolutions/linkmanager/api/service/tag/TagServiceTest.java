package com.nixsolutions.linkmanager.api.service.tag;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.nixsolutions.linkmanager.api.exception.LinkManagerExceptions;
import com.nixsolutions.linkmanager.api.exception.auth.AuthenticationExceptions;
import com.nixsolutions.linkmanager.api.model.tag.Tag;
import com.nixsolutions.linkmanager.api.model.tag.request.TagRequest;
import com.nixsolutions.linkmanager.api.model.tag.response.TagResponse;
import com.nixsolutions.linkmanager.api.model.user.KnownAuthority;
import com.nixsolutions.linkmanager.api.repository.TagRepository;
import com.nixsolutions.linkmanager.api.service.AuthPrincipal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.server.ResponseStatusException;

class TagServiceTest {


    private TagRepository tagRepository;

    private TagCRUD tagCRUD;

    @BeforeEach
    void setUp() {
        tagRepository = mock(TagRepository.class);
        tagCRUD = new TagService(tagRepository);
    }

    @Test
    void createTags() {

        Tag linkTag = new Tag();
        linkTag.setId(1L);
        linkTag.setTag("No Type");

        // Create tag

        TagRequest request = new TagRequest("Image");

        Authentication userAuth = new UsernamePasswordAuthenticationToken(
            AuthPrincipal.principal("root"), null,
            List.of(KnownAuthority.ROLE_USER, KnownAuthority.ROLE_ADMIN));

        when(tagRepository.existsByTagIgnoreCase(notNull())).thenAnswer(
            answer -> {
                String tag = answer.getArgument(0);
                return tag.equalsIgnoreCase(linkTag.getTag());
            }
        );

        when(tagRepository.save(notNull())).thenAnswer(
            answer -> {
                Tag requestedTag = answer.getArgument(0);
                assertNull(requestedTag.getId());
                assertEquals(requestedTag.getTag(), request.tag());
                requestedTag.setId(2L);
                return requestedTag;
            }
        );

        TagResponse response = tagCRUD.create(request, userAuth);

        assertEquals(request.tag(), response.tag());

        assertNotNull(response.id());

        TagRequest duplicateRequest = new TagRequest("no type");

        assertThrows(ResponseStatusException.class,
            () -> tagCRUD.create(duplicateRequest, userAuth),
            LinkManagerExceptions.duplicateTag(duplicateRequest.tag()).getMessage());
    }

    @Test
    public void updateTag() {
        String previousName = "No Type";

        Tag linkTag = new Tag();
        linkTag.setId(1L);
        linkTag.setTag(previousName);

        TagRequest request = new TagRequest("Image");

        Authentication userAuth = new UsernamePasswordAuthenticationToken(
            AuthPrincipal.principal("root"), null,
            List.of(KnownAuthority.ROLE_USER, KnownAuthority.ROLE_ADMIN));

        when(tagRepository.existsByTagIgnoreCase(notNull())).thenAnswer(
            answer -> {
                String tag = answer.getArgument(0);
                return tag.equalsIgnoreCase(linkTag.getTag());
            }
        );

        when(tagRepository.findById(notNull())).thenAnswer(
            answer -> {
                Long id = answer.getArgument(0);
                if (id.equals(linkTag.getId())) {
                    return Optional.of(linkTag);
                } else {
                    return Optional.empty();
                }
            }
        );

        tagCRUD.update(linkTag.getId(), request, userAuth);

        assertNotEquals(previousName, linkTag.getTag());

        Map<Long, Tag> tags = new HashMap<>();
        tags.put(linkTag.getId(), linkTag);

        doAnswer(answer -> {
            Long id = answer.getArgument(0);
            if (id.equals(linkTag.getId())) {
                tags.remove(id);
            }
            return null;
        }).when(tagRepository).deleteById(notNull());

        String notAdminNickName = "dummy";
        Authentication notAdminAuth = new UsernamePasswordAuthenticationToken(
            AuthPrincipal.principal(notAdminNickName), null,
            List.of(KnownAuthority.ROLE_USER));

        assertThrows(ResponseStatusException.class, () ->
                tagCRUD.deleteById(linkTag.getId(), notAdminAuth),
            AuthenticationExceptions.noAccessRightsToTag(notAdminNickName, linkTag.getId())
                .getMessage());

        assertNotNull(tags.get(linkTag.getId()));

        Long entityId = linkTag.getId();

        tagCRUD.deleteById(linkTag.getId(), userAuth);

        assertNull(tags.get(entityId));
    }
}
