package com.nixsolutions.linkmanager.ui.service;

import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.notFound;
import static com.github.tomakehurst.wiremock.client.WireMock.ok;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.nixsolutions.linkmanager.ui.data.tag.Tag;
import com.nixsolutions.linkmanager.ui.data.tag.TagCreateRequest;
import com.nixsolutions.linkmanager.ui.service.pagination.RestPageImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.web.client.HttpClientErrorException;

@SpringBootTest
class TagsServiceTest {

    private static final int restPort = 8091;

    private static final int uiPort = 8118;
    private static WireMockServer wireMockServer;
    @Autowired
    private TagService tagService;

    @Autowired
    private ObjectMapper objectMapper;

    private Page<Tag> defaultTags;

    @BeforeAll
    public static void setUp() {
        wireMockServer = new WireMockServer(options().port(restPort));
        wireMockServer.start();

    }

    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry registry) {
        registry.add("linkmanager.api.base-url", () -> "http://localhost:" + restPort);
        registry.add("server.port", () -> uiPort);
    }

    @AfterAll
    public static void clean() {
        wireMockServer.shutdown();
    }

    @BeforeEach
    public void setUpDefaultData() {
        defaultTags = defaultTagsPage();
    }

    @AfterEach
    public void tearDown() {
        wireMockServer.resetAll();
    }


    @Test
    public void tagsRequestWithPagesIsCorrect() throws JsonProcessingException {
        int pageOrder = 1;
        int pageSize = 4;
        wireMockServer.stubFor(get("/api/v1/tags" + "?page=" + pageOrder + "&size=" + pageSize)
            .willReturn(ok()
                .withHeader("Content-type", "application/json")
                .withBody(objectMapper.writeValueAsString(defaultTags))));

        Page<Tag> requestedPageTags = tagService.listAll(pageOrder, pageSize, Optional.empty(),
            Optional.empty());

        assertEquals(requestedPageTags.getTotalElements(), defaultTags.getTotalElements());

        assertEquals(requestedPageTags.getTotalPages(), defaultTags.getTotalPages());

        assertEquals(requestedPageTags.getContent().get(0), defaultTags.getContent().get(0));
    }

    @Test
    public void NotFoundTag_With_ID_5() {

        wireMockServer.stubFor(get("/api/v1/tags/5")
            .willReturn(notFound()));

        HttpClientErrorException expectedException = assertThrows(HttpClientErrorException.class,
            () -> tagService.getById(5L));

        assertEquals(expectedException.getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    public void addValidTag() throws JsonProcessingException {
        Tag createdTagReq = new Tag(5L, "News");

        TagCreateRequest request = new TagCreateRequest();
        request.setTag(createdTagReq.tag());

        wireMockServer.stubFor(post("/api/v1/tags")
            .willReturn(ok()
                .withHeader("Content-type", "application/json")
                .withBody(objectMapper.writeValueAsString(createdTagReq))));

        Tag response = tagService.createTag("", request);

        assertEquals(request.getTag(), response.tag());
        assertEquals(createdTagReq.id(), response.id());
    }


    private Page<Tag> defaultTagsPage() {
        List<Tag> defaultTagsList = new ArrayList<>();
        defaultTagsList.add(new Tag(1L, "Useful"));
        defaultTagsList.add(new Tag(2L, "Image"));
        defaultTagsList.add(new Tag(3L, "Start-page"));
        defaultTagsList.add(new Tag(4L, "No type"));

        return new RestPageImpl<>(defaultTagsList, Pageable.ofSize(defaultTagsList.size()),
            defaultTagsList.size());
    }


}
