package com.nixsolutions.linkmanager.ui.controller;

import static com.github.tomakehurst.wiremock.client.WireMock.ok;
import static com.github.tomakehurst.wiremock.client.WireMock.unauthorized;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.cookie;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.nixsolutions.linkmanager.ui.data.account.AccountType;
import com.nixsolutions.linkmanager.ui.data.auth.LoginDTO;
import com.nixsolutions.linkmanager.ui.data.auth.LogoutDTO;
import com.nixsolutions.linkmanager.ui.data.auth.TokenResponse;
import com.nixsolutions.linkmanager.ui.data.user.Authority;
import com.nixsolutions.linkmanager.ui.data.user.User;
import com.nixsolutions.linkmanager.ui.data.user.UserStatus;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieManagement;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieProperties;
import java.util.Set;
import javax.servlet.http.Cookie;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class AuthControllerTest {

    private static final String baseAPIUrl = "/api/v1";

    private static final int restPort = 8092;

    private static final int uiPort = 8182;

    private static WireMockServer wireMockServer;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CookieManagement cookieManagement;

    @Value("${access.token.max.age}")
    private Integer accessTokenAge;

    @Value("${refresh.token.max.age}")
    private Integer refreshTokenAge;

    @BeforeAll
    public static void init() {
        wireMockServer = new WireMockServer(options().port(restPort));
        wireMockServer.start();
    }

    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry registry) {
        registry.add("linkmanager.api.base-url", () -> "http://localhost:" + restPort);
        registry.add("server.port", () -> uiPort);
    }


    @AfterAll
    public static void clean() {
        wireMockServer.shutdown();
    }

    @AfterEach
    public void tearDown() {
        wireMockServer.resetAll();
    }

    @Test
    public void validLogin() throws Exception {

        LoginDTO loginData = new LoginDTO();
        loginData.setLogin("root");
        loginData.setPassword("12345678");

        String tokensResponse = objectMapper.writeValueAsString(createTokens());

        wireMockServer.stubFor(WireMock.post(baseAPIUrl + "/token")
            .willReturn(ok()
                .withHeader("Content-type", "application/json")
                .withBody(tokensResponse)));

        mockMvc.perform(post("/login")
                .flashAttr("loginForm", loginData)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginData))
            )
            .andExpect(status().is3xxRedirection())
            .andExpect(redirectedUrl("/links"))
            .andExpect(cookie().exists(CookieProperties.ACCESS_TOKEN))
            .andExpect(cookie().exists(CookieProperties.REFRESH_TOKEN));
    }

    @Test
    public void invalidLogin() throws Exception {

        LoginDTO loginData = new LoginDTO();

        loginData.setLogin("Doomfist");

        loginData.setPassword("");

        wireMockServer.stubFor(WireMock.post(baseAPIUrl + "/token")
            .willReturn(unauthorized()));

        performInvalidLogin(loginData);

        loginData.setLogin("");

        loginData.setPassword("qwec213cx");

        performInvalidLogin(loginData);

        // password or login is not correct
        loginData.setLogin("Doomfist");

        loginData.setPassword("qwec213cx");

        mockMvc.perform(post("/login")
                .flashAttr("loginForm", loginData)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginData)))
            .andExpect(status().isOk())
            .andExpect(model().attributeExists("loginError"));
    }


    @Test
    public void logoutClearsCookie() throws Exception {

        User dummyUser = createUser();

        TokenResponse tokens = createTokens();

        LogoutDTO logoutDTO = new LogoutDTO(tokens.refreshToken());

        wireMockServer.stubFor(WireMock.post(baseAPIUrl + "/token/invalidate")
            .willReturn(ok()));

        wireMockServer.stubFor(WireMock.get(baseAPIUrl + "/users/me")
            .willReturn(ok()
                .withHeader("Content-type", "application/json")
                .withBody(objectMapper.writeValueAsString(dummyUser))));

        Cookie accessToken = cookieManagement.createCookie(CookieProperties.ACCESS_TOKEN,
            tokens.accessToken(),
            accessTokenAge);

        Cookie refreshToken = cookieManagement.createCookie(CookieProperties.REFRESH_TOKEN,
            tokens.refreshToken(),
            refreshTokenAge);

        mockMvc.perform(get("/")
                .cookie(accessToken, refreshToken))
            .andExpect(model().attribute("nickname", dummyUser.nickname()));

        mockMvc.perform(post("/logout")
                .cookie(accessToken, refreshToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(logoutDTO)))
            .andExpect(status().isOk())
            .andExpect(cookie().doesNotExist(CookieProperties.REFRESH_TOKEN))
            .andExpect(cookie().doesNotExist(CookieProperties.ACCESS_TOKEN));
    }

    private User createUser() {
        return new User(
            1L, "root", new AccountType(1L, "Account", 10),
            UserStatus.ACTIVE, Set.of(Authority.ROLE_USER), 1);
    }

    private TokenResponse createTokens(){
        return new TokenResponse("dajs923jcak.321mcadko.123dj9",
            "dajs923jcak.321mcadko.123dj9", 600);
    }

    private void performInvalidLogin(LoginDTO loginData) throws Exception {
        mockMvc.perform(post("/login")
                .flashAttr("loginForm", loginData)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginData)))
            .andExpect(status().isOk())
            .andExpect(model().hasErrors());
    }
}
