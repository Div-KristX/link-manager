package com.nixsolutions.linkmanager.ui.controller;

import com.nixsolutions.linkmanager.ui.data.account.ChangeAccountRequest;
import com.nixsolutions.linkmanager.ui.data.auth.TokenResponse;
import com.nixsolutions.linkmanager.ui.data.user.ChangePasswordRequest;
import com.nixsolutions.linkmanager.ui.data.user.ChangeUserStatusRequest;
import com.nixsolutions.linkmanager.ui.data.user.OverridePassword;
import com.nixsolutions.linkmanager.ui.data.user.RegisterNewUserRequest;
import com.nixsolutions.linkmanager.ui.data.user.User;
import com.nixsolutions.linkmanager.ui.service.UserService;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieManagement;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieProperties;
import com.nixsolutions.linkmanager.ui.service.loader.UserInfoLoader;
import com.nixsolutions.linkmanager.ui.service.pagination.ModelPagination;
import com.nixsolutions.linkmanager.ui.service.token.TokenRefresher;
import com.nixsolutions.linkmanager.ui.service.token.UserAccessChecker;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

@Controller
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;

    private final UserInfoLoader userInfoLoader;

    private final ModelPagination modelPagination;

    private final TokenRefresher refresher;

    private final CookieManagement cookieManager;

    private final UserAccessChecker checker;


    public UserController(UserService userService, UserInfoLoader userInfoLoader,
        ModelPagination modelPagination, TokenRefresher refresher,
        CookieManagement cookieManager, UserAccessChecker checker) {
        this.userService = userService;
        this.userInfoLoader = userInfoLoader;
        this.modelPagination = modelPagination;
        this.refresher = refresher;
        this.cookieManager = cookieManager;
        this.checker = checker;
    }

    @GetMapping("/register")
    public String register(
        @ModelAttribute("registerForm") RegisterNewUserRequest loginForm,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response
    ) {
        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        if (checker.isAnonymous(tokens)) {
            return "auth/register";
        } else {
            return "redirect:/links";
        }
    }

    @PostMapping("/register")
    public String registerNewUser(
        @Valid @ModelAttribute("registerForm") RegisterNewUserRequest registerForm,
        BindingResult result,
        Model model
    ) {

        if (result.hasErrors()) {
            return "auth/register";
        }
        try {
            userService.registerNewUser(registerForm);
        } catch (HttpClientErrorException e) {
            model.addAttribute("registerTrouble", true);
            log.error(e.getMessage());
            return "auth/register";
        }
        return "home";
    }

    @GetMapping("/admin")
    public String registerAdmin(
        @ModelAttribute("registerForm") RegisterNewUserRequest registerForm,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        return "auth/admin";
    }

    @PostMapping("/admin")
    public String registerAdmin(
        @Valid
        @ModelAttribute("registerForm") RegisterNewUserRequest registerForm,
        BindingResult result,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        Model model) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        if (result.hasErrors()) {
            return "auth/admin";
        }

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        try {
            userService.registerNewAdmin(tokens.get().accessToken(), registerForm);
        } catch (HttpClientErrorException e) {
            model.addAttribute("registerTrouble", true);
            log.error(e.getMessage());
            return "auth/admin";
        }
        return "redirect:/";
    }

    @GetMapping("me")
    public String getCurrentUser(
        @ModelAttribute("changePassword") ChangePasswordRequest changePasswordRequest,
        @ModelAttribute("overridePassword") OverridePassword overridePassword,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        Model model) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User currentUser = userInfoLoader.loadUser(tokens.get().accessToken());

        userInfoLoader.setModelAttributesForUser(currentUser, model);

        model.addAttribute("requestedUser", currentUser);

        return "users/me";
    }

    @DeleteMapping("me")
    public String deleteCurrentUser(
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        userService.deleteCurrentUser(tokens.get().accessToken());

        refresher.cleanAccessAndRefreshTokenCookie(response);

        return "redirect:/";
    }


    @PatchMapping("me/password")
    public String changePasswordForMe(
        @Valid
        @ModelAttribute("changePassword") ChangePasswordRequest changePassword,
        BindingResult result,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        Model model
    ) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        userInfoLoader.setModelAttributesForUser(user, model);

        model.addAttribute("requestedUser", user);

        if (result.hasErrors()) {
            return "users/me";
        }
        try {
            userService.changePasswordByCurrentUser(tokens.get().accessToken(), changePassword);
        } catch (ResponseStatusException e) {
            log.error(e.getMessage());
            model.addAttribute("changePasswordError", "Old password does not match");
            return "users/me";
        }
        model.addAttribute("passwordChanged", true);
        return "users/me";
    }

    @GetMapping("/users/search")
    public String forwardToUser(@RequestParam(name = "typeId") String id) {
        return "redirect:/users/" + id;
    }

    @GetMapping("/users")
    public String listLinkTypes(
        Model model,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @CookieValue(name = CookieProperties.SHARING_INVITE_LINK, required = false) Cookie inviteCreating,
        @RequestParam(defaultValue = "1") int page,
        @RequestParam(defaultValue = "6") int size,
        @RequestParam(required = false) String keyword,
        @RequestParam(name = "sort", required = false) Optional<String> sort,
        HttpServletResponse response
    ) {
        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        userInfoLoader.setModelAttributesForUser(user, model);

        model.addAttribute("inviteCreating", inviteCreating);

        try {
            if (keyword != null) {
                model.addAttribute("keyword", keyword);
            }
            Page<User> responsePage = userService.listAll(tokens.get().accessToken(), page - 1,
                size, Optional.ofNullable(keyword), sort);
            modelPagination.setPagesToModel(responsePage, model);
            model.addAttribute("pageSize", size);
        } catch (Exception e) {
            log.error(e.getMessage());
            model.addAttribute("message", e.getMessage());
        }
        return "users/users";
    }

    @GetMapping("users/{id}")
    public String getUserById(
        @ModelAttribute("overridePassword") OverridePassword overridePassword,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @CookieValue(name = CookieProperties.CHANGE_USER_ACCOUNT, required = false) Cookie accountChangeCookie,
        @CookieValue(name = CookieProperties.CHANGE_USER_ACCOUNT_NAME, required = false) Cookie accountName,
        @CookieValue(name = CookieProperties.SHARING_INVITE_LINK, required = false) Cookie inviteCreating,
        HttpServletResponse response,
        @PathVariable
        Long id,
        Model model) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        if (user.id().equals(id)) {
            return "redirect:/me";
        }
        User requestedUser;
        try {
            requestedUser = userService.getUserById(tokens.get().accessToken(), id);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        if (accountChangeCookie != null) {
            model.addAttribute("userAccountInChangeStage", true);
            if (accountName != null) {
                model.addAttribute("chosenAccount", accountName.getValue());
            }
        }
        userInfoLoader.setModelAttributesForUser(user, model);
        model.addAttribute("inviteCreating", inviteCreating);
        model.addAttribute("requestedUser", requestedUser);
        return "/users/user";
    }

    @PatchMapping("users/{id}/password")
    public String overrideUserPasswordById(
        @Valid
        @ModelAttribute("overridePassword") OverridePassword overridePassword,
        BindingResult result,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        @PathVariable
        Long id,
        Model model) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        userInfoLoader.setModelAttributesForUser(user, model);

        User requestedUser = userService.getUserById(tokens.get().accessToken(), id);

        model.addAttribute("requestedUser", requestedUser);

        if (result.hasErrors()) {
            return "users/user";
        }
        try {
            userService.changePasswordById(tokens.get().accessToken(), requestedUser.id(),
                overridePassword);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        model.addAttribute("successfulPWDOverride", true);
        return "users/user";
    }

    @DeleteMapping("users/{id}")
    public String deleteUserById(
        HttpServletResponse response,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @PathVariable
        Long id) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);
        try {
            userService.deleteUserById(tokens.get().accessToken(), id);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        return "redirect:/users";
    }


    @PatchMapping("users/{id}/status")
    public String changeUserStatusById(
        @RequestParam(name = "userStatus") String userStatus,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        @PathVariable
        Long id
    ) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        userService.changeStatusById(tokens.get().accessToken(), id,
            new ChangeUserStatusRequest(userStatus));

        return "redirect:/users/" + id;
    }

    @PostMapping("users/{id}/account/change/decline")
    public String declineChangeUserAccount(
        HttpServletResponse response,
        @PathVariable Long id) {

        cookieManager.eraseCookies(response,
            List.of(CookieProperties.CHANGE_USER_ACCOUNT_NAME,
                CookieProperties.CHANGE_USER_ACCOUNT));

        return "redirect:/users/" + id;
    }

    @PostMapping("/accounts/change/{name}")
    public String changeAccountForUser(
        @CookieValue(name = CookieProperties.CHANGE_USER_ACCOUNT, required = false) Cookie changeAccountCookieReq,
        HttpServletResponse response,
        @PathVariable String name) {

        if (changeAccountCookieReq == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        Cookie accountChangeCookieConfirm = cookieManager.createWithEncodedValue(
            CookieProperties.CHANGE_USER_ACCOUNT_NAME, name, 600);
        response.addCookie(accountChangeCookieConfirm);

        return "redirect:/users/" + changeAccountCookieReq.getValue();
    }

    @PatchMapping("users/{id}/account")
    public String changeUserAccountById(
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @CookieValue(name = CookieProperties.CHANGE_USER_ACCOUNT, required = false) Cookie accountChangeCookie,
        @CookieValue(name = CookieProperties.CHANGE_USER_ACCOUNT_NAME, required = false) Cookie accountName,
        HttpServletResponse response,
        @PathVariable
        Long id
    ) {
        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);
        checker.hasAccessToRead(tokens);
        User user = userInfoLoader.loadUser(tokens.get().accessToken());
        checker.checkAdminAccess(user);

        if (accountChangeCookie == null) {
            Cookie cookie = cookieManager.createWithEncodedValue(
                CookieProperties.CHANGE_USER_ACCOUNT, String.valueOf(id), 600);
            response.addCookie(cookie);
            return "redirect:/accounts";
        }
        if (accountName == null) {
            cookieManager.eraseCookies(response, List.of(CookieProperties.CHANGE_USER_ACCOUNT_NAME,
                CookieProperties.CHANGE_USER_ACCOUNT));
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        try {
            String selectedAccount = cookieManager.getDecodedValue(accountName);
            long selectedUserId = getSelectedUserId(accountChangeCookie);
            userService.changeAccountTypeById(tokens.get().accessToken(), selectedUserId,
                new ChangeAccountRequest(selectedAccount));
        } catch (HttpClientErrorException e) {
            cookieManager.eraseCookies(response, List.of(CookieProperties.CHANGE_USER_ACCOUNT_NAME,
                CookieProperties.CHANGE_USER_ACCOUNT));
            throw new ResponseStatusException(e.getStatusCode());
        }
        cookieManager.eraseCookies(response,
            List.of(CookieProperties.CHANGE_USER_ACCOUNT_NAME,
                CookieProperties.CHANGE_USER_ACCOUNT));
        return "redirect:/users/" + id;
    }

    private long getSelectedUserId(Cookie selectedAccount) {
        long selectedUserId; // prevent unexpected usage {id} variable
        try {
            selectedUserId = Long.parseLong(selectedAccount.getValue());
        } catch (NumberFormatException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        return selectedUserId;
    }
}
