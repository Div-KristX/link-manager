package com.nixsolutions.linkmanager.ui.service;

import com.nixsolutions.linkmanager.ui.data.link.LinkCreateRequest;
import com.nixsolutions.linkmanager.ui.data.link.LinkResponse;
import com.nixsolutions.linkmanager.ui.service.headers.HttpHeaderWrap;
import com.nixsolutions.linkmanager.ui.service.pagination.RestPageImpl;
import com.nixsolutions.linkmanager.ui.service.uri.UriComponentsStructure;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@Service
public class LinkService {

    private final RestTemplate rest;

    private final UriComponentsStructure structure;


    @Value("${linkmanager.api.endpoint.link}")
    private String LINK_API_URL;

    @Value("${linkmanager.api.endpoint.user}")
    private String USER_API_URL;


    public LinkService(RestTemplate rest, UriComponentsStructure structure) {
        this.rest = rest;
        this.structure = structure;
    }

    public Page<LinkResponse> listAll(String accessToken, int page, int size,
        Optional<String> keyword, Optional<Long> linkType, Optional<String> sort) {

        Map<String, String> params = new LinkedHashMap<>();

        params.put("page", String.valueOf(page));

        params.put("size", String.valueOf(size));

        keyword.ifPresent(k -> params.put("keywords", k));

        linkType.ifPresent(id -> params.put("typeId", String.valueOf(id)));

        sort.ifPresent(s -> {
            if (s.equals("important")) {
                params.put("sort", "isImportant");
            } else {
                params.put("sort", s);
            }
        });

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<?> requestEntity = new HttpEntity<>(header.getHeader());

        ParameterizedTypeReference<RestPageImpl<LinkResponse>> responseType = new ParameterizedTypeReference<>() {
        };

        ResponseEntity<RestPageImpl<LinkResponse>> response = rest.exchange(
            structure.createURI(LINK_API_URL, params),
            HttpMethod.GET,
            requestEntity,
            responseType
        );

        if (response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        }
        throw new ResponseStatusException(response.getStatusCode());
    }

    public Page<LinkResponse> listLinksForUser(String accessToken, long userId, int page, int size) {
        Map<String, String> params = new LinkedHashMap<>();

        params.put("page", String.valueOf(page));

        params.put("size", String.valueOf(size));

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<?> requestEntity = new HttpEntity<>(header.getHeader());

        ParameterizedTypeReference<RestPageImpl<LinkResponse>> responseType = new ParameterizedTypeReference<>() {
        };

        ResponseEntity<RestPageImpl<LinkResponse>> response = rest.exchange(
            structure.createURI(USER_API_URL + "/" + userId + "/links", params),
            HttpMethod.GET,
            requestEntity,
            responseType
        );

        if (response.getStatusCode().is2xxSuccessful()) {
            return response.getBody();
        }
        throw new ResponseStatusException(response.getStatusCode());
    }


    public LinkResponse createLink(String accessToken, LinkCreateRequest request) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<LinkCreateRequest> requestEntity = new HttpEntity<>(request,
            header.getHeader());

        ResponseEntity<LinkResponse> createdLinkResponse = rest.exchange(
            LINK_API_URL,
            HttpMethod.POST,
            requestEntity,
            LinkResponse.class);
        return createdLinkResponse.getBody();
    }

    public LinkResponse updateLink(String accessToken, Long id, LinkCreateRequest request) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();
        HttpEntity<LinkCreateRequest> requestEntity = new HttpEntity<>(request,
            header.getHeader());

        ResponseEntity<LinkResponse> updatedLinkResponse = rest.exchange(
            LINK_API_URL + "/" + id,
            HttpMethod.PUT,
            requestEntity,
            LinkResponse.class);

        if (updatedLinkResponse.getStatusCode().is2xxSuccessful()) {
            return updatedLinkResponse.getBody();
        }

        throw new ResponseStatusException(updatedLinkResponse.getStatusCode());
    }


    public LinkResponse getLinkById(String accessToken, Long id) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<?> request = new HttpEntity<>(header.getHeader());

        ResponseEntity<LinkResponse> requestedLink = rest.exchange(
            LINK_API_URL + "/" + id,
            HttpMethod.GET,
            request,
            LinkResponse.class
        );

        if (requestedLink.getStatusCode().is2xxSuccessful()) {
            return requestedLink.getBody();
        }

        throw new ResponseStatusException(requestedLink.getStatusCode());
    }

    public void deleteLinkById(String accessToken, Long id) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .build();
        HttpEntity<?> request = new HttpEntity<>(header.getHeader());

        rest.exchange(LINK_API_URL + "/" + id, HttpMethod.DELETE, request, LinkResponse.class);
    }

}
