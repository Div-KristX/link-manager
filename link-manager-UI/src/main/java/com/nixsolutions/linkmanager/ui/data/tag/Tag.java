package com.nixsolutions.linkmanager.ui.data.tag;

public record Tag(
    Long id,
    String tag
) {

}
