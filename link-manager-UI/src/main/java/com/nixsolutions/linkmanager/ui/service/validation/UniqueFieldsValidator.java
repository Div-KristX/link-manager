package com.nixsolutions.linkmanager.ui.service.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.springframework.beans.BeanWrapperImpl;

public class UniqueFieldsValidator implements
    ConstraintValidator<FieldsAreUnique, Object> {

    private String field;
    private String fieldMatch;

    @Override
    public void initialize(FieldsAreUnique constraintAnnotation) {
        this.field = constraintAnnotation.field();
        this.fieldMatch = constraintAnnotation.fieldMatch();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        Object fieldValue = new BeanWrapperImpl(value)
            .getPropertyValue(field);
        Object fieldMatchValue = new BeanWrapperImpl(value)
            .getPropertyValue(fieldMatch);

        if (fieldValue != null && fieldMatchValue != null) {
            return !fieldValue.equals(fieldMatchValue);
        } else {
            return false;
        }
    }

}
