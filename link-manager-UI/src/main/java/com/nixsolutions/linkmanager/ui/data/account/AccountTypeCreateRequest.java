package com.nixsolutions.linkmanager.ui.data.account;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;

public class AccountTypeCreateRequest {

    @NotBlank
    @Length(min = 4, message = "Too short")
    private String name;

    @NotNull
    private Integer maxLinks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxLinks() {
        return maxLinks;
    }

    public void setMaxLinks(Integer maxLinks) {
        this.maxLinks = maxLinks;
    }
}
