package com.nixsolutions.linkmanager.ui.data.user;

public record ChangeUserStatusRequest(
    String status
) {

}
