package com.nixsolutions.linkmanager.ui.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Controller("/error")
public class ErrorController {

    private static final Logger logger = LoggerFactory.getLogger(ErrorController.class);

    @ExceptionHandler(Throwable.class)
    public String exception(Throwable throwable) {
        logger.error("Exception during execution of SpringSecurity application", throwable);
        return "error";
    }
}
