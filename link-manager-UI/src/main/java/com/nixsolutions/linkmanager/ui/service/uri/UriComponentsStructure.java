package com.nixsolutions.linkmanager.ui.service.uri;


import java.util.Map;

public interface UriComponentsStructure {


    String createURI(String httpUrl, Map<String, String> params);
}
