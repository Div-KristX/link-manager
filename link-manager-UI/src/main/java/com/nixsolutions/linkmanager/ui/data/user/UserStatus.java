package com.nixsolutions.linkmanager.ui.data.user;

public enum UserStatus {
    ACTIVE, SUSPENDED
}
