package com.nixsolutions.linkmanager.ui.service.token;

import com.nixsolutions.linkmanager.ui.data.auth.RefreshTokenRequest;
import com.nixsolutions.linkmanager.ui.data.auth.TokenResponse;
import com.nixsolutions.linkmanager.ui.service.AuthService;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieManagement;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieProperties;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TokenRefresher {


    private final AuthService authService;

    private final CookieManagement cookieManager;

    @Value("${access.token.max.age}")
    private Integer accessTokenAge;

    @Value("${refresh.token.max.age}")
    private Integer refreshTokenAge;


    public TokenRefresher(AuthService authService, CookieManagement cookieManager) {
        this.authService = authService;
        this.cookieManager = cookieManager;
    }


    public Optional<TokenResponse> getTokens(Cookie accessCookie, Cookie refreshCookie,
        HttpServletResponse response) {

        if (refreshCookie == null && accessCookie == null) {
            return Optional.empty();
        } else if (accessCookie == null) {
            return Optional.of(getRefreshedTokens(refreshCookie, response));
        } else if (refreshCookie == null) {
            return Optional.empty();
        } else {
            return Optional.of(
                new TokenResponse(
                    accessCookie.getValue(),
                    refreshCookie.getValue(),
                    accessTokenAge));
        }
    }


    public TokenResponse getRefreshedTokens(Cookie refreshCookie, HttpServletResponse response) {

        TokenResponse tokens = authService
            .refreshTokens(new RefreshTokenRequest(refreshCookie.getValue()));

        addAccessAndRefreshToken(tokens, response);

        return tokens;
    }

    public void cleanAccessAndRefreshTokenCookie(HttpServletResponse response) {
        cookieManager.eraseCookies(response,
            List.of(CookieProperties.ACCESS_TOKEN, CookieProperties.REFRESH_TOKEN));
    }


    public void addAccessAndRefreshToken(TokenResponse tokens, HttpServletResponse response) {
        addAccessToken(tokens.accessToken(), response);
        addRefreshToken(tokens.refreshToken(), response);
    }

    private void addAccessToken(String accessToken, HttpServletResponse response) {
        Cookie accessCookie = cookieManager
            .createCookie(CookieProperties.ACCESS_TOKEN, accessToken, accessTokenAge);
        response.addCookie(accessCookie);
    }

    private void addRefreshToken(String refreshToken, HttpServletResponse response) {
        Cookie refreshCookie = cookieManager
            .createCookie(CookieProperties.REFRESH_TOKEN, refreshToken, refreshTokenAge);
        response.addCookie(refreshCookie);
    }

}
