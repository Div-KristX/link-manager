package com.nixsolutions.linkmanager.ui.data.user;

import com.nixsolutions.linkmanager.ui.service.validation.FieldsAreUnique;
import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

@FieldsAreUnique.List({
    @FieldsAreUnique(
        field = "oldPassword",
        fieldMatch = "newPassword",
        message = "Old password equals New password"
    )
})
public record ChangePasswordRequest(

    @NotBlank(message = "Password content is mandatory")
    @Length(min = 8, message = "Password min length is 8")
    String oldPassword,

    @NotBlank(message = "Password content is mandatory")
    @Length(min = 8, message = "Password min length is 8")
    String newPassword
) {

}
