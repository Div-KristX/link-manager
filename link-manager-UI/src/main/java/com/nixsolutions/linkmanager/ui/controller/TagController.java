package com.nixsolutions.linkmanager.ui.controller;

import com.nixsolutions.linkmanager.ui.data.auth.TokenResponse;
import com.nixsolutions.linkmanager.ui.data.tag.Tag;
import com.nixsolutions.linkmanager.ui.data.tag.TagCreateRequest;
import com.nixsolutions.linkmanager.ui.data.tag.UpdateTag;
import com.nixsolutions.linkmanager.ui.data.user.User;
import com.nixsolutions.linkmanager.ui.service.TagService;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieManagement;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieProperties;
import com.nixsolutions.linkmanager.ui.service.loader.UserInfoLoader;
import com.nixsolutions.linkmanager.ui.service.pagination.ModelPagination;
import com.nixsolutions.linkmanager.ui.service.token.TokenRefresher;
import com.nixsolutions.linkmanager.ui.service.token.UserAccessChecker;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

@Controller
public class TagController {

    private static final Logger log = LoggerFactory.getLogger(TagController.class);
    private final TagService tagService;
    private final CookieManagement cookieManager;
    private final UserAccessChecker checker;

    private final UserInfoLoader userInfoLoader;

    private final ModelPagination modelPagination;
    private final TokenRefresher refresher;

    public TagController(TagService tagService, CookieManagement cookieManager,
        UserAccessChecker checker,
        UserInfoLoader userInfoLoader, ModelPagination modelPagination, TokenRefresher refresher) {
        this.tagService = tagService;
        this.cookieManager = cookieManager;
        this.checker = checker;
        this.userInfoLoader = userInfoLoader;
        this.modelPagination = modelPagination;
        this.refresher = refresher;
    }

    @GetMapping("/tags/search")
    public String forwardToLinkType(@RequestParam(name = "tagId") String id) {
        return "redirect:/tags/" + id;
    }

    @GetMapping("/tags")
    public String listLinkTypes(
        Model model,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @CookieValue(name = CookieProperties.LINK_CREATION_STAGE, required = false) Cookie linkCreation,
        @RequestParam(defaultValue = "1") int page,
        @RequestParam(defaultValue = "6") int size,
        @RequestParam(required = false) String keyword,
        @RequestParam(name = "sort", required = false) Optional<String> sort,
        HttpServletResponse response
    ) {
        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);
        if (checker.isAnonymous(tokens)) {
            userInfoLoader.setModelAttributesForAnonymous(model);
        } else {
            User user = userInfoLoader.loadUser(tokens.get().accessToken());
            userInfoLoader.setModelAttributesForUser(user, model);
        }
        try {
            if (keyword != null) {
                model.addAttribute("keyword", keyword);
            }
            if (linkCreation != null) {
                model.addAttribute("linkCreationStage", true);
            } else {
                model.addAttribute("linkCreationStage", false);
            }
            Page<Tag> responsePage = tagService.listAll(page - 1, size,
                Optional.ofNullable(keyword),
                sort);
            modelPagination.setPagesToModel(responsePage, model);
            model.addAttribute("pageSize", size);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
        return "tags/tags";
    }

    @PostMapping("/tags/for-link/{id}")
    public String chooseLinkType(
        @PathVariable Long id,
        HttpServletResponse response
    ) {
        Cookie chosenTypeId = cookieManager.createWithEncodedValue(
            CookieProperties.TAG_FOR_LINK, String.valueOf(id), 600);
        response.addCookie(chosenTypeId);
        return "redirect:/links/add";
    }


    @GetMapping("/tags/{id}")
    public String updateLinkType(
        @ModelAttribute("updateTag")
        UpdateTag updateTag,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @CookieValue(name = CookieProperties.LINK_CREATION_STAGE, required = false) Cookie linkCreation,
        HttpServletResponse response,
        @PathVariable
        Long id,
        Model model) {
        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);
        if (checker.isAnonymous(tokens)) {
            userInfoLoader.setModelAttributesForAnonymous(model);
        } else {
            User user = userInfoLoader.loadUser(tokens.get().accessToken());
            userInfoLoader.setModelAttributesForUser(user, model);
        }
        try {
            Tag tagById = tagService.getById(id);
            model.addAttribute("tagById", tagById);
            if (linkCreation != null) {
                model.addAttribute("linkCreationStage", true);
            } else {
                model.addAttribute("linkCreationStage", false);
            }
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        return "tags/tagById";
    }

    @PutMapping("/tags/{id}")
    public String updateLinkType(
        @Valid
        @ModelAttribute("updateTag")
        UpdateTag updateTag,
        BindingResult result,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        @PathVariable
        Long id,
        Model model) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        userInfoLoader.setModelAttributesForUser(user, model);

        Tag tagById = tagService.getById(id);
        model.addAttribute("tagById", tagById);

        if (result.hasErrors()) {
            return "tags/tagById";
        }
        try {
            tagService.update(tokens.get().accessToken(), id, updateTag);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            model.addAttribute("updateError", "Name is not correct");
            return "tags/tagById";
        }

        return "redirect:/tags";
    }


    @DeleteMapping("/tags/{id}")
    public String updateLinkType(
        @PathVariable Long id,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response
    ) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        try {
            tagService.delete(tokens.get().accessToken(), id);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        return "redirect:/tags";
    }

    @GetMapping("/tags/add")
    public String addLinkType(
        @ModelAttribute("newTag") TagCreateRequest request,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        Model model
    ) {
        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        userInfoLoader.setModelAttributesForUser(user, model);

        return "tags/tag-add";
    }


    @PostMapping("/tags/add")
    public String addLinkType(
        @Valid @ModelAttribute("newTag") TagCreateRequest request,
        BindingResult result,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        Model model,
        HttpServletResponse response) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        userInfoLoader.setModelAttributesForUser(user, model);

        if (result.hasErrors()) {
            return "tags/tag-add";
        }
        try {
            tagService.createTag(tokens.get().accessToken(), request);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            model.addAttribute("tagCreationError", "Already exists");
            return "tags/tag-add";
        }
        model.addAttribute("tagWasCreated", true);
        return "tags/tag-add";
    }
}
