package com.nixsolutions.linkmanager.ui.data.auth;

public record RefreshTokenRequest(
    String refreshToken
) {

}
