package com.nixsolutions.linkmanager.ui.service.headers;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.MultiValueMap;

public class HttpHeaderWrap {

    private final HttpHeaders headers;

    private HttpHeaderWrap(HttpHeaderBuilder builder) {

        headers = new HttpHeaders();

        headers.addAll(builder.httpHeaderValues);

        if (builder.mediaType != null) {
            headers.setContentType(builder.mediaType);
        }
        if (!builder.listOfMediaType.isEmpty()) {
            headers.setAccept(builder.listOfMediaType);
        }

    }

    public HttpHeaders getHeader() {
        return headers;
    }

    public static class HttpHeaderBuilder {

        private final MultiValueMap<String, String> httpHeaderValues = new HttpHeaders();

        private MediaType mediaType = MediaType.ALL;

        private List<MediaType> listOfMediaType = new ArrayList<>();

        public HttpHeaderBuilder withAuthorization(String authorizationValue) {
            httpHeaderValues.add(HttpHeaders.AUTHORIZATION, "Bearer " + authorizationValue);
            return this;
        }

        public HttpHeaderBuilder withAddedKeyValue(String name, String value) {
            httpHeaderValues.add(name, value);
            return this;
        }

        public HttpHeaderBuilder withContentType(MediaType mediaType) {
            this.mediaType = mediaType;
            return this;
        }

        public HttpHeaderBuilder noContentType() {
            mediaType = null;
            return this;
        }

        public HttpHeaderBuilder withAccessedContentType(List<MediaType> mediaTypes) {
            this.listOfMediaType = mediaTypes;
            return this;
        }

        public HttpHeaderWrap build() {
            return new HttpHeaderWrap(this);
        }
    }
}

