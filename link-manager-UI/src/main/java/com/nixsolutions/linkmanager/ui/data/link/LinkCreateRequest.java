package com.nixsolutions.linkmanager.ui.data.link;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class LinkCreateRequest {

    @NotBlank(message = "Must be not empty")
    @Pattern(regexp = "((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{1,256}([-a-zA-Z0-9@:%._\\+~#?&//=]*).+", message = "Incorrect hyperlink structure")
    private String hyperlink;

    @NotBlank(message = "Must be not empty")
    private String description;

    private Long tag;

    private Boolean important;

    public String getHyperlink() {
        return hyperlink;
    }

    public void setHyperlink(String hyperlink) {
        this.hyperlink = hyperlink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getTag() {
        return tag;
    }

    public void setTag(Long tag) {
        this.tag = tag;
    }

    public Boolean getImportant() {
        return important;
    }

    public void setImportant(Boolean important) {
        this.important = important;
    }
}
