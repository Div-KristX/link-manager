package com.nixsolutions.linkmanager.ui.service;

import com.nixsolutions.linkmanager.ui.data.tag.Tag;
import com.nixsolutions.linkmanager.ui.data.tag.TagCreateRequest;
import com.nixsolutions.linkmanager.ui.data.tag.UpdateTag;
import com.nixsolutions.linkmanager.ui.service.headers.HttpHeaderWrap;
import com.nixsolutions.linkmanager.ui.service.pagination.RestPageImpl;
import com.nixsolutions.linkmanager.ui.service.uri.UriComponentsStructure;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TagService {

    private final RestTemplate rest;

    private final UriComponentsStructure structure;

    @Value("${linkmanager.api.endpoint.tag}")
    private String TAG_API_URL;

    public TagService(RestTemplate rest, UriComponentsStructure structure) {
        this.rest = rest;
        this.structure = structure;
    }

    public Page<Tag> listAll(int page, int size, Optional<String> keyword,
        Optional<String> sort) {

        Map<String, String> params = new LinkedHashMap<>();

        params.put("page", String.valueOf(page));

        params.put("size", String.valueOf(size));

        keyword.ifPresent(k -> params.put("keyword", k));

        sort.ifPresent(s -> params.put("sort", s));

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .noContentType()
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<?> requestEntity = new HttpEntity<>(header.getHeader());

        ParameterizedTypeReference<RestPageImpl<Tag>> responseType = new ParameterizedTypeReference<>() {
        };

        ResponseEntity<RestPageImpl<Tag>> response = rest.exchange(
            structure.createURI(TAG_API_URL, params),
            HttpMethod.GET,
            requestEntity,
            responseType
        );

        return response.getBody();
    }

    public Tag getById(Long id) {
        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .noContentType()
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<?> requestEntity = new HttpEntity<>(header.getHeader());

        ResponseEntity<Tag> TagIdResponse = rest.exchange(
            TAG_API_URL + "/" + id,
            HttpMethod.GET,
            requestEntity,
            Tag.class);

        return TagIdResponse.getBody();

    }


    public Tag createTag(String accessToken, TagCreateRequest request) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<TagCreateRequest> requestEntity = new HttpEntity<>(request,
            header.getHeader());

        ResponseEntity<Tag> createdTagResponse = rest.exchange(
            TAG_API_URL,
            HttpMethod.POST,
            requestEntity,
            Tag.class);

        return createdTagResponse.getBody();
    }

    public Tag update(String accessToken, Long id, UpdateTag request) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<UpdateTag> requestEntity = new HttpEntity<>(request,
            header.getHeader());

        ResponseEntity<Tag> createdTagResponse = rest.exchange(
            TAG_API_URL + "/" + id,
            HttpMethod.PUT,
            requestEntity,
            Tag.class);

        return createdTagResponse.getBody();
    }

    public void delete(String accessToken, Long id) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<?> requestEntity = new HttpEntity<>(header.getHeader());

        rest.exchange(TAG_API_URL + "/" + id, HttpMethod.DELETE, requestEntity, Void.class);
    }
}
