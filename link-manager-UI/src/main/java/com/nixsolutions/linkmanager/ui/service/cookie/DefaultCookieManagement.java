package com.nixsolutions.linkmanager.ui.service.cookie;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;

@Component
public class DefaultCookieManagement implements CookieManagement {

    public Cookie createWithEncodedValue(String name, String value, int maxAge) {
        return createCookie(name, URLEncoder.encode(value, StandardCharsets.UTF_8), maxAge);
    }

    public String getDecodedValue(Cookie cookie) {
        return URLDecoder.decode(cookie.getValue(), StandardCharsets.UTF_8);
    }

    public Cookie createCookie(String name, String value, int maxAge) {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(maxAge);
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        return cookie;
    }

    public void eraseCookie(HttpServletResponse response, String cookieName) {
        Cookie cookieToErase = createCookie(cookieName, "", 0);
        response.addCookie(cookieToErase);
    }

    public void eraseCookies(HttpServletResponse response, List<String> cookieNames) {

        for (String cookieName : cookieNames) {
            Cookie templateCookie = createCookie(cookieName, "", 0);
            response.addCookie(templateCookie);
        }
    }
}
