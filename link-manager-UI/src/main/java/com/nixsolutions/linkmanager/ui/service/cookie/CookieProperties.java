package com.nixsolutions.linkmanager.ui.service.cookie;

public class CookieProperties {

    public final static String ACCESS_TOKEN = "accessToken";

    public final static String REFRESH_TOKEN = "refreshToken";

    public final static String CHANGE_USER_ACCOUNT = "changeAccountReqAccount";

    public final static String CHANGE_USER_ACCOUNT_NAME = "changeAccountReqName";

    public final static String LINK_CREATION_STAGE = "linkCreationStage";

    public final static String TAG_FOR_LINK = "chosenTagId";

    public final static String SHARING_INVITE_LINK = "shareSelectedLinkId";

    public final static String SHARING_INVITE_DEST_USER = "shareDestinationUserNick";

}
