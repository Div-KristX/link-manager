package com.nixsolutions.linkmanager.ui.data.account;

public record AccountType(
    Long id,
    String name,
    Integer maxLinks
) {

}
