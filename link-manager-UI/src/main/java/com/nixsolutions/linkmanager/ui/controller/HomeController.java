package com.nixsolutions.linkmanager.ui.controller;

import com.nixsolutions.linkmanager.ui.data.auth.TokenResponse;
import com.nixsolutions.linkmanager.ui.data.user.User;
import com.nixsolutions.linkmanager.ui.service.UserService;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieManagement;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieProperties;
import com.nixsolutions.linkmanager.ui.service.token.TokenRefresher;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

@Controller
public class HomeController {

    private final UserService service;

    private final TokenRefresher refresher;

    private final CookieManagement cookieManagement;

    public HomeController(UserService service,
        TokenRefresher refresher, CookieManagement cookieManagement) {
        this.service = service;
        this.refresher = refresher;
        this.cookieManagement = cookieManagement;
    }

    @GetMapping
    public String home(Model model,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response) {
        Optional<TokenResponse> tokens = Optional.empty();
        try {
            tokens = refresher.getTokens(
                accessCookie,
                refreshCookie, response);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.UNAUTHORIZED)){
                cookieManagement.eraseCookies(
                    response, List.of(
                        CookieProperties.ACCESS_TOKEN,
                        CookieProperties.REFRESH_TOKEN)
                );
                tokens = Optional.empty();
            }
        }
        if (tokens.isEmpty()) {
            model.addAttribute("isLoggedIn", false);
        } else {
            User currentUser = service.getCurrentUser(tokens.get().accessToken());
            model.addAttribute("nickname", currentUser.nickname());
            model.addAttribute("isLoggedIn", true);
        }

        return "home";
    }
}
