package com.nixsolutions.linkmanager.ui.data.sharing;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

public record LinkShareRequest(

    @NotBlank
    String originUser,

    @NotBlank
    String destinationUser,

    @Positive
    Long linkId

) {

}
