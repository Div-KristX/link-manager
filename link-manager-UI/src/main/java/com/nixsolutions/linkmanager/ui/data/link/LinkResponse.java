package com.nixsolutions.linkmanager.ui.data.link;

import com.nixsolutions.linkmanager.ui.data.tag.Tag;

public record LinkResponse(
    Long id,
    String hyperlink,
    String description,
    Tag tag,
    Boolean important

) {

}
