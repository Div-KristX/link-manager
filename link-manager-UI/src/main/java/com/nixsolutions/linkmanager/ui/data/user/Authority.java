package com.nixsolutions.linkmanager.ui.data.user;

public enum Authority {
    ROLE_USER, ROLE_ADMIN
}
