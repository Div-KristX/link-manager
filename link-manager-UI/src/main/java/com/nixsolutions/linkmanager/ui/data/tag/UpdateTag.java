package com.nixsolutions.linkmanager.ui.data.tag;

import javax.validation.constraints.NotBlank;

public class UpdateTag {

    @NotBlank(message = "Must be not empty")
    private String tag;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
