package com.nixsolutions.linkmanager.ui.controller;


import com.nixsolutions.linkmanager.ui.data.auth.TokenResponse;
import com.nixsolutions.linkmanager.ui.data.link.LinkCreateRequest;
import com.nixsolutions.linkmanager.ui.data.link.LinkResponse;
import com.nixsolutions.linkmanager.ui.data.user.User;
import com.nixsolutions.linkmanager.ui.service.LinkService;
import com.nixsolutions.linkmanager.ui.service.UserService;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieManagement;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieProperties;
import com.nixsolutions.linkmanager.ui.service.loader.UserInfoLoader;
import com.nixsolutions.linkmanager.ui.service.pagination.ModelPagination;
import com.nixsolutions.linkmanager.ui.service.token.TokenRefresher;
import com.nixsolutions.linkmanager.ui.service.token.UserAccessChecker;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

@Controller
public class LinkController {

    private static final Logger log = LoggerFactory.getLogger(LinkController.class);
    private final UserAccessChecker checker;

    private final UserInfoLoader userInfoLoader;

    private final TokenRefresher refresher;

    private final UserService userService;

    private final LinkService linkService;

    private final ModelPagination modelPagination;

    private final CookieManagement cookieManager;

    public LinkController(UserAccessChecker checker, UserInfoLoader userInfoLoader,
        TokenRefresher refresher, UserService userService,
        LinkService linkService, ModelPagination modelPagination, CookieManagement cookieManager) {
        this.checker = checker;
        this.userInfoLoader = userInfoLoader;
        this.refresher = refresher;
        this.userService = userService;
        this.linkService = linkService;
        this.modelPagination = modelPagination;
        this.cookieManager = cookieManager;
    }

    @GetMapping("/links/search")
    public String forwardToLink(@RequestParam(name = "linkId") String id) {
        return "redirect:/links/" + id;
    }

    @GetMapping("/links")
    public String listLinks(
        Model model,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @RequestParam(defaultValue = "1") int page,
        @RequestParam(defaultValue = "6") int size,
        @RequestParam(required = false) String keyword,
        @RequestParam(required = false) Long tagId,
        @RequestParam(name = "sort", required = false) Optional<String> sort,
        HttpServletResponse response
    ) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        userInfoLoader.setModelAttributesForUser(user, model);

        try {
            if (keyword != null) {
                model.addAttribute("keyword", keyword);
            }
            if (tagId != null) {
                model.addAttribute("tagId", tagId);
            }
            Page<LinkResponse> responsePage = linkService.listAll(tokens.get().accessToken(),
                page - 1, size, Optional.ofNullable(keyword), Optional.ofNullable(tagId), sort);
            modelPagination.setPagesToModel(responsePage, model);

            model.addAttribute("pageSize", size);

        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        return "links/links";
    }

    @GetMapping("users/{id}/links")
    public String getUserById(
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @RequestParam(defaultValue = "1") int page,
        @RequestParam(defaultValue = "6") int size,
        @PathVariable Long id,
        Model model,
        HttpServletResponse response
    ) {
        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        userInfoLoader.setModelAttributesForUser(user, model);
        try {
            User requestedUser = userService.getUserById(tokens.get().accessToken(), id);
            model.addAttribute("requestedUser", requestedUser);
            Page<LinkResponse> responsePage = linkService.listLinksForUser(tokens.get().accessToken(),
                id,
                page - 1, size);
            modelPagination.setPagesToModel(responsePage, model);
            model.addAttribute("pageSize", size);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        return "links/userlinks";
    }


    @DeleteMapping("/links/{id}")
    public String deleteLink(
        @PathVariable Long id,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response
    ) {
        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);


        try {
            linkService.deleteLinkById(tokens.get().accessToken(), id);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        return "redirect:/links";
    }

    @PutMapping("/links/{id}")
    public String updateLink(
        @Valid
        @ModelAttribute("updateLink")
        LinkCreateRequest updateLink,
        BindingResult result,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        @PathVariable
        Long id,
        Model model
    ) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);
        User user = userInfoLoader.loadUser(tokens.get().accessToken());
        LinkResponse linkById;

        try {
            linkById = linkService.getLinkById(tokens.get().accessToken(), id);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        model.addAttribute("linkById", linkById);
        userInfoLoader.setModelAttributesForUser(user, model);
        if (result.hasErrors()) {
            return "links/linkById";
        }
        try {
            linkService.updateLink(tokens.get().accessToken(), linkById.id(), updateLink);
        } catch (HttpClientErrorException e) {
            model.addAttribute("updateError", "One field was not filled");
            log.error(e.getMessage());
            return "links/linkById";
        }
        return "redirect:/links";
    }

    @GetMapping("/links/{id}")
    public String updateLink(
        @ModelAttribute("updateLink")
        LinkCreateRequest updateLink,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        @PathVariable
        Long id,
        Model model
    ) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        userInfoLoader.setModelAttributesForUser(user, model);

        try {
            LinkResponse linkById = linkService.getLinkById(tokens.get().accessToken(), id);
            model.addAttribute("linkById", linkById);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }

        return "links/linkById";

    }

    @PostMapping("links/add/decline")
    public String declineLinkCreation(
        HttpServletResponse response) {
        cookieManager.eraseCookies(response,
            List.of(CookieProperties.LINK_CREATION_STAGE, CookieProperties.TAG_FOR_LINK));
        return "redirect:/links/add";
    }

    @PostMapping("links/add/start")
    public String startLinkCreation(HttpServletResponse response) {
        Cookie creationStage = cookieManager.createCookie(CookieProperties.LINK_CREATION_STAGE, "1",
            900);
        response.addCookie(creationStage);
        return "redirect:/links/add";
    }


    @GetMapping("/links/add")
    public String addNewLink(
        @ModelAttribute("newLink") LinkCreateRequest request,
        Model model,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @CookieValue(name = CookieProperties.LINK_CREATION_STAGE, required = false) Cookie linkCreation,
        @CookieValue(name = CookieProperties.TAG_FOR_LINK, required = false) Cookie chosenTypeId,
        HttpServletResponse response) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        userInfoLoader.setModelAttributesForUser(user, model);

        if (linkCreation != null) {
            model.addAttribute("linkCreationStage", true);
        }
        if (chosenTypeId != null) {
            model.addAttribute("tagChosen", true);
            model.addAttribute("chosenTag", chosenTypeId.getValue());
        }

        return "links/links-add";
    }

    @PostMapping("/links/add")
    public String addNewLink(
        @Valid
        @ModelAttribute("newLink") LinkCreateRequest request,
        BindingResult result,
        Model model,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @CookieValue(name = CookieProperties.TAG_FOR_LINK, required = false) Cookie chosenTypeId,
        @CookieValue(name = CookieProperties.LINK_CREATION_STAGE, required = false) Cookie linkCreation,
        HttpServletResponse response) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        userInfoLoader.setModelAttributesForUser(user, model);
        if (result.hasErrors()) {
            if (linkCreation != null) {
                model.addAttribute("linkCreationStage", true);
            }
            if (chosenTypeId != null) {
                model.addAttribute("tagChosen", true);
                model.addAttribute("chosenTag", chosenTypeId.getValue());
            }
            return "links/links-add";
        }
        try {
            if (request.getTag() == null && chosenTypeId != null) {
                request.setTag(Long.parseLong(chosenTypeId.getValue()));
            }
            linkService.createLink(tokens.get().accessToken(), request);
        } catch (HttpClientErrorException e) {
            model.addAttribute("creationError", "Something went wrong");
            log.error(e.getMessage());
            return "links/links-add";
        }
        cookieManager.eraseCookies(response,
            List.of(CookieProperties.LINK_CREATION_STAGE, CookieProperties.TAG_FOR_LINK));
        return "redirect:/links";
    }
}
