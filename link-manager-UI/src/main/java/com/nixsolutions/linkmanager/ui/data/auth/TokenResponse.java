package com.nixsolutions.linkmanager.ui.data.auth;

public record TokenResponse(
    String accessToken,
    String refreshToken,

    long expireIn
) {

}
