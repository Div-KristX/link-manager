package com.nixsolutions.linkmanager.ui.data.auth;

import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

public class LoginDTO {

    @NotBlank(message = "Login content is mandatory")
    @Length(min = 4, max = 20, message = "Login length from 4 to 20")
    private String login;
    @NotBlank(message = "Password content is mandatory")
    @Length(min = 8, message = "Password min length is 8")
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
