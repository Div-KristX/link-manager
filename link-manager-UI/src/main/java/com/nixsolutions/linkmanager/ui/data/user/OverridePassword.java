package com.nixsolutions.linkmanager.ui.data.user;

import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

public record OverridePassword(

    @NotBlank(message = "Password content is mandatory")
    @Length(min = 8, message = "Password min length is 8")
    String newPassword
) {

}
