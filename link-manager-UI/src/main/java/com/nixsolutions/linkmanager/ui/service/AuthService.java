package com.nixsolutions.linkmanager.ui.service;

import com.nixsolutions.linkmanager.ui.data.auth.LoginDTO;
import com.nixsolutions.linkmanager.ui.data.auth.LogoutDTO;
import com.nixsolutions.linkmanager.ui.data.auth.RefreshTokenRequest;
import com.nixsolutions.linkmanager.ui.data.auth.TokenResponse;
import com.nixsolutions.linkmanager.ui.service.headers.HttpHeaderWrap;
import java.util.Collections;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@Service
public class AuthService {

    private final RestTemplate restTemplate;


    @Value("${linkmanager.api.endpoint.auth}")
    private String AUTH_API_URL;

    public AuthService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    public TokenResponse sendLoginInfo(LoginDTO login) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<LoginDTO> httpLoginEntity = new HttpEntity<>(login,
            header.getHeader());

        ResponseEntity<TokenResponse> loginResponse = restTemplate.exchange(AUTH_API_URL, HttpMethod.POST,
            httpLoginEntity, TokenResponse.class);

        if (loginResponse.getStatusCode().is2xxSuccessful()) {
            return loginResponse.getBody();
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
    }

    public void logout(String accessToken, LogoutDTO logoutDTO) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<LogoutDTO> httpLogoutEntity = new HttpEntity<>(logoutDTO, header.getHeader());

        ResponseEntity<Void> logoutResponse = restTemplate.exchange(AUTH_API_URL + "/invalidate",
            HttpMethod.POST, httpLogoutEntity, Void.class);

        if (!logoutResponse.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
            throw new ResponseStatusException(logoutResponse.getStatusCode());
        }
    }

    public TokenResponse refreshTokens(RefreshTokenRequest request) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<RefreshTokenRequest> refreshRequest = new HttpEntity<>(request,
            header.getHeader());

        ResponseEntity<TokenResponse> newTokens = restTemplate.exchange(
            AUTH_API_URL + "/refresh",
            HttpMethod.POST,
            refreshRequest,
            TokenResponse.class
        );
        if (!newTokens.getStatusCode().is2xxSuccessful()) {
            throw new ResponseStatusException(newTokens.getStatusCode());
        }
        return newTokens.getBody();
    }

    public void pruneOldTokens(String accessToken) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .build();

        HttpEntity<Void> deleteRequest = new HttpEntity<>(header.getHeader());

        ResponseEntity<Void> deleteOldTokensResponse = restTemplate.exchange(
            AUTH_API_URL + "/old-tokens", HttpMethod.DELETE, deleteRequest, Void.class);

        if (!deleteOldTokensResponse.getStatusCode().is2xxSuccessful()) {
            throw new ResponseStatusException(deleteOldTokensResponse.getStatusCode());
        }
    }
}
