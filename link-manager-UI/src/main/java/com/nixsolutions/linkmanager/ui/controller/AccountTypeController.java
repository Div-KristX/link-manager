package com.nixsolutions.linkmanager.ui.controller;

import com.nixsolutions.linkmanager.ui.data.account.AccountType;
import com.nixsolutions.linkmanager.ui.data.account.AccountTypeCreateRequest;
import com.nixsolutions.linkmanager.ui.data.account.UpdateAccountType;
import com.nixsolutions.linkmanager.ui.data.auth.TokenResponse;
import com.nixsolutions.linkmanager.ui.data.user.User;
import com.nixsolutions.linkmanager.ui.service.AccountTypeService;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieProperties;
import com.nixsolutions.linkmanager.ui.service.loader.UserInfoLoader;
import com.nixsolutions.linkmanager.ui.service.pagination.ModelPagination;
import com.nixsolutions.linkmanager.ui.service.token.TokenRefresher;
import com.nixsolutions.linkmanager.ui.service.token.UserAccessChecker;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

@Controller
public class AccountTypeController {

    private static final Logger log = LoggerFactory.getLogger(AccountTypeController.class);

    private final UserInfoLoader userInfoLoader;

    private final UserAccessChecker checker;

    private final AccountTypeService accountTypeService;

    private final TokenRefresher refresher;

    private final ModelPagination modelPagination;

    public AccountTypeController(UserInfoLoader userInfoLoader,
        UserAccessChecker checker,
        AccountTypeService accountTypeService, TokenRefresher refresher,
        ModelPagination modelPagination) {
        this.userInfoLoader = userInfoLoader;
        this.checker = checker;
        this.accountTypeService = accountTypeService;
        this.refresher = refresher;
        this.modelPagination = modelPagination;
    }


    @GetMapping("/accounts/search")
    public String forwardToAccountType(@RequestParam(name = "typeId") String id) {
        return "redirect:/accounts/" + id;
    }

    @GetMapping("/accounts")
    public String listAccountTypes(
        Model model,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @CookieValue(name = CookieProperties.CHANGE_USER_ACCOUNT, required = false) Cookie accountChangeCookie,
        @RequestParam(defaultValue = "1") int page,
        @RequestParam(defaultValue = "6") int size,
        @RequestParam(required = false) String keyword,
        @RequestParam(name = "sort", required = false) Optional<String> sort,
        HttpServletResponse response
    ) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);
        if (checker.isAnonymous(tokens)) {
            userInfoLoader.setModelAttributesForAnonymous(model);
        } else {
            User user = userInfoLoader.loadUser(tokens.get().accessToken());
            userInfoLoader.setModelAttributesForUser(user, model);
        }
        try {
            if (keyword != null) {
                model.addAttribute("keyword", keyword);
            }
            Page<AccountType> responsePage = accountTypeService.listAll(page - 1, size,
                Optional.ofNullable(keyword),
                sort);
            modelPagination.setPagesToModel(responsePage, model);
            model.addAttribute("pageSize", size);
            if (accountChangeCookie != null) {
                model.addAttribute("accountChangeRequest", true);
            } else {
                model.addAttribute("accountChangeRequest", false);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
        return "account/accounts";
    }

    @GetMapping("/accounts/{id}")
    public String updateAccountType(
        @ModelAttribute("updateAccountType")
        UpdateAccountType updateAccountType,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @CookieValue(name = CookieProperties.CHANGE_USER_ACCOUNT, required = false) Cookie accountChangeStage,
        HttpServletResponse response,
        @PathVariable
        Long id,
        Model model) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);
        if (checker.isAnonymous(tokens)) {
            userInfoLoader.setModelAttributesForAnonymous(model);
        } else {
            User user = userInfoLoader.loadUser(tokens.get().accessToken());
            userInfoLoader.setModelAttributesForUser(user, model);
        }
        try {
            AccountType accountTypeById = accountTypeService.getById(id);

            model.addAttribute("accountType", accountTypeById);

            if (accountChangeStage != null) {
                model.addAttribute("accountChangeRequest", true);
            } else {
                model.addAttribute("accountChangeRequest", false);
            }
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        return "account/accountById";
    }


    @PutMapping("/accounts/{id}")
    public String updateAccountType(
        @Valid
        @ModelAttribute("updateAccountType")
        UpdateAccountType updateAccountTypeType,
        BindingResult result,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @ModelAttribute("accountType") AccountType accountType,
        HttpServletResponse response,
        @PathVariable
        Long id,
        Model model) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        userInfoLoader.setModelAttributesForUser(user, model);

        if (result.hasErrors()) {
            return "account/accountById";
        }
        try {
            accountTypeService.update(tokens.get().accessToken(), id, updateAccountTypeType);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            if (e.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
                model.addAttribute("updateError", "Name is not correct");
                return "account/accountById";
            } else {
                throw new ResponseStatusException(e.getStatusCode());
            }
        }
        return "redirect:/accounts/";
    }


    @DeleteMapping("/accounts/{id}")
    public String updateLinkType(
        @PathVariable Long id,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response
    ) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        try {
            accountTypeService.delete(tokens.get().accessToken(), id);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        return "redirect:/accounts";
    }

    @GetMapping("/accounts/add")
    public String addAccountType(
        @ModelAttribute("newAccountType") AccountTypeCreateRequest request,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        Model model
    ) {
        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        userInfoLoader.setModelAttributesForUser(user, model);

        return "account/account-add";

    }


    @PostMapping("/accounts/add")
    public String addAccountType(
        @Valid @ModelAttribute("newAccountType") AccountTypeCreateRequest request,
        BindingResult result,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        Model model,
        HttpServletResponse response) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        userInfoLoader.setModelAttributesForUser(user, model);

        if (result.hasErrors()) {
            return "account/account-add";
        }

        try {
            accountTypeService.createAccountType(tokens.get().accessToken(), request);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            if (e.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
                model.addAttribute("accountCreationError", "Already exists");
                return "account/account-add";
            } else {
                throw new ResponseStatusException(e.getStatusCode());
            }
        }
        model.addAttribute("accountWasCreated", true);
        return "account/account-add";
    }
}
