package com.nixsolutions.linkmanager.ui.service;

import com.nixsolutions.linkmanager.ui.data.link.LinkResponse;
import com.nixsolutions.linkmanager.ui.data.sharing.LinkShareRequest;
import com.nixsolutions.linkmanager.ui.data.sharing.LinkShareResponse;
import com.nixsolutions.linkmanager.ui.service.headers.HttpHeaderWrap;
import com.nixsolutions.linkmanager.ui.service.pagination.RestPageImpl;
import com.nixsolutions.linkmanager.ui.service.uri.UriComponentsStructure;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SharingService {

    private final RestTemplate rest;

    private final UriComponentsStructure structure;

    @Value("${linkmanager.api.endpoint.sharing}")
    private String SHARING_API_URL;


    public SharingService(RestTemplate rest, UriComponentsStructure structure) {
        this.rest = rest;
        this.structure = structure;
    }

    public Page<LinkShareResponse> listOffers(String accessToken, int page, int size) {
        return offersOrInvites(SHARING_API_URL + "/offers", accessToken, page, size);
    }

    public Page<LinkShareResponse> listInvites(String accessToken, int page, int size) {
        return offersOrInvites(SHARING_API_URL + "/invites", accessToken, page, size);
    }

    public LinkShareResponse createInvite(String accessToken, LinkShareRequest request) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<LinkShareRequest> requestEntity = new HttpEntity<>(request,
            header.getHeader());

        ResponseEntity<LinkShareResponse> createdInvite = rest.exchange(
            SHARING_API_URL + "/invites",
            HttpMethod.POST,
            requestEntity,
            LinkShareResponse.class);
        return createdInvite.getBody();
    }

    public LinkResponse acceptOffer(String accessToken, Long id) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .build();
        HttpEntity<?> request = new HttpEntity<>(header.getHeader());

        ResponseEntity<LinkResponse> createdInvite = rest.exchange(
            SHARING_API_URL + "/offers/" + id,
            HttpMethod.POST,
            request,
            LinkResponse.class);

        return createdInvite.getBody();
    }


    public void deleteInvite(String accessToken, Long id) {
        deleteById(SHARING_API_URL + "/invites/", id, accessToken);
    }

    public void declineOffer(String accessToken, Long id) {
        deleteById(SHARING_API_URL + "/offers/", id, accessToken);
    }

    private void deleteById(String apiEndpoint, Long id, String accessToken) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .build();
        HttpEntity<?> request = new HttpEntity<>(header.getHeader());

        rest.exchange(apiEndpoint + id, HttpMethod.DELETE, request, LinkShareResponse.class);
    }


    private Page<LinkShareResponse> offersOrInvites(String apiEndpoint, String accessToken,
        int page, int size) {

        Map<String, String> params = new LinkedHashMap<>();

        params.put("page", String.valueOf(page));

        params.put("size", String.valueOf(size));

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<?> requestEntity = new HttpEntity<>(header.getHeader());

        ParameterizedTypeReference<RestPageImpl<LinkShareResponse>> responseType = new ParameterizedTypeReference<>() {
        };

        ResponseEntity<RestPageImpl<LinkShareResponse>> response = rest.exchange(
            structure.createURI(apiEndpoint, params),
            HttpMethod.GET,
            requestEntity,
            responseType
        );
        return response.getBody();
    }

}
