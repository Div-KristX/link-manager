package com.nixsolutions.linkmanager.ui.data.sharing;

import com.nixsolutions.linkmanager.ui.data.link.LinkResponse;

public record LinkShareResponse(

    Long id,
    String originUser,
    String destinationUser,
    LinkResponse linkToShare) {

}
