package com.nixsolutions.linkmanager.ui.controller;

import com.nixsolutions.linkmanager.ui.data.auth.LoginDTO;
import com.nixsolutions.linkmanager.ui.data.auth.LogoutDTO;
import com.nixsolutions.linkmanager.ui.data.auth.TokenResponse;
import com.nixsolutions.linkmanager.ui.data.user.User;
import com.nixsolutions.linkmanager.ui.service.AuthService;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieProperties;
import com.nixsolutions.linkmanager.ui.service.loader.UserInfoLoader;
import com.nixsolutions.linkmanager.ui.service.token.TokenRefresher;
import com.nixsolutions.linkmanager.ui.service.token.UserAccessChecker;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

@Controller
public class AuthController {

    private final AuthService authService;

    private final UserInfoLoader userInfoLoader;
    private final TokenRefresher refresher;
    private final UserAccessChecker checker;

    public AuthController(AuthService authService, UserInfoLoader userInfoLoader,
        TokenRefresher refresher,
        UserAccessChecker checker) {
        this.authService = authService;
        this.userInfoLoader = userInfoLoader;
        this.refresher = refresher;
        this.checker = checker;
    }

    @GetMapping("/login")
    public String login(
        @ModelAttribute("loginForm") LoginDTO loginDTO,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);
        if (checker.isAnonymous(tokens)) {
            return "auth/login";
        } else {
            return "redirect:/links";
        }
    }

    @PostMapping("/login")
    public String login(
        @Valid @ModelAttribute("loginForm") LoginDTO loginDTO,
        BindingResult result,
        HttpServletResponse response,
        Model model) {

        if (result.hasErrors()) {
            return "auth/login";
        }

        TokenResponse tokenResponse;
        try {
            tokenResponse = authService.sendLoginInfo(loginDTO);
        } catch (HttpClientErrorException e) {
            model.addAttribute("loginError", "Incorrect login or password");
            return "auth/login";
        }
        refresher.addAccessAndRefreshToken(tokenResponse, response);
        return "redirect:/links";
    }


    @PostMapping("/logout")
    public String logout(
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response) {

        Optional<TokenResponse> tokens = refresher.getTokens(
            accessCookie,
            refreshCookie, response);

        if (checker.isAnonymous(tokens)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        try {
            authService.logout(tokens.get().accessToken(),
                new LogoutDTO(tokens.get().refreshToken()));
        } catch (HttpClientErrorException e) {
            throw new ResponseStatusException(e.getStatusCode());
        }

        refresher.cleanAccessAndRefreshTokenCookie(response);

        return "redirect:/";
    }

    @DeleteMapping("/old-tokens")
    public String deleteOldTokens(
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        checker.checkAdminAccess(user);

        authService.pruneOldTokens(tokens.get().accessToken());

        return "redirect:/users";
    }
}
