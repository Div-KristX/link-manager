package com.nixsolutions.linkmanager.ui.data.account;

import javax.validation.constraints.NotBlank;

public record ChangeAccountRequest(
    @NotBlank
    String accountType
) {

}
