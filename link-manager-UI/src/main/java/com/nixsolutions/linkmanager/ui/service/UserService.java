package com.nixsolutions.linkmanager.ui.service;

import com.nixsolutions.linkmanager.ui.data.account.ChangeAccountRequest;
import com.nixsolutions.linkmanager.ui.data.user.ChangePasswordRequest;
import com.nixsolutions.linkmanager.ui.data.user.ChangeUserStatusRequest;
import com.nixsolutions.linkmanager.ui.data.user.OverridePassword;
import com.nixsolutions.linkmanager.ui.data.user.RegisterNewUserRequest;
import com.nixsolutions.linkmanager.ui.data.user.User;
import com.nixsolutions.linkmanager.ui.service.headers.HttpHeaderWrap;
import com.nixsolutions.linkmanager.ui.service.pagination.RestPageImpl;
import com.nixsolutions.linkmanager.ui.service.uri.UriComponentsStructure;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class UserService {

    private final RestTemplate rest;

    private final UriComponentsStructure structure;


    @Value("${linkmanager.api.endpoint.user}")
    private String USER_API_URL;


    public UserService(RestTemplate rest, UriComponentsStructure structure) {
        this.rest = rest;
        this.structure = structure;
    }

    public User getCurrentUser(String accessToken) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .build();

        HttpEntity<String> request = new HttpEntity<>(accessToken, header.getHeader());

        ResponseEntity<User> currentUser = rest.exchange(
            USER_API_URL + "/me",
            HttpMethod.GET,
            request,
            User.class);

        return currentUser.getBody();
    }

    public Page<User> listAll(String accessToken, int page, int size,
        Optional<String> keyword,
        Optional<String> sort) {

        Map<String, String> params = new LinkedHashMap<>();

        params.put("page", String.valueOf(page));

        params.put("size", String.valueOf(size));

        keyword.ifPresent(k -> params.put("keyword", k));

        sort.ifPresent(s -> params.put("sort", s));

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<?> requestEntity = new HttpEntity<>(header.getHeader());

        ParameterizedTypeReference<RestPageImpl<User>> responseType = new ParameterizedTypeReference<>() {
        };

        ResponseEntity<RestPageImpl<User>> response = rest.exchange(
            structure.createURI(USER_API_URL, params),
            HttpMethod.GET,
            requestEntity,
            responseType
        );

        return response.getBody();
    }


    public void registerNewUser(RegisterNewUserRequest registerUser) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<RegisterNewUserRequest> request = new HttpEntity<>(registerUser,
            header.getHeader());

        rest.exchange(USER_API_URL, HttpMethod.POST, request, Void.class);
    }

    public void registerNewAdmin(String accessToken, RegisterNewUserRequest form) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<RegisterNewUserRequest> request = new HttpEntity<>(form, header.getHeader());

         rest.exchange(USER_API_URL + "/admins", HttpMethod.POST, request, Void.class);
    }


    public User changePasswordByCurrentUser(String accessToken,
        ChangePasswordRequest passwords) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<ChangePasswordRequest> request = new HttpEntity<>(passwords,
            header.getHeader());

        ResponseEntity<User> currentUser = rest.exchange(
            USER_API_URL + "/me/password",
            HttpMethod.PATCH,
            request,
            User.class);

        return currentUser.getBody();
    }

    public User changePasswordById(String accessToken, Long id,
        OverridePassword password) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<OverridePassword> request = new HttpEntity<>(password, header.getHeader());

        ResponseEntity<User> currentUser = rest.exchange(
            USER_API_URL + "/" + id + "/password",
            HttpMethod.PATCH,
            request,
            User.class);
        return currentUser.getBody();
    }

    public User getUserById(String accessToken, Long id) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();
        HttpEntity<?> request = new HttpEntity<>(header.getHeader());

        ResponseEntity<User> requestedUser = rest.exchange(
            USER_API_URL + "/" + id,
            HttpMethod.GET,
            request,
            User.class
        );

        return requestedUser.getBody();
    }

    public User changeStatusById(String accessToken, Long id,
        ChangeUserStatusRequest status) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<ChangeUserStatusRequest> request = new HttpEntity<>(status, header.getHeader());

        ResponseEntity<User> currentUser = rest.exchange(
            USER_API_URL + "/" + id + "/status",
            HttpMethod.PATCH,
            request,
            User.class);
        return currentUser.getBody();
    }

    public User changeAccountTypeById(String accessToken, Long id,
        ChangeAccountRequest requestedAccount) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .withContentType(MediaType.APPLICATION_JSON)
            .build();

        HttpEntity<ChangeAccountRequest> request = new HttpEntity<>(requestedAccount,
            header.getHeader());

        ResponseEntity<User> currentUser = rest.exchange(
            USER_API_URL + "/" + id + "/account",
            HttpMethod.PATCH,
            request,
            User.class);

        return currentUser.getBody();

    }


    public void deleteCurrentUser(String accessToken) {
        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .build();

        HttpEntity<String> request = new HttpEntity<>(accessToken, header.getHeader());

        rest.exchange(USER_API_URL + "/me", HttpMethod.DELETE, request, User.class);
    }

    public void deleteUserById(String accessToken, Long id) {

        // todo headers create in a class
        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .build();
        HttpEntity<?> request = new HttpEntity<>(header.getHeader());

        rest.exchange(USER_API_URL + "/" + id, HttpMethod.DELETE, request, User.class);
    }
}
