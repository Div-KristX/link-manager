package com.nixsolutions.linkmanager.ui.data.auth;

public record LogoutDTO(
    String refreshToken
) {

}
