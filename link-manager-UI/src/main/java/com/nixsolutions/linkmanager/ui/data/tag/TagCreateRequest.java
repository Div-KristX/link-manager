package com.nixsolutions.linkmanager.ui.data.tag;

import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

public class TagCreateRequest {

    @NotBlank(message = "Input name is empty")
    @Length(min = 2, message = "Name is too short")
    private String tag;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}


