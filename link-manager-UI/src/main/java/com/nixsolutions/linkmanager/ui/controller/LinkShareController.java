package com.nixsolutions.linkmanager.ui.controller;

import com.nixsolutions.linkmanager.ui.data.auth.TokenResponse;
import com.nixsolutions.linkmanager.ui.data.sharing.LinkShareRequest;
import com.nixsolutions.linkmanager.ui.data.sharing.LinkShareResponse;
import com.nixsolutions.linkmanager.ui.data.user.User;
import com.nixsolutions.linkmanager.ui.service.SharingService;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieManagement;
import com.nixsolutions.linkmanager.ui.service.cookie.CookieProperties;
import com.nixsolutions.linkmanager.ui.service.loader.UserInfoLoader;
import com.nixsolutions.linkmanager.ui.service.pagination.ModelPagination;
import com.nixsolutions.linkmanager.ui.service.token.TokenRefresher;
import com.nixsolutions.linkmanager.ui.service.token.UserAccessChecker;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

@Controller
public class LinkShareController {


    private static final Logger log = LoggerFactory.getLogger(LinkController.class);

    private final UserAccessChecker checker;

    private final TokenRefresher refresher;

    private final UserInfoLoader userInfoLoader;

    private final CookieManagement cookieManager;

    private final SharingService sharingService;

    private final ModelPagination modelPagination;


    public LinkShareController(UserAccessChecker checker, TokenRefresher refresher,
        UserInfoLoader userInfoLoader, CookieManagement cookieManager,
        SharingService sharingService, ModelPagination modelPagination) {
        this.checker = checker;
        this.refresher = refresher;
        this.userInfoLoader = userInfoLoader;
        this.cookieManager = cookieManager;
        this.sharingService = sharingService;
        this.modelPagination = modelPagination;
    }


    @GetMapping("/sharing/offers")
    public String offers(
        Model model,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @RequestParam(defaultValue = "1") int page,
        @RequestParam(defaultValue = "6") int size,
        HttpServletResponse response
    ) {
        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        userInfoLoader.setModelAttributesForUser(user, model);

        try {
            Page<LinkShareResponse> responsePage = sharingService.listOffers(
                tokens.get().accessToken(),
                page - 1, size);
            modelPagination.setPagesToModel(responsePage, model);
            model.addAttribute("pageSize", size);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        return "sharing/offers";
    }

    @GetMapping("/sharing/invites")
    public String invites(
        Model model,
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @CookieValue(name = CookieProperties.SHARING_INVITE_LINK, required = false) Cookie inviteCreation,
        @RequestParam(defaultValue = "1") int page,
        @RequestParam(defaultValue = "6") int size,
        HttpServletResponse response
    ) {
        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());

        userInfoLoader.setModelAttributesForUser(user, model);
        try {
            if (inviteCreation != null) {
                model.addAttribute("inviteCreating", true);
            }
            Page<LinkShareResponse> responsePage = sharingService.listInvites(
                tokens.get().accessToken(),
                page - 1, size);
            modelPagination.setPagesToModel(responsePage, model);
            model.addAttribute("pageSize", size);
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        return "sharing/invites";
    }

    @DeleteMapping("/sharing/invites/{id}")
    public String deleteInvite(
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        @PathVariable Long id
    ) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        sharingService.deleteInvite(tokens.get().accessToken(), id);

        return "redirect:/sharing/invites";
    }

    @DeleteMapping("/sharing/offers/{id}")
    public String declineOffer(
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        @PathVariable Long id
    ) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        sharingService.declineOffer(tokens.get().accessToken(), id);

        return "redirect:/sharing/offers";
    }

    @PostMapping("/sharing/offers/{id}")
    public String acceptOffer(
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        @PathVariable Long id
    ) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        sharingService.acceptOffer(tokens.get().accessToken(), id);

        return "redirect:/links";
    }

    @PostMapping("/users/share/{nickname}")
    public String selectUserToShare(
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        @PathVariable String nickname) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        Cookie linkIdCookie = cookieManager
            .createWithEncodedValue
                (CookieProperties.SHARING_INVITE_DEST_USER, nickname, 600);

        response.addCookie(linkIdCookie);
        return "redirect:/sharing/invites";
    }


    @PostMapping("/links/share/{id}")
    public String selectLinkToShare(
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        HttpServletResponse response,
        @PathVariable Long id) {

        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);

        checker.hasAccessToRead(tokens);

        Cookie linkIdCookie = cookieManager
            .createCookie(CookieProperties.SHARING_INVITE_LINK, String.valueOf(id), 600);

        response.addCookie(linkIdCookie);
        return "redirect:/users";
    }

    @PostMapping("/sharing/invites")
    public String createInvite(
        @CookieValue(name = CookieProperties.ACCESS_TOKEN, required = false) Cookie accessCookie,
        @CookieValue(name = CookieProperties.REFRESH_TOKEN, required = false) Cookie refreshCookie,
        @CookieValue(name = CookieProperties.SHARING_INVITE_DEST_USER, required = false) Cookie destUser,
        @CookieValue(name = CookieProperties.SHARING_INVITE_LINK, required = false) Cookie selectedLink,
        HttpServletResponse response
    ) {
        Optional<TokenResponse> tokens = refresher.getTokens(accessCookie, refreshCookie, response);
        checker.hasAccessToRead(tokens);

        User user = userInfoLoader.loadUser(tokens.get().accessToken());
        if (destUser == null || selectedLink == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        String destinationUser = cookieManager.getDecodedValue(destUser);

        if (destinationUser.equals(user.nickname())) {
            cookieManager.eraseCookie(response, CookieProperties.SHARING_INVITE_DEST_USER);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        Long selectedLinkId = Long.parseLong(selectedLink.getValue());

        try {
            sharingService.createInvite(tokens.get().accessToken(),
                new LinkShareRequest(user.nickname(), destinationUser, selectedLinkId));
        } catch (HttpClientErrorException e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(e.getStatusCode());
        }
        cookieManager.eraseCookies(response, List.of(CookieProperties.SHARING_INVITE_LINK,
            CookieProperties.SHARING_INVITE_DEST_USER));
        return "redirect:/sharing/invites";
    }
}
