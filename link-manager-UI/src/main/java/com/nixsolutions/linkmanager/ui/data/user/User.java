package com.nixsolutions.linkmanager.ui.data.user;

import com.nixsolutions.linkmanager.ui.data.account.AccountType;
import java.util.Set;

public record User(
    Long id,
    String nickname,

    AccountType accountType,

    UserStatus status,
    Set<Authority> authorities,

    Integer usedLinks
) {

}
