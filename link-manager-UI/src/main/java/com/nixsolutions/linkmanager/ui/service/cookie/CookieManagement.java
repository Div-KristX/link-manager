package com.nixsolutions.linkmanager.ui.service.cookie;

import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

public interface CookieManagement {

    Cookie createWithEncodedValue(String name, String value, int maxAge);

    String getDecodedValue(Cookie cookie);

    Cookie createCookie(String name, String value, int maxAge);

    void eraseCookie(HttpServletResponse response, String cookieName);

    void eraseCookies(HttpServletResponse response, List<String> cookieNames);
}
