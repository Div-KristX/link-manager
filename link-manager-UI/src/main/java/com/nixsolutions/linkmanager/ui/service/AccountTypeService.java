package com.nixsolutions.linkmanager.ui.service;

import com.nixsolutions.linkmanager.ui.data.account.AccountType;
import com.nixsolutions.linkmanager.ui.data.account.AccountTypeCreateRequest;
import com.nixsolutions.linkmanager.ui.data.account.UpdateAccountType;
import com.nixsolutions.linkmanager.ui.service.headers.HttpHeaderWrap;
import com.nixsolutions.linkmanager.ui.service.pagination.RestPageImpl;
import com.nixsolutions.linkmanager.ui.service.uri.UriComponentsStructure;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

@Service
public class AccountTypeService {

    private final RestTemplate rest;

    private final UriComponentsStructure structure;

    @Value("${linkmanager.api.endpoint.account}")
    private String ACCOUNT_TYPES_API_URL;


    public AccountTypeService(RestTemplate rest, UriComponentsStructure structure) {
        this.rest = rest;
        this.structure = structure;
    }

    public AccountType getById(Long id) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .noContentType()
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<?> requestEntity = new HttpEntity<>(header.getHeader());

        ResponseEntity<AccountType> accountIdResponse = rest.exchange(
            ACCOUNT_TYPES_API_URL + "/" + id,
            HttpMethod.GET,
            requestEntity,
            AccountType.class);
        if (accountIdResponse.getStatusCode().is2xxSuccessful()) {
            return accountIdResponse.getBody();
        }

        throw new ResponseStatusException(accountIdResponse.getStatusCode());
    }


    public Page<AccountType> listAll(int page, int size, Optional<String> keyword,
        Optional<String> sort) {

        Map<String, String> params = new LinkedHashMap<>();

        params.put("page", String.valueOf(page));

        params.put("size", String.valueOf(size));

        keyword.ifPresent(k -> params.put("keyword", k));

        sort.ifPresent(s -> params.put("sort", s));

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .noContentType()
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<?> requestEntity = new HttpEntity<>(header.getHeader());

        ParameterizedTypeReference<RestPageImpl<AccountType>> responseType = new ParameterizedTypeReference<>() {
        };

        ResponseEntity<RestPageImpl<AccountType>> response = rest.exchange(
            structure.createURI(ACCOUNT_TYPES_API_URL, params),
            HttpMethod.GET,
            requestEntity,
            responseType
        );
        return response.getBody();
    }

    public AccountType createAccountType(String accessToken, AccountTypeCreateRequest request) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<AccountTypeCreateRequest> requestEntity = new HttpEntity<>(request,
            header.getHeader());

        ResponseEntity<AccountType> createdAccount = rest.exchange(
            ACCOUNT_TYPES_API_URL,
            HttpMethod.POST,
            requestEntity,
            AccountType.class);

            return createdAccount.getBody();
    }


    public AccountType update(String accessToken, Long id, UpdateAccountType request) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .withContentType(MediaType.APPLICATION_JSON)
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<UpdateAccountType> requestEntity = new HttpEntity<>(request,
            header.getHeader());

        ResponseEntity<AccountType> createdLinkTypeResponse = rest.exchange(
            ACCOUNT_TYPES_API_URL + "/" + id,
            HttpMethod.PUT,
            requestEntity,
            AccountType.class);

        if (createdLinkTypeResponse.getStatusCode().is2xxSuccessful()) {
            return createdLinkTypeResponse.getBody();
        }

        throw new ResponseStatusException(createdLinkTypeResponse.getStatusCode());
    }

    public void delete(String accessToken, Long id) {

        HttpHeaderWrap header = new HttpHeaderWrap.HttpHeaderBuilder()
            .withAuthorization(accessToken)
            .noContentType()
            .withAccessedContentType(Collections.singletonList(MediaType.APPLICATION_JSON))
            .build();

        HttpEntity<?> requestEntity = new HttpEntity<>(header.getHeader());

        ResponseEntity<Void> deletedAccountResponse = rest.exchange(
            ACCOUNT_TYPES_API_URL + "/" + id,
            HttpMethod.DELETE,
            requestEntity,
            Void.class);

        if (!deletedAccountResponse.getStatusCode().is2xxSuccessful()) {
            throw new ResponseStatusException(deletedAccountResponse.getStatusCode());
        }
    }
}
