package com.nixsolutions.linkmanager.ui.service.token;

import com.nixsolutions.linkmanager.ui.data.auth.TokenResponse;
import com.nixsolutions.linkmanager.ui.data.user.Authority;
import com.nixsolutions.linkmanager.ui.data.user.User;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
public class UserAccessChecker {

    public boolean hasAccessToRead(Optional<TokenResponse> tokens) {
        if (tokens.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
        }
        return true;
    }

    public boolean isAnonymous(Optional<TokenResponse> tokens) {
        return tokens.isEmpty();
    }

    public boolean checkAdminAccess(User currentUser) {
        if (currentUser.authorities().contains(Authority.ROLE_ADMIN)) {
            return true;
        }
        throw new ResponseStatusException(HttpStatus.FORBIDDEN);
    }
}
