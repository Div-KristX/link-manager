package com.nixsolutions.linkmanager.ui.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("home");
        registry.addViewController("/login").setViewName("auth/login");
        registry.addViewController("/register").setViewName("auth/register");
        registry.addViewController("/admin").setViewName("auth/admin");
        registry.addViewController("/error").setViewName("/error");
        registry.addViewController("/tags").setViewName("tags/tags");
        registry.addViewController("/users").setViewName("users/users");
        registry.addViewController("/accounts").setViewName("account/accounts");
        registry.addViewController("/sharing/offers").setViewName("sharing/offers");
        registry.addViewController("/sharing/invites").setViewName("sharing/invites");
    }

}
