package com.nixsolutions.linkmanager.ui.service.loader;

import com.nixsolutions.linkmanager.ui.data.user.Authority;
import com.nixsolutions.linkmanager.ui.data.user.User;
import com.nixsolutions.linkmanager.ui.service.UserService;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

@Component
public class UserInfoLoader {

    private final UserService userService;

    public UserInfoLoader(UserService userService) {
        this.userService = userService;
    }

    public User loadUser(String accessToken) {
        User user;
        try {
            user = userService.getCurrentUser(accessToken);
        } catch (HttpClientErrorException e) {
            throw new ResponseStatusException(e.getStatusCode());
        }
        return user;
    }

    public void setModelAttributesForUser(User user, Model model) {
        model.addAttribute("viewerIsAdmin",
            user.authorities().contains(Authority.ROLE_ADMIN));
        model.addAttribute("viewer", user);
        model.addAttribute("nickname", user.nickname());
    }

    public void setModelAttributesForAnonymous(Model model) {
        model.addAttribute("viewerIsAdmin", false);
        model.addAttribute("nickname", "none");
        model.addAttribute("viewer", null);
    }
}
