const navElements = {
  "links_href": {
    ua: "Гіперпосилання",
    en: "Links"
  },
  "tags_href": {
    ua: "Теги",
    en: "Tags"
  },
  "account-types_href": {
    ua: "Види акаунтів",
    en: "Account-types"
  },
  "users_href": {
    ua: "Користувачі",
    en: "Users"
  },
  "sharing_href": {
    ua: "Обмін посиланнями",
    en: "Sharing"
  },
  "invites_href": {
    ua: "Мої пропозиції",
    en: "Invites"
  },
  "offers_href": {
    ua: "Пропозиції мені",
    en: "Offers"
  },
  "signIn_href": {
    ua: "Вхід",
    en: "Sign In"
  },
  "signUp_href": {
    ua: "Реєстрація",
    en: "Sign Up"
  },
  "signOut_href": {
    ua: "Вихід",
    en: "Sign Out"
  }
};

const userHomePage = {
  "nick_header": {
    ua: "Ім'я:",
    en: "Nickname:"
  },
  "accountType_header": {
    ua: "Тип аккаунта:",
    en: "Account type:"
  },
  "usedLinks_header": {
    ua: "Використані гіперпосилання",
    en: "Used links"
  },
  "changePass_header": {
    ua: "Заміна пароля",
    en: "Change password"
  },
  "shareTo_btn": {
    ua: "Поділитись з ним",
    en: "Share to"
  },
  "oldPass_label": {
    ua: "Старий пароль",
    en: "Old password"
  },
  "newPass_label": {
    ua: "Новий пароль",
    en: "New password"
  },
  "changePass_btn": {
    ua: "Заміна пароля",
    en: "Change password"
  },
  "deleteUser_btn": {
    ua: "Видалення профілю",
    en: "Delete profile"
  },
  "switchAccount_header": {
    ua: "Зміна типу аккаунту",
    en: "Switch user account"
  },
  "changeAccount_btn": {
    ua: "Змінити типу аккаунту",
    en: "Change account"
  },
  "changeAccountDecline_btn": {
    ua: "Відміна зміни типу аккаунту",
    en: "Decline account change"
  },
  "links_head": {
    ua: "Показати гіперпосилання користувача",
    en: "Show links"
  },
  "links_btn": {
    ua: "Гіперпосилання користувача",
    en: "User`s links"
  },
  "userDelete_btn": {
    ua: "Видалення профілю",
    en: "Delete profile"
  },
  "status_header": {
    ua: "Зміна статусу профілю",
    en: "Swtich user`s status"
  },
  "status_label": {
    ua: "Статус",
    en: "Status"
  },
  "status_btn": {
    ua: "Змінити статусу профілю",
    en: "Change status"
  },
  "passOverride_header": {
    ua: "Встановити пароль",
    en: "Override password"
  },
  "passOverride_label": {
    ua: "Новий пароль",
    en: "New password"
  },
  "passOverride_btn": {
    ua: "Встановити пароль",
    en: "Override password"
  },
};
const loginRegisterPage = {
  "register_title": {
    ua: "Реєстрація",
    en: "Registration"
  },
  "login_title": {
    ua: "Вхід",
    en: "Login"
  },
  "login_header": {
    ua: "Вхід",
    en: "Login"
  },
  "register_header": {
    ua: "Реєстрація",
    en: "Register"
  },
  "login_label": {
    ua: "Логін",
    en: "Login"
  },
  "pass_label": {
    ua: "Пароль",
    en: "Password"
  },
  "signIn_btn": {
    ua: "Увійти",
    en: "Sign In"
  },
  "register_btn": {
    ua: "Зареєструватись",
    en: "Register"
  },
  "registerAdmin_header": {
    ua: "Зареєструвати адміністратора",
    en: "Register admin"
  },
  "registerAdmin_btn": {
    ua: "Зареєструвати",
    en: "Register"
  },
  "return_btn": {
    ua: "Повернутись",
    en: "Return"
  },
  "admin_title": {
    ua: "Реєстрація адміністратора",
    en: "Register admin"
  },

};
const errorPage = {
  "error_title": {
    ua: "Помилка",
    en: "Error"
  },
  "wrongAttention_header": {
    ua: "Щось пішло не так:",
    en: "Something went wrong:"
  },
  "home_btn": {
    ua: "Повернутись до головної сторінки",
    en: "Back to Home Page"
  }
};
const common = {
  "adminActions_header": {
    ua: "Дії адміністратора",
    en: "Admin's actions"
  },
  "searchParam_header": {
    ua: "Пошук з параметрами",
    en: "Search by attributes"
  },
  "clearSearch_header": {
    ua: "Очистити параметри",
    en: "Clear attributes"
  },
  "clearSearch_btn": {
    ua: "Очистити",
    en: "Clear"
  },
  "search_btn": {
    ua: "Пошук",
    en: "Search"
  },
  "search_header": {
    ua: "Пошук",
    en: "Search"
  },
  "confirm_btn": {
    ua: "Підтвердити",
    en: "Confirm"
  },
  "searchElements_header": {
    ua: "Елементи",
    en: "Elements"
  },
  "searchSort_header": {
    ua: "Сортування",
    en: "Sort"
  },
  "searchId_btn": {
    ua: "Пошук за Id",
    en: "Search by Id"
  },
  "delete_btn": {
    ua: "Видалити",
    en: "Delete"
  },
  "actions_th": {
    ua: "Дії",
    en: "Actions"
  },
  "name_th": {
    ua: "Ім'я",
    en: "Name"
  },
  "hyperlink_th": {
    ua: "Гіперпосилання",
    en: "Hyperlink"
  },
  "description_th": {
    ua: "Опис",
    en: "Description"
  },
  "searchId_header": {
    ua: "Пошук за Id",
    en: "Search by Id"
  },
  "update_btn": {
    ua: "Оновити",
    en: "Update"
  },
}

const usersPage = {
  "users_title": {
    ua: "Користувачі",
    en: "Users"
  },
  "pruneOldTokens_btn": {
    ua: "Видалити старі токени",
    en: "Delete old tokens"
  },
  "account_th": {
    ua: "Тип аккаунту",
    en: "Account"
  },
  "status_th": {
    ua: "Статус користувача",
    en: "Status"
  },
  "linksCount_th": {
    ua: "Кількість гіперпосилань",
    en: "Links count"
  },
  "actions_th": {
    ua: "Дії",
    en: "Actions"
  },
  "shareTo_btn": {
    ua: "Поділитись",
    en: "Share to"
  },
  "acceptOffer_btn": {
    ua: "Прийняти пропозицію",
    en: "Accept offer"
  },
  "declineOffer_btn": {
    ua: "Відмовитись від пропозиції",
    en: "Decline offer"
  },
};

const accountsPage = {
  "accounts_title": {
    ua: "Види аккаунтів",
    en: "Account types"
  },
  "createAccount_btn": {
    ua: "Створити новий вид аккаунту",
    en: "Create new Account type"
  },
  "account_name": {
    ua: "Тип аккаунту",
    en: "Account name"
  },
  "accountName_header": {
    ua: "Ім'я аккаунту",
    en: "Account name:"
  },
  "maxLinks_th": {
    ua: "Максимальна кількість посилань",
    en: "Max links"
  },
  "changeAccount_act": {
    ua: "Зміна тип аккаунту",
    en: "Change user account"
  },
  "maxLinks_header": {
    ua: "Максимальна кількість посилань:",
    en: "Max links:"
  },
  "changeAccount_action": {
    ua: "Змінити на цей тип аккаунту на цей",
    en: "Change user account to this"
  },
  "confirmChange_btn": {
    ua: "Підтвердити зміну",
    en: "Confirm change"
  }
};
const invitesPage = {
  "invites_title": {
    ua: "Мої запрошення",
    en: "Invites"
  },
  "confirmInviteCreate_header": {
    ua: "Підтвердити створення цього запрошення",
    en: "Confirm this created Invite"
  },
  "forUser_th": {
    ua: "Для користувача",
    en: "For user"
  },
  "deleteInvite_btn": {
    ua: "Видалити запрошення",
    en: "Delete invite"
  }
};

const sharingPage = {
  "share_title": {
    ua: "Пропозиції мені",
    en: "Offers"
  },
  "offeredBy_th": {
    ua: "Запропоновано від",
    en: "Offered by"
  },
};

const tagsPage = {
  "tags_title": {
    ua: "Теги",
    en: "Tags"
  },
  "createTag_btn": {
    ua: "Створити новий тег",
    en: "Create new tag"
  },
  "tagName_header": {
    ua: "Ім'я тегу ",
    en: "Tag name "
  },
  "addToLink_btn": {
    ua: "Додати до гіперпосилання",
    en: "Add to link"
  },
};

const tagIDPage = {
  "tagAddToLink_btn": {
    ua: "Додати до посилання",
    en: "Add to link"
  }
};
const tagAddPage = {
  "successfulCreation_message": {
    ua: "Успішно створено",
    en: "Created successfully"
  },
  "createNewTag_btn": {
    ua: "Створити тег",
    en: "Create tag"
  }
};

const linksPage = {
  "links_title": {
    ua: "Посилання",
    en: "Links"
  },
  "share_btn": {
    ua: "Поділитись",
    en: "Share"
  },
  "tag_th": {
    ua: "Тег",
    en: "Tag"
  },
  "updateLink_btn": {
    ua: "Оновити це посилання",
    en: "Update this link"
  },
  "important_th": {
    ua: "Важливість",
    en: "Important"
  },
  "createLink_btn": {
    ua: "Створити нове посилання",
    en: "Create new Link"
  },
  "contains_header": {
    ua: "Містить в собі",
    en: "Contains"
  },
  "tagId_header": {
    ua: "Id тегу",
    en: "Tag id"
  },
};
const linkAddPage = {
  "linkAdd_title": {
    ua: "Створення посилання",
    en: "Link creation"
  },
  "declineLinkConstruct_btn": {
    ua: "Відмінити створення посилання",
    en: "Decline link construct"
  },
  "constructLink_btn": {
    ua: "Створити посилання",
    en: "Construct link"
  },
  "tag_header": {
    ua: "Тег",
    en: "Tag"
  },
  "tagIdEnter_header": {
    ua: "Ввести id-тегу",
    en: "Enter Tag-id"
  },
  "chosenTagId_header": {
    ua: "Обраний id-тегу:",
    en: "Chosen tag id:"
  },
  "chosenTag_header": {
    ua: "Або обрати з тегів",
    en: "Or choose from tags"
  },
  "choseTag_btn": {
    ua: "Обрати тег",
    en: "Choose tag"
  },
  "hyperlink_header": {
    ua: "Гіперпосилання - ",
    en: "Hyperlink - "
  },
  "description_header": {
    ua: "Опис - ",
    en: "Description - "
  },
  "important_header": {
    ua: "Важливість - ",
    en: "Important - "
  },
  "saveNewLink_btn": {
    ua: "Зберегти це посилання",
    en: "Save this link"
  },
}