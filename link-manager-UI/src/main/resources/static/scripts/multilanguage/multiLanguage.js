function changeLanguageToUa() {
  setCookie("languageSet", "ua", {age: "9999"});
  enableTranslateButton("en");
  disableTranslateButton("ua");
  translatePage("ua");
}

function changeLanguageToEn() {
  setCookie("languageSet", "en", {age: "9999"});
  disableTranslateButton("en");
  enableTranslateButton("ua");
  translatePage("en");
}

function setLanguageOnStart() {
  let cookie = getCookie("languageSet");
  if (cookie === undefined) {
    changeLanguageToEn();
  } else {
    if (cookie === "ua") {
      changeLanguageToUa();
    } else {
      changeLanguageToEn();
    }
  }
}

function disableTranslateButton(lang) {
  const selector = `[data-btn="${lang}"]`;
  const element = document.querySelector(selector);
  if (element) {
    element.style.disabled = true;
    element.style.color = "#7f7f7f";
  }
}

function enableTranslateButton(lang) {
  const selector = `[data-btn="${lang}"]`;
  const element = document.querySelector(selector);
  if (element) {
    element.style.disabled = false;
    element.style.color = "#FFFFFFFF";
  }
}

function translatePage(lang) {
  translateElements(navElements, lang);
  translateElements(userHomePage, lang);
  translateElements(loginRegisterPage, lang);
  translateElements(usersPage, lang);
  translateElements(accountsPage, lang);
  translateElements(errorPage, lang);
  translateElements(common, lang);
  translateElements(invitesPage, lang);
  translateElements(sharingPage, lang);
  translateElements(linksPage, lang);
  translateElements(tagsPage, lang);
  translateElements(linkAddPage,lang);
  translateElements(tagIDPage, lang);
  translateElements(tagAddPage, lang);
}

function translateElements(dictionary, lang) {
  for (let key in dictionary) {
    let selector = `[data-lang="${key}"]`;
    let elems = document.querySelectorAll(selector);
    for (let elem of elems.values()) {
      if (elem) {
        if (elem instanceof HTMLInputElement) {
          elem.value = dictionary[key][lang];
        } else {
          elem.textContent = dictionary[key][lang];
        }
      }
    }
  }
}