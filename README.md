Link Manager
============
Web app for storage, structure, and sharing hyperlinks.

User storages hyperlinks with a tag, such as "Useful" or "Image", hyperlink can be shared to another
user.

Structure
===========
The project is structured into three modules

1. link-manager

- Parent Pom

2. link-manager-API (RESTful)

- Spring Boot
- Spring Security
- Spring Web
- Spring Data
- Validation
- Flyway
- JWT

3. link-manager-UI (UI)

- Spring Boot
- Spring Web
- Validation
- Thymeleaf

Endpoints in RESTful

1. `/api/v1/token` - gives JWT access-refresh tokens, refreshing-invalidating them
2. `/api/v1/users` - data about users
3. `/api/v1/links` - hyperlinks that users created
4. `/api/v1/tags`- tags that are using for hyperlinks
5. `/api/v1/accounts` - data about account-types
6. `/api/v1/sharing` - hyperlink sharing (offers and invites)

Admin`s opportunity

- Create-edit-delete : /tags and /accounts
- Users : change password, account-type delete user and his hyperlinks

Install
==========

1. Set environment variables : In the root dir, the .env file is present with executable env.cmd to
   set these environment variables
2. Package the modules by maven : Manually in IDE packaging can be done by parent pom, Docker is
   required for the test-containers
3. Build the containers by docker-compose : docker-compose is present in the root dir, it executes
   the Dockerfiles for modules

Usage
=========
The modules use server-ports REST-8090, UI-8080

In a browser paste "localhost:8080" to use UI

or

"localhost:8090/swagger-ui/index.html" to use REST thought swagger

For the default:

- Only one user with nickname root and admin`s authorities is registered
- Only one account-type is present, it is the "Default Account"
- No tags are present

## Web-pages (some examples are present here)

#### **Main-page (nav-bar is similar for any page)**

Without authorization
![img.png](README-images/img.png)

With authorization
![img_1.png](README-images/img_1.png)

### **Tags**

Search menu with **admin`s** button, to add new tag

![img_2.png](README-images/img_2.png)

### **Account-types**

Search menu with **admin`s** button, to add new account-type

![img_3.png](README-images/img_3.png)

### **Users (Concrete user)**

User`s page:

**Admin`s view**

![img_4.png](README-images/img_4.png)

**Default view**

![img_5.png](README-images/img_5.png)

Disclaimer
=====
- Some buttons don't have a style - the reason is to view their status or a possible usage